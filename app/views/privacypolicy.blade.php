@extends('layouts.master')
@section('header')
 @include('header')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}

@stop

@section('body')
<div class="intro">
  <div class="intro-body">
   <!--  <div class="slogan"> -->
    <div style="padding-top:30px">
   

<h2>PRIVACY POLICY</h2>
<div class="container" style="width:700px ;font-size:20px">
	<p style="text-align:justify " >
		Policy
		The following terminology applies to these “Privacy Policy”, “Terms and Conditions”, “Cancellation/Refund Policy” and any or all Agreements: "Student", “You” and “Your” refers to you, the student accessing this website to register for events and accepting the College’s terms and conditions. "The College", “Fr C. Rodrigues Institute of Technology”, “Fr.CRIT”, “Student Council of Fr.CRIT”, “Student Council”, “Etamax Council”, “Etamax Committee”, “Ourselves”, “We” and "Us", refers to the Etamax Council, part of Fr. C. Rodrigues Institute of Technology.<br>
		<br>
		
		<b><u style="font-size:24px">What information is collected?</u></b> <br>
		The student database present with the college (Fr.CRIT) is used to auto populate most of your information and create an account for you. But you will be required to fill in information while registering for events or while making payments.<br>
		When registering on our site for events, as appropriate, you may be asked to enter your Roll-No, E-mail address, Credit Card/Debit Card/Net Banking information (which ever you opted) for payment purpose. You may, however, visit our site anonymously to explore the events. Registration and payment for an event will require you to use your account.	<br><br>
		<b><u style="font-size:24px">For what purpose is your information used?</u></b>
		Any of the information we collect from you may be used in one of the following ways:
		<ul style="text-align:justify ">
			<li> To personalize your experience<br>
					Your information is used to personalize the content shown to you.
			</li>		
			<li> To improve our website and services <br>
					We continually strive to improve our website based on the information and feedback given by you.
			</li>
			<li> To process transactions<br>
					Your information will be used to perform transactions on the Credit Card/Debit Card/Net Banking depending on your selection of the mode of payment. This information is not stored on our server in any way. Your information will not be sold, exchanged, transferred, or given to any other company/organization for any reason whatsoever, without your consent, other than for the express purpose of providing you with the purchased event.
			</li>
			<li> To conduct a contest, perform promotion, survey or other site features</li>
			<li> To send periodic emails <br>
					The email address provided by the student database held by FCRIT, may be used to send you information and updates pertaining to your event registration, in addition to receiving occasional news, updates, related events or service information, etc.
			</li>
		</ul>

	</p>

      
       
         </div>
  <!-- </div> -->
  
</div>
</div>
</div>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}

{{ HTML::script('js/jsfrontend/main.js')}}

@stop
