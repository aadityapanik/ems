@extends('layouts.master')
@section('style')
{{ HTML::style('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css') }}
@stop
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')
<header class="intro">
  
<div class="intro-body">
<div class="container">
<!--   <div class="col-md-offset-1"> -->
    
@if($students->count()>0)
<h1>List of Students</h1>
<table id="studenttable" class="table table-hover">
  <thead>
    <tr>
      <th>Roll No.</th>
      <th>Branch</th>
      <th>Semester</th>
      <th>Name</th>
      <th>Contact No.</th>
      <th>Email ID.</th>
    </tr>
  </thead>
  <tbody>
    @foreach($students as $student)
    <tr>
      <td>{{$student->Student_RollNo}}</td>
      <td>{{$student->Student_Branch}}</td>
      <td>{{$student->Student_Semester}}</td>
      <td>{{$student->Student_FN." ".$student->Student_LN}}</td>
      <td>{{$student->Student_ContactNo}}</td>
      <td>{{$student->Student_EmailID}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@else
  <h1>No Students</h1>
@endif

</div>
  </div>
<!--   </div> -->
</header>  
@stop

@section('footer')
{{ HTML::script('http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js') }}
{{ HTML::script('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js') }}
<script>
  $(document).ready(function() {
    $('#studenttable').DataTable();
  });
</script>
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.5.0/bootstrap-table.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop