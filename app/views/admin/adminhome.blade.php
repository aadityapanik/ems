@extends('layouts.master')
<!-- @section('style')
    <style>
        .dropdown-menu {
            width: 300px !important;
            padding: 10px 10px 0px 10px;
        }
        .navbar-nav>li>.dropdown-menu {
            margin-top: 0;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }
    </style>
@stop -->
@section('header')
@include('admin.adminheadernew')
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
{{ HTML::style('css/cssfrontend/main.css') }}
@stop
@section('body')
<header class="intro" style="padding-top: 100px">
    <div class="intro-body">
      <div class="slogan">
     <div style="padding-top: 30px">
        <h1 style="font-size:60px">EMS</h1>
        <p style="font-size:40px">Simple, yet efficient</p>
        @if(Auth::admin()->check())
            <br><h2>Administrator Commands</h2>
            <a class="btn btn-default" href="{{ URL::to('admin/add') }}" role="button">Add Admin</a>
            <a class="btn btn-default" href="{{ URL::to('admin/delete') }}" role="button">Delete Admin</a>
            <a class="btn btn-default" href="{{ URL::to('admin/event/add') }}" role="button">Add Event</a>
            <a class="btn btn-default" href="{{ URL::to('admin/event/show') }}" role="button">Show Event</a>
            <a class="btn btn-default" href="{{ URL::to('admin/mail') }}" role="button">Send Mail</a>
            <a class="btn btn-default" href=" {{ URL::to('admin/excel') }} " role="button"> Report Generation</a>
            <a class="btn btn-default" href=" {{ URL::to('admin/uploadfile') }} " role="button"> File Updataion</a>
            <br><h2>Student Commands</h2>
            <a class="btn btn-default" href=" {{ URL::to('admin/students') }} " role="button"> View All Students</a>
        @endif
</div>
  </div>
  </div>
</header>
@stop
@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}

{{ HTML::script('js/jsfrontend/main.js')}}

@stop
