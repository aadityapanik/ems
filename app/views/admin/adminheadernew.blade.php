<!-- <nav class="navbar navbar-inverse" style="margin-top: 1%;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ETAMAX</a>
        </div>
      
       -->

{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css')}}

      <!-- Navigation -->
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header" >
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand page-scroll" href="{{URL::to('/')}}">
          <i class="fa fa-play-circle"></i>  <span class="light"></span> Home
        </a>
      </div>
    
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
        <ul class="nav navbar-nav">
          <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
          <li class="hidden">
            <a href="#page-top"></a>
          </li>
         
          <li>
            <a href="{{URL::to('student/')}}">Student</a>
          </li>
        </ul>
        
            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::admin()->check())
                    <li @if($errors->has())class="dropdown open" @else class="dropdown" @endif>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                        <ul id="dropdown-login" class="dropdown-menu">
                            {{ Form::open(array('url'=>'admin/login','method'=>'post','class'=>'form-group')) }}
                            {{ Form::label('username','Username',array('for'=>'username')) }}
                            {{ Form::text('username',null,array('class'=>'form-control','placeholder'=>'Username')) }}
                            {{ Form::label('password','Password',array('for'=>'password')) }}
                            {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
                            {{ Form::label('rememberme', 'Remember Me') }}
                            {{ Form::checkbox('remember_me',null, array('class'=>'checkbox'))}}<br>
                            {{ Form::submit('Login',array('class'=>'btn btn-primary')) }}
                            {{ Form::close() }}
                                     
                            @if($errors->has())
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        {{{ $error }}}
                                    </div>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                @else
              
                  <!-- sidebar attempt -->
          <li>
            <a href="#">Actions</a>
            <ul class="dropdown-menu">
              <li><a href="{{ URL::to('/admin') }}">Admin Dashboard</a>
              </li>
              
              <li><a href="{{ URL::to('admin/add') }}">Add an Admin</a>
              </li>
              <li><a href="{{ URL::to('admin/delete') }}">Delete Admin</a>
              </li>
              <li><a href="{{ URL::to('admin/event/add') }}">Add a new Event</a>
              </li>
              <li><a href="{{ URL::to('admin/event/show') }}">Modify existing Events</a>
              </li>
              
              <li><a href="{{ URL::to('admin/mail') }}">Send an Email</a>
              </li>
              <li><a href="{{ URL::to('admin/excel') }} ">Report Generation</a>
              </li>
              <li><a href="{{ URL::to('admin/uploadfile') }} ">Upload Files to DB</a>
              </li>
               <li><a href="{{ URL::to('admin/students') }} ">View all students</a>
              </li>
            </ul>
          </li>
            
          
          <!-- /.sidebar attempt -->
             
              
              
            
                    <li><p class="navbar-text"> Welcome, {{Auth::admin()->get()->Admin_FN}}</p></li>
                    <li><a href="{{ URL::to('admin/logout') }}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
             
           
                @endif       
            
        </ul>
        
        
        
        
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </nav>
  <!-- /Navigation -->
      
      
      
      
<!--         <div class="collapse navbar-collapse" id="myNavbar">
          
          
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('admin/') }}">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
          
          
          
        </div>
    </div>
</nav> -->