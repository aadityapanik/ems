@extends('layouts.master')
@section('header')
@include('admin.adminheadernew')
@stop

@section('body')
<header class="intro">
   <div class="container">
     <div class="col-md-6 col-md-push-3" >
       <div class="slogan">
         <div style="padding-top: 30px">
<h1> Report Generation </h1>


{{ Form::open(array('url'=>'admin/excel-submit','method'=>'post','class'=>'form-group')) }}

{{ Form::label('criteria', 'Criteria')}}
{{ Form::select('criteria',['0'=>'Student','1'=>'Event','2'=>'Payment'],'1',array('class'=>'class="btn btn-default dropdown-toggle"') ) }}
@if ('criteria'==1)
           {
           {{ Form::label('eventidvalue','Event ID')}}
           {{ Form::text('eventidvalue')}}
           }
@endif
{{ Form::submit('Generate',array('class'=>'btn btn-info')) }}
<!--         btn btn-default dropdown-toggle    -->

{{ Form::close() }}

</div>
  </div>
     </div>
  </div>
     </header>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop
