<nav class="navbar navbar-inverse" style="margin-top: 1%;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ETAMAX</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('admin/') }}">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::admin()->check())
                    <li @if($errors->has())class="dropdown open" @else class="dropdown" @endif>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login<span class="caret"></span></a>
                        <ul id="dropdown-login" class="dropdown-menu">
                            {{ Form::open(array('url'=>'admin/login','method'=>'post','class'=>'form-group')) }}
                            {{ Form::label('username','Username',array('for'=>'username')) }}
                            {{ Form::text('username',null,array('class'=>'form-control','placeholder'=>'Username')) }}
                            {{ Form::label('password','Password',array('for'=>'password')) }}
                            {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
                            {{ Form::label('rememberme', 'Remember Me') }}
                            {{ Form::checkbox('remember_me',null, array('class'=>'checkbox'))}}<br>
                            {{ Form::submit('Login',array('class'=>'btn btn-primary')) }}
                            {{ Form::close() }}
                                     
                            @if($errors->has())
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        {{{ $error }}}
                                    </div>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                @else
                    <li><p class="navbar-text"> Welcome, {{Auth::admin()->get()->Admin_FN}}</p></li>
                    <li><a href="{{ URL::to('admin/logout') }}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>