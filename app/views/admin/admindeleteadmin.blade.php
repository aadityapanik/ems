@extends('layouts.master')
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')
<header class="intro">
   <div class="container">
     <div class="col-md-6 col-md-push-3" >
       <div class="slogan">
         <div style="padding-top: 30px">
<h2>Delete an Admin !</h2>
{{ Form::open(array('url'=>'admin/delete','class'=>'form-group')) }}
{{ Form::label('user','Admin Username') }}
{{ Form::text('user',null,array('placeholder'=>'Username','class'=>'form-control')) }}
<br>
{{ Form::submit('Delete',array('class'=>'btn btn-danger')) }}
{{ Form::close() }}
  </div>
  </div>
     </div>
  </div>
</header>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop