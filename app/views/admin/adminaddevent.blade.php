@extends('layouts.master')
@section('header')
@include('admin.adminheadernew')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

{{ HTML::style('css/bootstrap-datetimepicker.min.css') }}
<!-- {{ HTML::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css')}} -->
@stop
@section('body')
<header class="intro">
    <div class="intro-body" id="page-top">
      <div class="slogan">
    <h2>Add an Event !</h2>
		{{ Form::open(array('url'=>'admin/event/add','method'=>'post','class'=>'form-group')) }}
		{{ Form::label('eventname','Event Name' ) }}
		{{ Form::text('eventname',null,array('placeholder' => 'Enter the name of the event','class'=>'form-control')) }}
		<br>
		{{ Form::label('eventid','Event ID') }}
		{{ Form::text('eventid',null,array('placeholder' => 'Enter the ID of the event','class'=>'form-control') ) }}
		<br>
    {{ Form::label('eventday','Event Day') }}
		{{ Form::text('eventid',null,array('placeholder' => 'Enter the ID of the event','class'=>'form-control') ) }}
		<br>
		{{ Form::label('maxseats','Maximum Seats' ) }}
		{{ Form::text('maxseats',null,array('placeholder' => 'Enter the maximum no of seats for the event','class'=>'form-control')) }}
		<br>
<!-- 		{{ Form::label('minseats','Minimum Seats') }}
		{{ Form::text('minseats',null,array('placeholder' => 'Enter the minimum no of seats for the event','class'=>'form-control') ) }}
		<br> -->
		{{ Form::label('eventamount','Amount' ) }}
		{{ Form::text('eventamount',null,array('placeholder'=>'Enter the cost of entry to the event','class'=>'form-control')) }}
		<br>
    {{ Form::label('eventstarttime','Starting Time') }}
		<div id="start" class="input-append">
	    <input placeholder="Start Time"  name="eventstarttime" class="form-control" type="text">	
		<span class="add-on btn">
	        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
	    </span>
	    </div>
		
      	
		<br>
		
		{{ Form::label('eventendtime','Ending Time') }}
		<div id="end" class= "input-append">
	    <input placeholder="End Time"  name="eventendtime" class="form-control" type="text" >	
		<span class="add-on btn">
	        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
	    </span>
	    </div>
		<br>

		{{ Form::label('eventdate','Date' ) }}
		{{ Form::text('eventdate',null,array('placeholder' => 'Enter the event date','class'=>'form-control','id'=>'datepicker','class'=>'form-control')) }}
		<br>
		{{ Form::label('eventtype','Type of Event' ) }}
		{{ Form::text('eventtype',null,array('placeholder' => 'Enter the type of event ','class'=>'form-control')) }}
		<br>
        	<br>
		{{ Form::label('groupeventtype','Type of Group Event' ) }}
		{{ Form::text('groupeventtype',null,array('placeholder' => 'Enter the type of group event ','class'=>'form-control')) }}
        <br>
        	{{ Form::label('currentseat','Seat Count' ) }}
		{{ Form::text('currentseat',null,array('placeholder' => 'current seats ','class'=>'form-control')) }}

		<br>
		{{ Form::submit('Insert',array('class'=>'btn btn-primary')) }}
		{{ Form::close() }}
	</div>	
</div>
</header>
@stop

@section('footer')
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}    
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}

{{ HTML::script('js/bootstrap-datetimepicker.min.js') }}			
<script>
  $(function Work(){

  	$( "#datepicker" ).datepicker({
  			showOn: "button",
  			buttonText: "Choose date",
    		changeMonth: true,
    		changeYear: true,
        color: 'black',
    		dateFormat: 'dd-mm-yy'
  	});


    $('#start, #end').datetimepicker({
		
		pickDate: false,
		pick12HourFormat: true,
		format:"HH:mm PP",
		pickSeconds: false
    

		});
	 });

  </script>
@stop




<!--
{{ Form::label('eventstarttime','Starting Time' ) }}
		{{ Form::text('eventstarttime',null,array('placeholder' => 'Enter the event start time','class'=>'form-control','id'=>'starttime')) }}
		<br>
		{{ Form::label('eventendtime','Ending Time') }}
		{{ Form::text('eventendtime',null,array('placeholder' => 'Enter the event end time','class'=>'form-control','id'=>'endtime') )}}
		<br>
		
-->