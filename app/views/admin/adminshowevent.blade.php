@extends('layouts.master')
@section('style')
{{ HTML::style('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css') }}
@stop
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')
<header class="intro">
  <div class="intro-body">
    <div class="slogan">
<div class=" col-md-10 col-md-offset-1">
  
    <h1>{{$event->Event_Name}}</h1>
    <h3>{{$event->Event_ID}}</h3>
    <p>{{$event->Event_Description}}</p>
  </div>
  <p>Maximum Number of Seats : <b>{{$event->Event_Seats_Max}}</b><p>
  <p>Date of the Event : <b>{{Carbon::parse($event->Event_Date)->format('jS F, Y')}}</b><p>
  <p>Starts at : <b>{{Carbon::parse($event->Event_Start_Time)->format('h:i A')}}</b><p>
  <p>Ends at : <b>{{Carbon::parse($event->Event_End_Time)->format('h:i A')}}</b><p>
  <p>Type of the Event : <b>{{$event->Event_Type}}</b></p>
  <a class="btn btn-info" href="{{ URL::to('admin/event/update/'.$event->Event_ID)}}" role="button">Update</a>
  <a class="btn btn-danger" href="{{ URL::to('admin/event/delete/'.$event->Event_ID)}}" role="button">Delete</a>
</div>
  </div>
</header>
@if($students->count()>0)
<table id="studenttable" class="table table-hover">
  <thead>
    <th>Roll No.</th>
    <th>Branch</th>
    <th>Semester</th>
    <th>Name</th>
    <th>Contact No.</th>
    <th>Email ID.</th>
  </thead>
  <tbody>
    @foreach($students as $student)
    <tr>
      <td>{{$student->Student_RollNo}}</td>
      <td>{{$student->Student_Branch}}</td>
      <td>{{$student->Student_Semester}}</td>
      <td>{{$student->Student_FN." ".$student->Student_LN}}</td>
      <td>{{$student->Student_ContactNo}}</td>
      <td>{{$student->Student_EmailID}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@else
<h2>No Students</h2>
@endif
@stop

@section('footer')
{{ HTML::script('http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js') }}
{{ HTML::script('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js') }}
<script>
  $(document).ready(function() {
    $('#studenttable').DataTable();
  });
</script>
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop