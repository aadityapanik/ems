@extends('layouts.master')
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')

@if($errors->has())
  @foreach($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      <span class="sr-only">Error:</span>
      {{{ $error }}}
    </div>
  @endforeach
@endif

@if(Session::has('messagemailsent'))
    <div class="alert alert-success" role="alert">
      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
      <span class="sr-only"></span>
      {{{ Session::get('messagemailsent') }}}
    </div>
@endif

<header class="intro">
  <div class="intro-body">
  <div class="slogan">
  <div class="col-md-6 col-md-push-3  ">
    <h1>Sending Mail</h1>
      {{ Form::open(array('url'=>'admin/mail/','method'=>'post','class'=>'form-group')) }}
        {{ Form::label('rollno','Roll No.' ) }}
        {{ Form::text('rollno',null,array('class'=>'form-control','placeholder'=>'Roll No.')) }}
        <br>
        {{ Form::submit('Send',array('class'=>'btn btn-info')) }}
		  {{ Form::close() }}

    </div>
  </div>
  </div>
</header>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop