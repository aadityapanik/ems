@extends('layouts.master')
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')
<!-- <div class="jumbotron">
  <div class="container">
	<div class="col-md-4"> -->
<header class="intro">
    <div class="intro-body" id="page-top">
      <div class="slogan">
    <h2>Update {{ $event->Event_Name }}</h2>
		{{ Form::open(array('url'=>'admin/event/update/'.$event->Event_ID,'method'=>'post','class'=>'form-group')) }}
		{{ Form::label('eventname','Event Name' ) }}
		{{ Form::text('eventname',$event->Event_Name,array('class'=>'form-control')) }}
		<br>
		{{ Form::label('eventid','Event ID') }}
		{{ Form::text('eventid',$event->Event_ID,array('class'=>'form-control') ) }}
		<br>
    {{ Form::label('eventday','Event Day') }}
		{{ Form::text('eventday',null,array('placeholder' => 'Enter day of the event','class'=>'form-control') ) }}
		<br>
        
		{{ Form::label('maxseats','Maximum Seats' ) }}
		{{ Form::text('maxseats',$event->Event_Seats_Max,array('class'=>'form-control')) }}
		<br>
<!-- 		{{ Form::label('minseats','Minimum Seats') }}
		{{ Form::text('minseats',$event->Event_Seats_Min,array('class'=>'form-control') ) }}
		<br> -->
		{{ Form::label('eventamount','Amount' ) }}
		{{ Form::text('eventamount',$event->Event_Amount,array('class'=>'form-control')) }}
		<br>
		{{ Form::label('eventstarttime','Starting Time' ) }}
		{{ Form::text('eventstarttime',Carbon::parse($event->Event_Start_Time)->format('h:i A'),array('class'=>'form-control')) }}
		<br>
		{{ Form::label('eventendtime','Ending Time') }}
		{{ Form::text('eventendtime',Carbon::parse($event->Event_End_Time)->format('h:i A'),array('class'=>'form-control') ) }}
		<br>
		{{ Form::label('eventdate','Date' ) }}
		{{ Form::text('eventdate',Carbon::parse($event->Event_Date)->format('d-m-Y'),array('class'=>'form-control')) }}
		<br>
		{{ Form::label('eventtype','Type of Event' ) }}
		{{ Form::text('eventtype',$event->Event_Type,array('class'=>'form-control')) }}
		<br>
    {{ Form::label('groupeventtype','Type of Group Event' ) }}
		{{ Form::text('groupeventtype',null,array('placeholder' => 'Enter the type of group event ','class'=>'form-control')) }}
        <br>
    {{ Form::label('currentseat','Seat Count' ) }}
		{{ Form::text('currentseat',null,array('placeholder' => 'Current Seat Count','class'=>'form-control')) }}

		<br>
		{{ Form::submit('Update',array('class'=>'btn btn-info')) }}
		{{ Form::close() }}
<!-- 	</div>	
  </div>
</div> -->
      </div>
  </div>
</header>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop