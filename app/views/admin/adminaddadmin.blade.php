@extends('layouts.master')
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')
<header class="intro">
   <div class="container">
     <div class="col-md-6 col-md-push-3" >
<h2>New Admin</h2>
{{ Form::open(array('url'=>'admin/add', 'class'=>'form-group')) }}

{{ Form::label('username','Username') }}
{{ Form::text('username',null,array('placeholder'=>'Username','class'=>'form-control')) }}
<br>
{{ Form::label('password','Password') }}
{{ Form::password('password',array('placeholder'=>'Password','class'=>'form-control')) }}
<br>
{{ Form::label('email','Email ID') }}
{{ Form::text('email',null,array('placeholder'=>'Email ID','class'=>'form-control')) }}
<br>
{{ Form::label('firstname','First Name') }}
{{ Form::text('firstname',null,array('placeholder'=>'First Name','class'=>'form-control')) }}
<br>
{{ Form::label('lastname','Last Name') }}
{{ Form::text('lastname',null,array('placeholder'=>'Last Name','class'=>'form-control')) }}
<br>
{{ Form::label('contact','Contact Number') }}
{{ Form::text('contact',null,array('placeholder'=>'Contact Number','class'=>'form-control')) }}
<br>

{{ Form::submit('Add Admin',array('class'=>'btn btn-primary')) }}

{{ Form::close() }}
     </div>
    </div>
</header>

@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop
