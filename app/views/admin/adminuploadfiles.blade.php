@extends('layouts.master')
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')
<header class="intro">
    <div class="intro-body" id="page-top">
      <div class="slogan">
  <h1> Upload Files to Database </h1>

{{ Form::open(array('url'=>'admin/uploadfile-submit','method'=>'post','class'=>'form-group','files'=>'true')) }}

{{ Form::label('choice', 'Choose for Upload')}}<br>
{{ Form::select('choice',['0'=>'Upload Files PD','1'=>'Upload Files Events','2'=>'Upload Files Students','3'=>'Upload Files EventHeads'],'0',array('class'=>'btninline btn btn-sm btn-default dropdown-toggle','data-toggle'=>'dropdown','aria-expanded'=>'false') ) }}
{{ Form::file('file',array('class'=>'btninline')) }}<br>
{{ Form::submit('Insert',array('class'=>'btn btn-primary')) }}
{{ Form::close() }}
<br>
   <br>
        <h1>Templates Download Section</h1>
         
            <a class="btn btn-info" href=" {{ URL::to('admin/downloadfile/PD') }} " role="button"  >PD Template</a>
            <a class="btn btn-info" href=" {{ URL::to('admin/downloadfile/Events') }} " role="button"  > Events Template</a>
         <a class="btn btn-info" href=" {{ URL::to('admin/downloadfile/Students') }} " role="button"  > Students Template</a>
             
       
    @if($errors->uploadfail->has())
                        @foreach($errors->uploadfail->all() as $error)

                          <div class="alert alert-danger" role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>

                        @endforeach
  @endif
  
  @if(Session::has('uploadsuccess'))
        <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only"></span>
            {{{ Session::pull('uploadsuccess') }}}
        </div>
      </div>
  </div>
</header>

  @endif

 
  
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop

