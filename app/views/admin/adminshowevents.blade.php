@extends('layouts.master')
@section('style')
{{ HTML::style('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css') }}
@stop
@section('header')
@include('admin.adminheadernew')
@stop
@section('body')

@if($errors->has())
  @foreach($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      <span class="sr-only">Error:</span>
      {{{ $error }}}
    </div>
  @endforeach
@endif

@if(Session::has('messageupdate'))
    <div class="alert alert-info" role="alert">
      <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
      <span class="sr-only"></span>
      {{{ Session::get('messageupdate') }}}
    </div>
@endif

@if(Session::has('messagedelete'))
    <div class="alert alert-warning" role="alert">
      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
      <span class="sr-only"></span>
      {{{ Session::get('messagedelete') }}}
    </div>
@endif
<header class="intro">
  
<div class="intro-body">
<div class="container">
<!--   <div class="col-md-offset-1"> -->
    
@if($events->count()>0)
<h1>List of Events</h1>
<table id="eventtable" class="table table-hover">
  <thead>
    <tr>
      <th>Event ID</th>
      <th>Event Name</th>
      <th>Event Day</th>
      <th>Event Seats</th>
      <th>Event Amount</th>
      <th>Event Date</th>
      <th>Start Time</th>
      <th>End Time</th>
      <th>Event Type</th>
      <th>Group Event Type</th>
      <th>Current Seat Count</th>
      <th>Update Event</th>
      <th>Delete Event</th>
    </tr>
  </thead>
  <tbody>
    @foreach($events as $event)
    <tr onclick="document.location = '{{ URL::to('admin/event/'.$event->Event_ID) }}';">
      <td>{{$event->Event_ID}}</td>
      <td>{{$event->Event_Name}}</td>
      <td>{{$event->Event_Day}}</td>
      <td>{{$event->Event_Seats_Max}}</td>
      <td>{{$event->Event_Amount}}</td>
      <td>{{Carbon::parse($event->Event_Date)->format('jS F, Y')}}</td>
      <td>{{Carbon::parse($event->Event_Start_Time)->format('h:i A')}}</td>
      <td>{{Carbon::parse($event->Event_End_Time)->format('h:i A')}}</td>
      <td>{{$event->Event_Type}}</td>
      <td>{{$event->Group_Event_Type}}</td>
      <td>{{$event->Event_Seat_Count}}</td>
      <td><a class="btn btn-info btn-xs" href="{{ URL::to('admin/event/update/'.$event->Event_ID)}}" role="button">Update</a></td>
      <td><a class="btn btn-danger btn-xs" href="{{ URL::to('admin/event/delete/'.$event->Event_ID)}}" role="button">Delete</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
@else
  <h1>No Events</h1>
@endif

</div>
  </div>
<!--   </div> -->
</header>  
@stop

@section('footer')
{{ HTML::script('http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js') }}
{{ HTML::script('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js') }}
<script>
  $(document).ready(function() {
    $('#eventtable').DataTable();
  });
</script>
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.5.0/bootstrap-table.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop