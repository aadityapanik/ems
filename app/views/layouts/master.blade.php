<!doctype html>
<html lang="en">
<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('style')
    <title>ETAMAX | EMS</title>
</head>
<body>
    <div class="container-fluid">
        @yield('header')
      <div id="body">@yield('body')</div>
        @yield('footer')
    </div>
    <script>
      @yield('scripts')
    </script>
</body>
</html>
