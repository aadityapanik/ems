@extends('layouts.master')
@section('header')
 @include('header')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}

@stop

@section('body')
<div class="intro">
  <div class="intro-body">
   <!--  <div class="slogan"> -->
    <div style="padding-top:30px">
   

<h2>CANCELLATION/REFUND POLICY</h2>
<div class="container" style="width:700px ;font-size:20px">
	<p style="text-align:justify " >
		
		
		<ul style="text-align:justify ">
			<li> Once the events are registered, the registration fees paid by the student are not refundable under any condition.</li>		
			<li> 
				If for any reason, the event is cancelled, the following process should be followed by you:<br>
				<ol>
					<li> Contact the respective event head regarding the cancellation along with your details.</li>
					<li> The student council, in this scenario will refund the registration charges to the registered students offline.</li>
					<li> The student council is the final authority for the refunds.</li>
				</ol><br>
			</li>
			<li> In case if you miss your registered event, the registration charges will be not be refunded under any circumstances.</li>
			<li> While availing of any of the payment methods available on the Website, we will not be responsible or assume any liability, whatsoever in respect of any loss or damage arising directly or indirectly to you due to:
				<ol>
					<li> Lack of authorization for any transaction/s, or</li>
					<li> Exceeding the present limit mutually agreed by You and between "Bank/s", or</li>
					<li> Any payment issues arising out of the transaction, or</li>
					<li> Decline of transaction for any other reason/s</li>
					<br>
				</ol>
			</li>
			<li> In case, the registration charges for an event are charged on the Credit Card/ Debit Card/ Net-banking but not captured by our system, please send us an email at etamax@eventsfcrit.in. In such a scenario, we will investigate the issue and on successful tracking of the transaction will either:
				<ol>
					<li> Refund the registration charges if the event has been sold out</li>
					<li> Update the transaction in our system and register you for the event otherwise</li>
					<br>
				</ol>
			</li>
			<br>
		<b>Your Consent	</b><br>
		By using our site, you consent to our policies including “Privacy Policy”, “Terms and Conditions”, “Cancellation/Refund Policy” and any or all Agreements.<br>
		<b>Changes to our Policies</b><br>
		The policies mentioned above might change purely on our descretion without prior notice. However, the updated policies will be posted on this page. If you do not agree to the modified policies, you should discontinue your use of our website and services offered.<br>
		</ul>


	</p>

      
       
         </div>
  <!-- </div> -->
  
</div>
</div>
</div>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}

{{ HTML::script('js/jsfrontend/main.js')}}

@stop
