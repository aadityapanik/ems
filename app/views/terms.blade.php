@extends('layouts.master')
@section('header')
 @include('header')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}

@stop

@section('body')
<div class="intro">
  <div class="intro-body">
   <!--  <div class="slogan"> -->
    <div style="padding-top:30px">
   

<h2>TERMS AND CONDITIONS</h2>
<div class="container" style="width:700px ;font-size:20px">
	<p style="text-align:justify " >
		Welcome to the Website of Fr.C.R.I.T for ETAMAX 2015. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy and refund policy govern our relationship with you in relation to this website.<br>
		The following are the terms of use:<br>
		<ul style="text-align:justify ">
			<li> The Event will take place for 3 days on 12th, 13th and 14th of February, 2015.</li>
			<li> <b>Registration Criteria:</b><br>
				<ol><li> Atleast one event has to be selected on each day and hence attendance on each day is compulsory.</li> 
				<li> Atleast one event each from Group A, Group B and Group C must be selected by the student.</li>
				</ol><br>
			</li>
			<li> This completes the compulsory criteria (Phase 2).</li>
			<li> Registration for the non-compulsory events may be done later.</li><br>
			<li> <b>EVENT REGISTRATION PROCESS:</b><br>
				<b>Pre-Requisite: Sign-Up </b><br>
				<ol>
					<li> Go to the website URL- <a target='_blank' href="/">eventsfcrit.in</a> </li>
					<li> Click on the ‘Student’ button present on the top-header. </li>
					<li> Click on Sign up. </li>
					<li> You’ll be asked for your roll no and email ID(Your email id will have to be the same as the ones you have given for the database or else you won’t be able to sign up.  </li>
					<li> A mail will be sent to you immediately with the PASSWORD that will be used for the further registration process to login to your account.</li><br>
				</ol>
				<ul>
					<b>Note:</b>
					<li> For all the events, Registration is complete only if payment process has been completed.</li>
					<li> Once the registration process is complete, refund is not permitted.</li><br>
				</ul>
			</li>
			The Student has to Login first to perform the following activities using the Roll-no and the password that has been mailed during the Sign-Up process.<br>
			The registration process will occur in 3 phases as follows:<br><br>
			Phase 1: Team Formation Phase.<br>
			Phase 2: Compulsory events Registration with Online Payment.<br>
			Phase 3: Non-Compulsory events Registration with Online Payment.<br><br>

				<b><u>Phase 1 (Only for students registering for team/group events)
				Team Formation Phase:</b></u><br>
				Phase 1 will last for 1 day only. <br>
				ONLY the <b>Team Leaders</b> will have to register in this phase, so please ensure you have formed your teams beforehand and all the team members know who the team leader is. If more than one member of the same team registers as team leader then there will be no refund of Registration amount. 
				If any issue arises in a team, the team leader will be held responsible for payment and/or other damages.<br>

			<b>Process of registration:		</b><br>
				<ul>
					<li> The team leader enters his/her roll no, and selects the event and the day of the event.</li>
					<li> Then the team leader enters the no. of group members excluding himself/herself.</li>
					<li> After selecting all the fields he/she has to click on Enter Team Members.
							Now he/she will be directed to another page where he/she will have to enter Roll-Nos. of his/her team members.
							Team leader has to make sure that the correct Roll-No of his/her team members is entered.
					</li>
					<li> A mail will be sent to all the team members including the team leader with the TEAM-ID and the details of the events selected.
						 This team ID will have to be used later for registering TEAM EVENTS.

					</li>
					<li> The team leader enters his/her roll no, and selects the event and the day of the event.</li><br>
					Phase 1 is now completed.<br><br>
				</ul>

			<b><u>Phase 2: Compulsory Events Registration:</u></b><br>
			This phase will start after the Phase 1 is over. <br>
			<ul>
				<li> Here he/she can register for only 3 compulsory events that will fullfill the 3 day criteria as well as Group A, Group B, Group C criteria.</li>
				<li> If he/she selects an individual event, then the TEAM ID field should be left blank.</li>
				<li> If he/she selects a team event then the corresponding TEAM ID of the selected event should be entered.</li>
				<li> If he/she has selected more than one TEAM EVENT, than he/she has to enter the correct TEAM ID.</li>
				<li> Once you have chosen all the events then click on payment option and fill the necessary details.</li>
			</ul><br>

			<b><u>Phase 3: Non-Compulsory Events Registration:</u></b> <br>
			This phase will start after Phase 2 is over.<br>
			If he/she is interested to participate in more competitions or attend more workshops then register in this phase.<br>
			For team events follow the same steps as mentioned in Phase 2.<br>

			<b>Viewing the Selected Events:</b>
If he/she wants to check what all events you have registered for then click on SHOW ALL EVENTS <br>on your Student Homepage.




		</ul>
	</p>

      
      
      
      
         </div>
  <!-- </div> -->
  
</div>
</div>
</div>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}

{{ HTML::script('js/jsfrontend/main.js')}}

@stop