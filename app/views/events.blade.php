<!--@extends('layouts.master')-->

@section('style')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('http://fonts.googleapis.com/css?family=Josefin+Sans:400,700|Amatic+SC:400,700')}}
{{ HTML::style('css/cssfrontend/jquery.remodal.min.css')}}
{{ HTML::style('css/cssfrontend/easy-responsive-tabs.min.css')}}
{{ HTML::style('css/cssfrontend/component.min.css')}}
@stop

@stop


@section('body')
<div class="col-xs-12">
  <ol class="breadcrumb">
  <li><a  href="{{URL::to('/')}}">HOME</a></li>
  <li class="active">EVENTS</li>
</ol>
    <!--Vertical Tab-->
    <div id="parentVerticalTab">
      <ul class="resp-tabs-list hor_1">
        <li>
          <h1>
             A FEW THINGS...
            </h1> </li>
        <li>
          <h1>
              DAY 1, 12-02-15
            </h1> </li>
        <li>
          <h1>
              DAY 2, 13-02-15
            </h1> </li>
        <li>
          <h1>
              DAY 3, 14-02-15
            </h1> </li>
      </ul>
      <div class="resp-tabs-container hor_1">
        <div>
        
        <p class="ps">This page is organised in the following manner.</p>
        <div class="col-md-4">
<!-- DO NOT INSERT WHITESPACE-->
<pre><code>
EVENTS
├── A FEW THINGS...
├── DAY 1, 12-02-15
│   ├── Group-A
│   ├── Group-B
│   └── Group-C
├── DAY 2, 13-02-15
│   ├── Group-A
│   ├── Group-B
│   └── Group-C
└── DAY 3, 13-02-15
    ├── Group-A
    ├── Group-B
    └── Group-C
</code></pre>
        </div>
        <div class="col-md-8">
        <a href="#test" class="btn btn-material-blue-400 btn-raised">TECHNICAL EVENTS</a>
        <a href="#test" class="btn btn-material-red-400 btn-raised">CULTURAL EVENTS</a>
        <a href="#test" class="btn btn-primary btn-raised">DEPARTMENTAL SEMINARS</a>
        
<p class="ps">"*" represents some special criterion for participation besides the usual requirements. Clicking on a Event opens a modal windows to show additional details.
<br /><br />
The event details like <b>Venue</b>, <b>Timing</b> and even <b>Participation Fee</b> are subjected to change. <b>It is the responsibility of the participants to confirm event details form the respective Event Heads</b>.</p>
        </div>
        
        </div>
        <div>
          <ul class="og-grid">
            <li class="group-A technical workshop"> <a href="#14" class="btn btn-material-blue-400 btn-raised">AUTONOMOUS
                ROBOTICS</a> </li>
            <li class="group-A technical workshop"> <a href="#15" class="btn btn-material-blue-400 btn-raised">ANSYS</a> </li>
            <li class="group-A technical workshop"> <a href="#16" class="btn btn-material-blue-400 btn-raised">AUTOMOBILE
                WORKSHOP</a> </li>
            <li class="group-A technical workshop"> <a href="#17" class="btn btn-material-blue-400 btn-raised">MATLAB</a> </li>
            <li class="group-A technical workshop"> <a href="#18" class="btn btn-material-blue-400 btn-raised">PHOTOSHOP**</a> </li>
            <li class="group-A technical workshop"> <a href="#19" class="btn btn-material-blue-400 btn-raised">WEB
                DESIGNING**</a> </li>
            <li class="group-A technical workshop"> <a href="#20" class="btn btn-material-blue-400 btn-raised">EMBEDDED
                SYSTEMS</a> </li>
            <li class="group-A technical workshop"> <a href="#21" class="btn btn-material-blue-400 btn-raised">.NET</a> </li>
            <li class="group-A departmental-seminar"> <a href="#28" class="btn btn-primary btn-raised">IT
                DEPARTMENTAL SEMINAR</a> </li>
            <li class="group-A departmental-seminar"> <a href="#29" class="btn btn-primary btn-raised">COMPUTER DEPARTMENTAL
                SEMINAR</a> </li>
            <li class="group-A departmental-seminar"> <a href="#30" class="btn btn-primary btn-raised">HUMANITIES
                DEPARTMENTAL SEMINAR</a> </li>
            <li style="list-style: none; display: inline">
              <hr /> </li>
            <li class="group-B cultural workshop"> <a href="#1" class="btn btn-material-red-400 btn-raised">YOGA AND
                MEDITATION</a> </li>
            <li class="group-B cultural workshop"> <a href="#2" class="btn btn-material-red-400 btn-raised">GRAFFITTI
                WORSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#3" class="btn btn-material-red-400 btn-raised">DEBATE
                COMPETITION</a> </li>
            <li class="group-B cultural workshop"> <a href="#4" class="btn btn-material-red-400 btn-raised">CUP CAKE
                MAKING</a> </li>
            <li class="group-B cultural workshop"> <a href="#5" class="btn btn-material-red-400 btn-raised">AD MAKING</a> </li>
            <li class="group-B cultural workshop"> <a href="#6" class="btn btn-material-red-400 btn-raised">DANCE
                WORKSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#7" class="btn btn-material-red-400 btn-raised">MAGIC
                WORKSHOP</a> </li>
            <li class="group-B cultural competition"> <a href="#8" class="btn btn-material-red-400 btn-raised">SOLO & DUET SINGING(ELIMINATIONS)</a> </li>
            <li class="group-B cultural workshop"> <a href="#9" class="btn btn-material-red-400 btn-raised">MUSIC
                THERAPY*</a> </li>
            <li class="group-B cultural workshop"> <a href="#10" class="btn btn-material-red-400 btn-raised">ENTERPRENEURSHIP
                WORKSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#11" class="btn btn-material-red-400 btn-raised">PERSONALITY
                DEVELOPMENT</a> </li>
            <li class="group-B cultural workshop"> <a href="#12" class="btn btn-material-red-400 btn-raised">FILM MAKING
                WORKSHOP</a> </li>
            <li class="group-B technical competition"> <a href="#22" class="btn btn-material-blue-400 btn-raised">ROAD
                RASH(ELIMINATIONS)</a> </li>
            <li class="group-B technical competition"> <a href="#23" class="btn btn-material-blue-400 btn-raised">KICK TO
                GOAL(ELIMINATIONS)</a> </li>
            <li class="group-B technical competition"> <a href="#24" class="btn btn-material-blue-400 btn-raised">RUN FOR THE
                QUEEN(ELIMINATIONS)</a> </li>
            <li class="group-B technical competition"> <a href="#25" class="btn btn-material-blue-400 btn-raised">SMACK
                DOWN(ELIMINATIONS)</a> </li>
            <li class="group-B technical competition"> <a href="#26" class="btn btn-material-blue-400 btn-raised">MAGNETO</a> </li>
            <li class="group-B technical competition"> <a href="#27" class="btn btn-material-blue-400 btn-raised">ONLINE TECH
                HUNT</a> </li>
            <li style="list-style: none; display: inline">
              <hr /> </li>
            <li class="group-C cultural competition evening free to attend"> <a href="#13" class="btn btn-material-red-400 btn-raised">WAR OF
                BANDS</a> </li>
          </ul>
        </div>
        <div>
          <ul class="og-grid">
            <li class="group-A technical workshop"> <a href="#40" class="btn btn-material-blue-400 btn-raised">ECAD</a> </li>
            <li class="group-A technical workshop"> <a href="#41" class="btn btn-material-blue-400 btn-raised">AUTOMOBILE
                WORKSHOP</a> </li>
            <li class="group-A technical workshop"> <a href="#42" class="btn btn-material-blue-400 btn-raised">MATLAB</a> </li>
            <li class="group-A technical workshop"> <a href="#43" class="btn btn-material-blue-400 btn-raised">PHOTOSHOP**</a> </li>
            <li class="group-A technical workshop"> <a href="#44" class="btn btn-material-blue-400 btn-raised">WEB
                DESIGNING**</a> </li>
            <li class="group-A technical workshop"> <a href="#45" class="btn btn-material-blue-400 btn-raised">ETHICAL
                HACKING</a> </li>
            <li class="group-A technical workshop"> <a href="#46" class="btn btn-material-blue-400 btn-raised">PYTHON</a> </li>
            <li class="group-A technical workshop"> <a href="#47" class="btn btn-material-blue-400 btn-raised">GUI
                PROCESSING</a> </li>
            <li class="group-A technical workshop"> <a href="#48" class="btn btn-material-blue-400 btn-raised">ANDROID
                DEVELOPMENT</a> </li>
            
            <li style="list-style: none; display: inline">
              <hr /> </li>
            <li class="group-B cultural workshop"> <a href="#31" class="btn btn-material-red-400 btn-raised">YOGA AND
                MEDITATION</a> </li>
            <li class="group-B cultural workshop"> <a href="#32" class="btn btn-material-red-400 btn-raised">GRAFFITTI
                WORSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#33" class="btn btn-material-red-400 btn-raised">CUP CAKE
                MAKING</a> </li>
            <li class="group-B cultural workshop"> <a href="#34" class="btn btn-material-red-400 btn-raised">PHOTOGRAPHY</a> </li>
            <li class="group-B cultural workshop"> <a href="#35" class="btn btn-material-red-400 btn-raised">DANCE
                WORKSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#36" class="btn btn-material-red-400 btn-raised">DJ SOUND
                MIXING</a> </li>
            <li class="group-B cultural workshop"> <a href="#37" class="btn btn-material-red-400 btn-raised">STREET
                PLAY</a> </li>
            <li class="group-B cultural workshop"> <a href="#38" class="btn btn-material-red-400 btn-raised">PERSONALITY
                DEVELOPMENT</a> </li>
            <li class="group-B technical competition"> <a href="#49" class="btn btn-material-blue-400 btn-raised">REVERSE
                ENGINEERING</a> </li>
            <li class="group-B technical competition"> <a href="#50" class="btn btn-material-blue-400 btn-raised">TRUSS</a> </li>
            <li class="group-B technical competition"> <a href="#51" class="btn btn-material-blue-400 btn-raised">UNKNOWN
                CODING</a> </li>
            <li class="group-B technical competition"> <a href="#52" class="btn btn-material-blue-400 btn-raised">CONTRAPTIONS(ELIMINATIONS)</a> </li>
            <li class="group-B technical competition"> <a href="#53" class="btn btn-material-blue-400 btn-raised">ROAD
                RASH(FINALS)</a> </li>
            <li class="group-B technical competition"> <a href="#54" class="btn btn-material-blue-400 btn-raised">KICK TO
                GOAL(FINALS)</a> </li>
            <li class="group-B technical competition"> <a href="#55" class="btn btn-material-blue-400 btn-raised">RUN FOR THE
                QUEEN(FINALS)</a> </li>
            <li class="group-B technical competition"> <a href="#56" class="btn btn-material-blue-400 btn-raised">SMACK
                DOWN(FINALS)</a> </li>
            <li style="list-style: none; display: inline">
              <hr /> </li>
            <li class="group-C cultural competition evening free to attend"> <a href="#39" class="btn btn-material-red-400 btn-raised">GROUP DANCE</a> </li>
          </ul>
        </div>
        <div>
          <ul class="og-grid">
            <li class="group-A technical workshop"> <a href="#68" class="btn btn-material-blue-400 btn-raised">ECAD</a> </li>
            <li class="group-A technical workshop"> <a href="#69" class="btn btn-material-blue-400 btn-raised">GUI
                PROCESSING</a> </li>
            <li class="group-A technical workshop"> <a href="#70" class="btn btn-material-blue-400 btn-raised">ANDROID
                DEVELOPMENT</a> </li>
            <li class="group-A technical workshop"> <a href="#71" class="btn btn-material-blue-400 btn-raised">AUTOMOBILE
                WORKSHOP</a> </li>
            <li class="group-A technical workshop"> <a href="#72" class="btn btn-material-blue-400 btn-raised">PYTHON</a> </li>
            <li class="group-A technical workshop"> <a href="#73" class="btn btn-material-blue-400 btn-raised">WEB
                DESIGNING**</a> </li>
            <li class="group-A technical workshop"> <a href="#74" class="btn btn-material-blue-400 btn-raised">ANSYS</a> </li>
            <li class="group-A technical workshop"> <a href="#75" class="btn btn-material-blue-400 btn-raised">PHOTOSHOP**</a> </li>
            <li class="group-A technical workshop"> <a href="#76" class="btn btn-material-blue-400 btn-raised">.NET</a> </li>
            <li class="group-A departmental-seminar"> <a href="#82" class="btn btn-primary btn-raised">EXTC DEPARTMENTAL
                SEMINAR</a> </li>
            <li class="group-A departmental-seminar"> <a href="#83" class="btn btn-primary btn-raised">ELECTRICAL
                DEPARTMENTAL SEMINAR</a> </li>
                <li class="group-A departmental-seminar"> <a href="#57" class="btn btn-primary btn-raised">MECHANICAL
                DEPARTMENTAL SEMINAR</a> </li>
            <li style="list-style: none; display: inline">
              <hr /> </li>
            <li class="group-B cultural workshop"> <a href="#58" class="btn btn-material-red-400 btn-raised">YOGA AND
                MEDITATION</a> </li>
            <li class="group-B cultural workshop"> <a href="#59" class="btn btn-material-red-400 btn-raised">SELF
                DEFENCE</a> </li>
            <li class="group-B cultural workshop"> <a href="#60" class="btn btn-material-red-400 btn-raised">GENERAL
                AWARENESS (GYNAECOLOGIST SESSION)***</a> </li>
            <li class="group-B cultural workshop"> <a href="#61" class="btn btn-material-red-400 btn-raised">ENTREPRENEURSHIP
                WORKSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#62" class="btn btn-material-red-400 btn-raised">DANCE
                WORKSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#63" class="btn btn-material-red-400 btn-raised">MAGIC
                WORKSHOP</a> </li>
            <li class="group-B cultural workshop"> <a href="#65" class="btn btn-material-red-400 btn-raised">PERSONALITY
                DEVELOPMENT</a> </li>
            <li class="group-B technical competition"> <a href="#77" class="btn btn-material-blue-400 btn-raised">CRACK THE
                ALGO</a> </li>
            <li class="group-B technical competition"> <a href="#78" class="btn btn-material-blue-400 btn-raised">CONTRAPTIONS(FINALS)</a> </li>
            <li class="group-B technical competition"> <a href="#79" class="btn btn-material-blue-400 btn-raised">LINE
                FOLLOWER</a> </li>
            <li class="group-B technical competition"> <a href="#80" class="btn btn-material-blue-400 btn-raised">TPP</a> </li>
            <li class="group-B technical competition"> <a href="#81" class="btn btn-material-blue-400 btn-raised">JUNKYARD
                WARS</a> </li>
            <li style="list-style: none; display: inline">
              <hr /> </li>
            <li class="group-C cultural competition evening free to attend"> <a href="#64" class="btn btn-material-red-400 btn-raised">ELOCUTION</a> </li>
            <li class="group-C cultural competition evening free to attend"> <a href="#66" class="btn btn-material-red-400 btn-raised">SOLO &amp; DUET
                SINGING(FINALS)</a> </li>
            <li class="group-C cultural competition evening free to attend"> <a href="#67" class="btn btn-material-red-400 btn-raised">JAM SESSION</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="remodal" data-remodal-id="1">
    <dl> <dt>
          YOGA AND MEDITATION
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 12/02/2015 06:00:00 </dd>
      <dd> 12/02/2015 08:00:00 </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="2">
    <dl> <dt>
          GRAFFITTI WORSHOP
        </dt>
      <dd> Participation Fee: Rs.400(2) </dd>
      <dd> Venue: FOYER &amp; CR101 </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> Akshada <a href="tel:+917506080037">+917506080037</a>
          <br /> Akshay Bhirud <a href="tel:+919930044720">+919930044720</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="3">
    <dl> <dt>
          DEBATE COMPETITION
        </dt>
      <dd>Where the intelligent minds come to debate and share the opinions.</dd>
      <dd> Participation Fee: Rs.160(2) </dd>
      <dd> Venue: CR110 </dd>
      <dd> 12/02/2015 11:00:00 </dd>
      <dd> 12/02/2015 14:00:00 </dd>
      <dd> Contact:
        <p> Ramkrishna Tamboli:<a href="tel:+919892963116">+919892963116</a>
          <br /> SANKET <a href="tel:+919028602182">+919028602182</a> </p>
      </dd>
      <dd> group-B, cultural, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="4">
    <dl> <dt>
          CUP CAKE MAKING
        </dt>
      <dd> Participation Fee: Rs.140(2) </dd>
      <dd> Venue: ED HALL </dd>
      <dd> 12/02/2015 11:00:00 </dd>
      <dd> 12/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> PHALGUNI PATHAK <a href="tel:+919420557577">+919420557577</a>
          <br /> VIDUSHI <a href="tel:+919930747240">+919930747240</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="5">
    <dl> <dt>
          AD MAKING
        </dt>
      <dd>Advertisement making , is a great platform for showcasing that creative zing in you.</dd>
      <dd> Participation Fee: Rs.50 </dd>
      <dd> Venue: CR202 </dd>
      <dd> 12/02/2015 12:00:00 </dd>
      <dd> 12/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> Prashant <a href="tel:+91966466920">+91966466920</a>
          <br /> Shraddha <a href="tel:+919869984568">+919869984568</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="6">
    <dl> <dt>
          DANCE WORKSHOP
        </dt>
      <dd>It does not matter whether you like jazz or waltz, bharatnatyam or garba. What matters is that your heart loves to dance. Here you just need to Dance your way.</dd>
      <dd> Participation Fee: Rs.150(2) </dd>
      <dd> Venue: ED HALL </dd>
      <dd> 12/02/2015 14:30:00 </dd>
      <dd> 12/02/2015 16:30:00 </dd>
      <dd> Contact:
        <p> Aman M. [IT] <a href="tel:+919619871634">+919619871634</a>
          <br /> Ann M. Michael [COMPS] <a href="tel:+919920816749">+919920816749</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="7">
    <dl> <dt>
          MAGIC WORKSHOP
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: CR202 </dd>
      <dd> 12/02/2015 14:00:00 </dd>
      <dd> 12/02/2015 15:00:00 </dd>
      <dd>Contact:
        <p> RUTWICK <a href="tel:+919833944949">+919833944949</a>
          <br /> Ritesh Gogade(extc-6) <a href="tel:+918983432826">+918983432826</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="8">
    <dl> <dt>
          SOLO & DUET SINGING(ELIMINATIONS)
        </dt>
      <dd>Do you love to sing? Well here's a chance for you to showcase your talent and get rewarded.</dd>
      <dd> Participation Fee: Rs.100 & Rs.200(2)</dd>
      <dd> Venue: FOYER </dd>
      <dd> 12/02/2015 15:30:00 </dd>
      <dd> 12/02/2015 17:00:00 </dd>
      <dd> Contact:
        <p> TERRENCE <a href="tel:+919920707850">+919920707850</a>
          <br /> MANSI M. <a href="tel:+919833293240">+919833293240</a> </p>
      </dd>
      <dd> group-B, cultural, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="9">
    <dl> <dt>
          MUSIC THERAPY*
        </dt>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 12/02/2015 10:30:00 </dd>
      <dd> 12/02/2015 11:30:00 </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="10">
    <dl> <dt>
          ENTERPRENEURSHIP WORKSHOP
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 10:00:00 </dd>
      <dd> Contact:
        <p> PRABHAT M <a href="tel:+919987137481">+919987137481</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="11">
    <dl> <dt>
          PERSONALITY DEVELOPMENT
        </dt>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: CR303 </dd>
      <dd> 12/02/2015 14:30:00 </dd>
      <dd> 12/02/2015 16:30:00 </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="12">
    <dl> <dt>
          FILM MAKING WORKSHOP
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: CR305 </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> Prithviraj Shetty <a href="tel:+917506310342">+917506310342</a>
          <br /> Vidhya Gowda <a href="tel:+919167440280">+919167440280</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="13">
    <dl> <dt>
          WAR OF BANDS
        </dt>
      <dd> Participation Fee: Rs.500(2+) </dd>
      <dd> Venue: LAWN </dd>
      <dd> 12/02/2015 17:00:00 </dd>
      <dd> 12/02/2015 20:00:00 </dd>
      <dd> Contact:
        <p> ADITYA RAO <a href="tel:+919769320393">+919769320393</a>
          <br /> ROHAN M <a href="tel:+917208203631">+917208203631</a>
          <br /> SHASHANK <a href="tel:+919860017213">+919860017213</a>
          <br /> SHARDUL <a href="tel:+919833320662">+919833320662</a>
          <br /> AJAY <a href="tel:+919004240179">+919004240179</a> </p>
      </dd>
      <dd> group-C, cultural, competition, evening, free to attend </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="14">
    <dl> <dt>
          AUTONOMOUS ROBOTICS
        </dt>
      <dd> A detailed view on automation in robotics. </dd>
      <dd> Participation Fee: Rs.2100 </dd>
      <dd> Venue: CR105 </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> RISHI GIRSHAN <a href="tel:+919987809159">+919987809159</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="15">
    <dl> <dt>
          ANSYS
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CADCAM </dd>
      <dd> 12/02/2015 10:00:00 </dd>
      <dd> 12/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> YOGESH LAD (EH) [MECH] <a href="tel:+919930745024">+919930745024</a>
          <br /> PRAKSHAL [IT] <a href="tel:+917588757555">+917588757555</a>
          <br /> SHUBHAM KAMBLE [ELEC] <a href="tel:+919967046654">+919967046654</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="16">
    <dl> <dt>
          AUTOMOBILE WORKSHOP
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: ITI LAB </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> RAGHAV U. (EH) [MECH] <a href="tel:+918655423989">+918655423989</a>
          <br /> PRANIT WAVHAL [IT] <a href="tel:+918655357922">+918655357922</a>
          <br /> DHIRENDRA PANDEY [ELEC] <a href="tel:+919768303798">+919768303798</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="17">
    <dl> <dt>
          MATLAB
        </dt>
      <dd> Let's solve some maths problem, with the power of the computer. </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: MATLAB </dd>
      <dd> 12/02/2015 11:00:00 </dd>
      <dd> 12/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> KSHITIJA (EH) [EXTC] <a href="tel:+919819033869">+919819033869</a>
          <br /> JOHN DAVID [ELEC] <a href="tel:+918898475207">+918898475207</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="18">
    <dl> <dt>
          PHOTOSHOP**
        </dt>
      <dd> Editing various photos and creating a single photo by merging different photos is being taught using cs 5 photoshop software. </dd>
      <dd> Participation Fee: Rs.110 </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 12/02/2015 12:00:00 </dd>
      <dd> 12/02/2015 14:00:00 </dd>
      <dd> Contact:
        <p> TARUN P. (EH) [IT] <a href="tel:+919967704804">+919967704804</a>
          <br /> FELIX [ELEC] <a href="tel:+917208226672">+917208226672</a>
          <br /> SUFIYAN [EXTC] <a href="tel:+919702052467">+919702052467</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="19">
    <dl> <dt>
          WEB DESIGNING**
        </dt>
      <dd> Let's make a few web pages first. </dd>
      <dd> Participation Fee: Rs.110 </dd>
      <dd> Venue: INTERNET LAB </dd>
      <dd> 12/02/2015 12:00:00 </dd>
      <dd> 12/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> SHUBHAM THAKUR (EH) [IT] <a href="tel:">+919920416622</a>
          <br /> PIYUSH JAIN [MECH] <a href="tel:+919920416622">+919029278741</a>
          <br /> AKASH KUMBHARE [COMPS] <a href="tel:">+919594684194</a>
          <br /> SHAILESH V. [ELEC] <a href="tel:+919594684194">+919768184051</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="20">
    <dl> <dt>
          EMBEDDED SYSTEMS
        </dt>
      <dd> Participation Fee: Rs.220 </dd>
      <dd> Venue: CR205 </dd>
      <dd> 12/02/2015 14:00:00 </dd>
      <dd> 12/02/2015 17:00:00 </dd>
      <dd> Contact:
        <p> NILANJANA (EH) [ELEC] <a href="tel:+919867333837">+919867333837</a>
          <br /> SHUBHAM BHOIR [EXTC] <a href="tel:+918898394889">+918898394889</a>
          <br /> VIVEK PATIL [MECH] <a href="tel:+919167888311">+919167888311</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="21">
    <dl> <dt>
          .NET
        </dt>
      <dd> making dynamic web pages and backend of a website using .net framework. </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CR103 </dd>
      <dd> 12/02/2015 14:00:00 </dd>
      <dd> 12/02/2015 16:00:00 </dd>
      <dd> Contact:
        <p> SWATI S. (EH) [IT] <a href="tel:+919769673479">+919769673479</a>
          <br /> PRIYANKA B. [EXTC] <a href="tel:+919769673479">+919769673479</a>
          <br /> TUSHAR K. [ELEC] <a href="tel:+918286023976">+918286023976</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="22">
    <dl> <dt>
          ROAD RASH(ELIMINATIONS)
        </dt>
      <dd> Participation Fee: Rs.100(3) </dd>
      <dd> Venue: FOYER( DEGREE) </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> FRANCIS COLACO (EH) [EXTC] <a href="tel:+919930059162">+919930059162</a>
          <br /> AKSHAY CHALKE [MECH] <a href="tel:+919403112129">+919403112129</a>
          <br /> AKSHAY MAHAJAN [ELEC] <a href="tel:+919975942452">+919975942452</a>
          <br /> HARDEEP SINGH [MECH] <a href="tel:+918082536590">+918082536590</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="23">
    <dl> <dt>
          KICK TO GOAL(ELIMINATIONS)
        </dt>
      <dd> A game of football for your bot. The robot scoring maximum number of goals in limited time wins. </dd>
      <dd> Participation Fee: Rs.100(3) </dd>
      <dd> Venue: FOYER( DEGREE) </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> RHEA JOHNSON (EH) [ELEC] <a href="tel:+919820609514">+919820609514</a>
          <br /> PARAS B. [MECH] <a href="tel:+918446677689">+918446677689</a>
          <br /> BHUSHAN KAMBLE [EXTC] <a href="tel:">+917208869063</a>
          <br /> PRITESH TUPE [IT] <a href="tel:+917208869063">+918976577195</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="24">
    <dl> <dt>
          RUN FOR THE QUEEN(ELIMINATIONS)
        </dt>
      <dd> Two bots will start from different starting points and will have to rescue the Queen which is located at the destination point. The bot which rescues the queen first wins the game. </dd>
      <dd> Participation Fee: Rs.100(3) </dd>
      <dd> Venue: FOYER(DEGREE) </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> PRATHAMESH J. [ELEC] <a href="tel:+919833988491">+919833988491</a>
          <br /> SANKET KANADE (EH) [EXTC] <a href="tel:+919167426657">+919167426657</a>
          <br /> RAKSHIT OGRA [IT] <a href="tel:+919619517820">+919619517820</a>
          <br /> ANIKET DHARMADHIKARI [MECH] <a href="tel:+919096960239">+919096960239</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="25">
    <dl> <dt>
          SMACK DOWN(ELIMINATIONS)
        </dt>
      <dd> Build a robot to oust your opponent out of the arena just by pushing your opponent. Active weapons not allowed. The bot should be able to carry its own weight. </dd>
      <dd> Participation Fee: Rs.160(4) </dd>
      <dd> Venue: FOYER( DEGREE) </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> RANGARAJAN S(EH) [ELEC] <a href="tel:+917208329692">+917208329692</a>
          <br /> ABIN THOMAS [EXTC] <a href="tel:+919769660979">+919769660979</a>
          <br /> HIMALI MHATRE [ELEC] <a href="tel:+919503366307">+919503366307</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="26">
    <dl> <dt>
          MAGNETO
        </dt>
      <dd> The robot will have to throw ball in a pit. The bot throwing maximum balls is the winner. </dd>
      <dd> Participation Fee: Rs.160(4) </dd>
      <dd> Venue: BACK FOYER </dd>
      <dd> 12/02/2015 09:00:00 </dd>
      <dd> 12/02/2015 15:00:00 </dd>
      <dd>
        <p> VINAYAK S(EH) [EXTC] <a href="tel:+917208384878">+917208384878</a>
          <br /> SAURABH VERMA [EXTC] <a href="tel:+919769660319">+919769660319</a>
          <br /> SUDARSHAN SHETTY [MECH] <a href="tel:+919930524234">+919930524234</a>
          <br /> kANWAL JEET IT - 6 <a href="tel:+919820201787">+919820201787</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="27">
    <dl> <dt>
          ONLINE TECH HUNT
        </dt>
      <dd> It is online hunt for the clues given to make a meaningful sentence. The puzzle is to be solved in the least time. </dd>
      <dd> Participation Fee: Rs.80(2) </dd>
      <dd> Venue: CR205 </dd>
      <dd> 12/02/2015 10:00:00 </dd>
      <dd> 12/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> TANVI P. (EH) [EXTC] <a href="tel:+919892651667">+919892651667</a>
          <br /> MITRA [IT] <a href="tel:+918976531473">+918976531473</a>
          <br /> MRUNAL LIMAYE [ELEC] <a href="tel:+919757104561">+919757104561</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="28">
    <dl> <dt>
          IT DEPARTMENTAL SEMINAR
        </dt>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 12/02/2015 13:30:00 </dd>
      <dd> 12/02/2015 15:30:00 </dd>
      <dd> group-A, departmental seminar </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="29">
    <dl> <dt>
          COMPUTER DEPARTMENTAL SEMINAR
        </dt>
      <dd> Participation Fee: Rs.NONE </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 12/02/2015 11:00:00 </dd>
      <dd> 12/02/2015 13:00:00 </dd>
      <dd> group-A, departmental seminar </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="30">
    <dl> <dt>
          HUMANITIES DEPARTMENTAL SEMINAR
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 12/02/2015 10:30:00 </dd>
      <dd> 12/02/2015 12:30:00 </dd>
      <dd> group-A, departmental seminar </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="31">
    <dl> <dt>
          YOGA AND MEDITATION
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 13/02/2015 06:00:00 </dd>
      <dd> 13/02/2015 08:00:00 </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="32">
    <dl> <dt>
          GRAFFITTI WORSHOP
        </dt>
      <dd> Participation Fee: Rs.400(2) </dd>
      <dd> Venue: FOYER &amp; CR101 </dd>
      <dd> 13/02/2015 09:00:00 </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> Akshada <a href="tel:+917506080037">+917506080037</a>
          <br /> Akshay Bhirud <a href="tel:+919930044720">+919930044720</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="33">
    <dl> <dt>
          CUP CAKE MAKING
        </dt>
      <dd> Participation Fee: Rs.140(2) </dd>
      <dd> Venue: ED HALL </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> 13/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> PHALGUNI PATHAK <a href="tel:+919420557577">+919420557577</a>
          <br /> VIDUSHI <a href="tel:+919930747240">+919930747240</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="34">
    <dl> <dt>
          PHOTOGRAPHY
        </dt>
      <dd>Photography is more than a medium for factual communication of ideas. It is a creative art, here we give you a chance to showcase your art.</dd>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: CR110 </dd>
      <dd> 13/02/2015 10:00:00 </dd>
      <dd> 13/02/2015 12:00:00 </dd>
      <dd>
        <p> Daksha Dhakane [IT] <a href="tel:+918082196690">+918082196690</a>
          <br /> Apeksha Khilari [EXTC] <a href="tel:+919699271673">+919699271673</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="35">
    <dl> <dt>
          DANCE WORKSHOP
        </dt>
      <dd>It does not matter whether you like jazz or waltz, bharatnatyam or garba. What matters is that your heart loves to dance. Here you just need to Dance your way.</dd>
      <dd> Participation Fee: Rs.150(2) </dd>
      <dd> Venue: ED HALL </dd>
      <dd> 13/02/2015 13:30:00 </dd>
      <dd> 13/02/2015 15:30:00 </dd>
      <dd> Contact:
        <p> Aman M. [IT] <a href="tel:+919619871634">+919619871634</a>
          <br /> Ann M. Michael [COMPS] <a href="tel:+919920816749">+919920816749</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="36">
    <dl> <dt>
          DJ SOUND MIXING
        </dt>
      <dd> Participation Fee: Rs.120 </dd>
      <dd> Venue: CR202 </dd>
      <dd> 13/02/2015 14:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> Saurabh Warde <a href="tel:+917709013400">+917709013400</a>
          <br /> Shrutika Sawardekar <a href="tel:">+919769455805</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="37">
    <dl> <dt>
          STREET PLAY
        </dt>
      <dd>Offers avenues for wholesome entertainment. It combines group performances with live acting and hearty song-and-dance sequences.</dd>
      <dd> Participation Fee: Rs.800(10+) </dd>
      <dd> Venue: LAWN </dd>
      <dd> 13/02/2015 15:30:00 </dd>
      <dd> 13/02/2015 16:30:00 </dd>
      <dd> Contact:
        <p> SPARSH <a href="tel:+918149464536">+918149464536</a>
          <br /> SAURABH T <a href="tel:+919969786230">+919969786230</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="38">
    <dl> <dt>
          PERSONALITY DEVELOPMENT
        </dt>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: CR303 </dd>
      <dd> 13/02/2015 14:30:00 </dd>
      <dd> 13/02/2015 16:30:00 </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="39">
    <dl> <dt>
          GROUP DANCE
        </dt>
      <dd>It does not matter whether you like jazz or waltz, bharatnatyam or garba. What matters is that your heart loves to dance. Here you just need to Dance your way to win exciting prizes.</dd>
      <dd> Participation Fee: Rs.1000(2-5) </dd>
      <dd> Venue: LAWN </dd>
      <dd> 13/02/2015 17:00:00 </dd>
      <dd> 13/02/2015 20:00:00 </dd>
      <dd> Contact:
        <p> RANJEET <a href="tel:+919820071220">+919820071220</a>
          <br /> AJAY B <a href="tel:+919769718818">+919769718818</a>
          <br /> CHRISTINA <a href="tel:+919820263963">+919820263963</a>
          <br /> LIYA <a href="tel:+919930940179">+919930940179</a>
          <br /> OMKAR G <a href="tel:+919594303230">+919594303230</a> </p>
      </dd>
      <dd> group-C, cultural, competition, evening, free to attend </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="40">
    <dl> <dt>
          ECAD
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CADCAM </dd>
      <dd> 13/02/2015 10:00:00 </dd>
      <dd> 13/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> HARSHADA ARORA(EH) [IT] <a href="tel:+919702461547">+919702461547</a>
          <br /> HARSHAL PARMAR [COMPS] <a href="tel:+918691869059">+918691869059</a>
          <br /> KISHAN PATIL [MECH] <a href="tel:+918698049224">+918698049224</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="41">
    <dl> <dt>
          AUTOMOBILE WORKSHOP
        </dt>
      <dd> Various types of engines and recent trends in automobile engines with turbocharging and supercharging theory explanation and various volkswagen engines </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: ITI LAB </dd>
      <dd> 13/02/2015 11:30:00 </dd>
      <dd> 13/02/2015 13:30:00 </dd>
      <dd> Contact:
        <p> RAGHAV U (EH) [MECH] <a href="tel:+918655423989">+918655423989</a>
          <br /> PRANIT WAVHAL [IT] <a href="tel:+918655357922">+918655357922</a>
          <br /> DHIRENDRA PANDEY [ELEC] <a href="tel:+919768303798">+919768303798</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="42">
    <dl> <dt>
          MATLAB
        </dt>
      <dd> Solve maths using the power of the computer. </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: MATLAB </dd>
      <dd> 13/02/2015 09:00:00 </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> KSHITIJA (EH) [EXTC] <a href="tel:+919819033869">+919819033869</a>
          <br /> JOHN DAVID [ELEC] <a href="tel:+918898475207">+918898475207</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="43">
    <dl> <dt>
          PHOTOSHOP**
        </dt>
      <dd> Editing various photos and creating a single photo by merging different photos is being taught using cs 5 photoshop software. </dd>
      <dd> Participation Fee: Rs.110 </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 13/02/2015 13:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> TARUN P (EH) [IT] <a href="tel:+919967704804">+919967704804</a>
          <br /> FELIX [ELEC] <a href="tel:+917208226672">+917208226672</a>
          <br /> SUFIYAN [EXTC] <a href="tel:+919702052467">+919702052467</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="44">
    <dl> <dt>
          WEB DESIGNING**
        </dt>
      <dd> Let's make a few web pages first. </dd>
      <dd> Participation Fee: Rs.110 </dd>
      <dd> Venue: INTERNET LAB </dd>
      <dd> 13/02/2015 12:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> SHUBHAM THAKUR (EH) [IT] <a href="tel:+919920416622">+919920416622</a>
          <br /> PIYUSH JAIN [MECH] <a href="tel:+919029278741">+919029278741</a>
          <br /> AKASH KUMBHARE [COMPS] <a href="tel:+919594684194">+919594684194</a>
          <br /> SHAILESH V [ELEC] <a href="tel:+919768184051">+919768184051</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="45">
    <dl> <dt>
          ETHICAL HACKING
        </dt>
      <dd> Black hat or White had? </dd>
      <dd> Participation Fee: Rs.500 </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 13/02/2015 09:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contcat:
        <p> AISHWARYA W (EH) [COMPS] <a href="tel:+919702106530">+919702106530</a>
          <br /> AKASH ANAND RAO [IT] <a href="tel:+919987736337">+919987736337</a>
          <br /> CHIRAG K [EXTC] <a href="tel:+919029388594">+919029388594</a>
          <br /> VIGHNESH S [MECH] <a href="tel:+919920955151">+919920955151</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="46">
    <dl> <dt>
          PYTHON
        </dt>
      <dd> Learning a new scripting language basics, making a GUI interface,creating a small game. </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CR201 </dd>
      <dd> 13/02/2015 13:00:00 </dd>
      <dd> 13/02/2015 16:00:00 </dd>
      <dd> Contact:
        <p>  PREETAM WALVEKAR (EH) [COMPS] <a href="tel:+918097976989">+918097976989</a>
        <br /> KANIKA [EXTC] <a href="tel:+919969023581">+919969023581</a>
          <br /> KRUNAL BONDE [EXTC] <a href="tel:+917208801292">+917208801292</a>
          <br /> AKASH S [IT] <a href="tel:+919619692531">+919619692531</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="47">
    <dl> <dt>
          GUI PROCESSING
        </dt>
      <dd> Time to make sweet GUIs. </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CR305 </dd>
      <dd> 13/02/2015 13:30:00 </dd>
      <dd> 13/02/2015 16:30:00 </dd>
      <dd> Contact:
        <p> ASHITHA ANN (EH) [EXTC] <a href="tel:+919987543423">+919987543423</a>
          <br /> SAYALEE K [ELEC] <a href="tel:+918454042779">+918454042779</a>
          <br /> AKSHAY P [COMPS] <a href="tel:+918655726027">+918655726027</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="48">
    <dl> <dt>
          ANDROID DEVELOPMENT
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 13/02/2015 09:00:00 </dd>
      <dd> 13/02/2015 12:00:00 </dd>
      <dd> Contact:
        <p> DASHMIT(EH) [IT] <a href="tel:+919619995655">+919619995655</a>
          <br /> ABHILASHA [IT] <a href="tel:+919821814934">+919821814934</a>
          <br /> SHUBHAM PATIL [MECH] <a href="tel:+919819868414">+919819868414</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="49">
    <dl> <dt>
          REVERSE ENGINEERING
        </dt>
      <dd> It has been made, but can you unmake it? </dd>
      <dd> Participation Fee: Rs.100(2) </dd>
      <dd> Venue: CR204 </dd>
      <dd> 13/02/2015 09:00:00 </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> SNEHAL K. (EH) [EXTC] <a href="tel:+919819743729">+919819743729</a>
          <br /> JENY MATHEW [ELEC] <a href="tel:+918108237229">+918108237229</a>
          <br /> AKSHAY PHAPALE [COMPS] <a href="tel:+918655726027">+918655726027</a>
          <br /> RISHAB AGRAWAL [IT] <a href="tel:+918806119975">+918806119975</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="50">
    <dl> <dt>
          TRUSS
        </dt>
      <dd> Make a structure which can withstand maximum weight using the ice cream sticks provided by the organizers. </dd>
      <dd> Participation Fee: Rs.100(3) </dd>
      <dd> Venue: BACK FOYER </dd>
      <dd> 13/02/2015 10:00:00 </dd>
      <dd> 13/02/2015 13:00:00 </dd>
      <dd> Contact:
        <p> SHYAM S. (EH) [MECH] <a href="tel:+919619029685">+919619029685</a>
          <br /> ARPIT B. [ELEC] <a href="tel:+919870753854">+919870753854</a>
          <br /> SAYANA R. [EXTC] <a href="tel:+918898133792">+918898133792</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="51">
    <dl> <dt>
          UNKNOWN CODING
        </dt>
      <dd> Generate a simple program in an unknown language, the instruction set of which will be provided by the organisers on the spot. </dd>
      <dd> Participation Fee: Rs.50 </dd>
      <dd> Venue: INTERNET LAB </dd>
      <dd> 13/02/2015 09:00:00 </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> ASHESH KUMAR (EH) [IT] <a href="tel:+919833170924">+919833170924</a>
          <br /> PRAFUL NIKOSE [COMPS] <a href="tel:+919619340986">+919619340986</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="52">
    <dl> <dt>
          CONTRAPTIONS(ELIMINATIONS)
        </dt>
      <dd> Make a marble reach its target through a track you design and keep it rolling for the longest time. The materials will be provided by the organisers. </dd>
      <dd> Participation Fee: Rs.100(3) </dd>
      <dd> Venue: BACK FOYER </dd>
      <dd> 13/02/2015 13:30:00 </dd>
      <dd> 13/02/2015 16:00:00 </dd>
      <dd> Contact:
        <p> MANISH SHARMA (EH) [ELEC] <a href="tel:+919076953979">+919076953979</a>
          <br /> ASHISH SHELKE [MECH] <a href="tel:+918149131099">+918149131099</a>
          <br /> KAVIT K [EXTC] 09619583514 </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="53">
    <dl> <dt>
          ROAD RASH(FINALS)
        </dt>
      <dd> Don't be rash on the roads. </dd>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: FOYER( DEGREE) </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> FRANCIS COLACO (EH) [EXTC] <a href="tel:+919930059162">+919930059162</a>
          <br /> AKSHAY CHALKE [MECH] <a href="tel:+919403112129">+919403112129</a>
          <br /> AKSHAY MAHAJAN [ELEC] <a href="tel:+919975942452">+919975942452</a>
          <br /> HARDEEP SINGH [MECH] <a href="tel:+918082536590">+918082536590</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="54">
    <dl> <dt>
          KICK TO GOAL(FINALS)
        </dt>
      <dd> A game of football for your bot. The robot scoring maximum number of goals in limited time wins. </dd>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: FOYER(DEGREE) </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> RHEA JOHNSON (EH) [ELEC] <a href="tel:+919820609514">+919820609514</a>
          <br /> PARAS B. [MECH] <a href="tel:+918446677689">+918446677689</a>
          <br /> BHUSHAN KAMBLE [EXTC] <a href="tel:+917208869063">+917208869063</a>
          <br /> PRITESH TUPE [IT] <a href="tel:+918976577195">+918976577195</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="55">
    <dl> <dt>
          RUN FOR THE QUEEN(FINALS)
        </dt>
      <dd> Two bots will start from different starting points and will have to rescue the Queen which is located at the destination point. The bot which rescues the queen first wins the game. </dd>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: FOYER( DEGREE) </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> PRATHAMESH J. [ELEC] <a href="tel:+919833988491">+919833988491</a>
          <br /> SANKET KANADE (EH) [EXTC] <a href="tel:+919167426657">+919167426657</a>
          <br /> RAKSHIT OGRA [IT] <a href="tel:+919619517820">+919619517820</a>
          <br /> ANIKET DHARMADHIKARI [MECH] <a href="tel:+919096960239">+919096960239</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="56">
    <dl> <dt>
          SMACK DOWN(FINALS)
        </dt>
      <dd> Build a robot to oust your opponent out of the arena just by pushing your opponent. Active weapons not allowed. The bot should be able to carry its own weight. </dd>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: FOYER( DEGREE) </dd>
      <dd> 13/02/2015 11:00:00 </dd>
      <dd> 13/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> RANGARAJAN S(EH) [ELEC] <a href="tel:+917208329692">+917208329692</a>
          <br /> ABIN THOMAS [EXTC] <a href="tel:+919769660979">+919769660979</a>
          <br /> HIMALI MHATRE [ELEC] <a href="tel:+919503366307">+919503366307</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  
  <div class="remodal" data-remodal-id="58">
    <dl> <dt>
          YOGA AND MEDITATION
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 14/02/2015 06:00:00 </dd>
      <dd> 14/02/2015 08:00:00 </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="59">
    <dl> <dt>
          SELF DEFENCE
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: ED HALL </dd>
      <dd> 14/02/2015 09:00:00 </dd>
      <dd> 14/02/2015 11:00:00 </dd>
      <dd> Contact:
        <p> Surabhi Sawant. [IT]
          <br /> Sayalee Koli [ELEC] </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="60">
    <dl> <dt>
          GENERAL AWARENESS (GYNAECOLOGIST SESSION)***
        </dt>
      <dd> Participation Fee: Rs.50 </dd>
      <dd> Venue: CLASSROOM 110 </dd>
      <dd> 14/02/2015 11:30:00 </dd>
      <dd> 14/02/2015 12:30:00 </dd>
      <dd> Contact:
        <p> SHEETAL <a href="tel:+919619089617">+919619089617</a>
          <br /> VIDHYA G. <a href="tel:+919167440280">+919167440280</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="61">
    <dl> <dt>
          ENTREPRENEURSHIP WORKSHOP
        </dt>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: CR105 </dd>
      <dd> 14/02/2015 13:00:00 </dd>
      <dd> 14/02/2015 14:00:00 </dd>
      <dd> Contact:
        <p> PRABHAT M. <a href="tel:+919987137481">+919987137481</a> </p>
      </dd>
      <dd>group-B, cultural, workshop</a> </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="62">
    <a href="tel:+919987137481"></a>
    <dl> <dt>
          DANCE WORKSHOP
        </dt>
      <dd>It does not matter whether you like jazz or waltz, bharatnatyam or garba. What matters is that your heart loves to dance. Here you just need to Dance your way.</dd>
      <dd> Participation Fee: Rs.150(2) </dd>
      <dd> Venue: ED HALL </dd>
      <dd> 14/02/2015 13:30:00 </dd>
      <dd> 14/02/2015 15:30:00 </dd>
      <dd> Contact:
        <p> Aman M. [IT] <a href="tel:+919619871634">+919619871634</a>
          <br /> Ann M. Michael [COMPS] <a href="tel:+919920816749">+919920816749</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="63">
    <dl> <dt>
          MAGIC WORKSHOP
        </dt>
      <dd> Participation Fee: Rs.100 </dd>
      <dd> Venue: CLASSROOM 202 </dd>
      <dd> 14/02/2015 14:00:00 </dd>
      <dd> 14/02/2015 15:00:00 </dd>
      <dd>Contact:
        <p> RUTWICK <a href="tel:+919833944949">+919833944949</a>
          <br /> Ritesh Gogade [EXTC] <a href="tel:+918983432826">+918983432826</a> </p>
      </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="64">
    <dl> <dt>
          ELOCUTION
        </dt>
      <dd>Suit the action to the word, the word to the action, with this special observance, that you o'erstep not the modesty of nature. </dd>
      <dd> Participation Fee: Rs.50 </dd>
      <dd> Venue: FOYER </dd>
      <dd> 14/02/2015 15:30:00 </dd>
      <dd> 14/02/2015 16:30:00 </dd>
      <dd> Contact:
        <p> Utkarsh <a href="tel:+919833635920">+919833635920</a>
          <br /> Akshay Bawkar <a href="tel:+918097924079">+918097924079</a> </p>
      </dd>
      <dd> group-B, cultural, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="65">
    <dl> <dt>
          PERSONALITY DEVELOPMENT
        </dt>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: CR303 </dd>
      <dd> 14/02/2015 14:30:00 </dd>
      <dd> 14/02/2015 16:30:00 </dd>
      <dd> group-B, cultural, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="66">
    <dl> <dt>
          SOLO &amp; DUET SINGING(FINALS)
        </dt>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: LAWN </dd>
      <dd> 14/02/2015 17:00:00 </dd>
      <dd> 14/02/2015 18:30:00 </dd>
      <dd> Contact:
        <p> TERRENCE <a href="tel:+919920707850">+919920707850</a>
          <br /> MANSI M <a href="tel:+919833293240">+919833293240</a> </p>
      </dd>
      <dd> group-C, cultural, competition, evening, free to attend </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="67">
    <dl> <dt>
          JAM SESSION
        </dt>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: LAWN </dd>
      <dd> 14/02/2015 18:30:00 </dd>
      <dd> 14/02/2015 20:00:00 </dd>
      <dd> Contact:
        <p> UTKARSH <a href="tel:+919833635920">+919833635920</a>
          <br /> AKSHAY B <a href="tel:+918097924079">+918097924079</a>
          <br /> SAMRUDDHI K. [EXTC] <a href="tel:+919833273787">+919833273787</a>
          <br /> PRATAHAMESH S. [MECH] <a href="tel:+918080449466">+918080449466</a></p>
      </dd>
      <dd> group-C, ganpati dance, cultural, evening, free to attend </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="68">
    <dl> <dt>
          ECAD
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CADCAM </dd>
      <dd> 14/02/2015 09:00:00 </dd>
      <dd> 14/02/2015 12:00:00 </dd>
      <dd> Contact:
        <p> HARSHADA ARORA(EH) [IT] <a href="tel:+919702461547">+919702461547</a>
          <br /> HARSHAL PARMAR [COMPS] <a href="tel:+918691869059">+918691869059</a>
          <br /> KISHAN PATIL [MECH] <a href="tel:+918698049224">+918698049224</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="69">
    <dl> <dt>
          GUI PROCESSING
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CR305 </dd>
      <dd> 14/02/2015 09:00:00 </dd>
      <dd> 14/02/2015 12:00:00 </dd>
      <dd> Contact:
        <p> ASHITHA ANN (EH) [EXTC] <a href="tel:+919987543423">+919987543423</a>
          <br /> SAYALEE K. [ELEC] <a href="tel:+918454042779">+918454042779</a>
          <br /> AKSHAY P. [COMPS] <a href="tel:+918655726027">+918655726027</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="70">
    <dl> <dt>
          ANDROID DEVELOPMENT
        </dt>
      <dd> Let's make our own app. </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 14/02/2015 09:00:00 </dd>
      <dd> 14/02/2015 12:00:00 </dd>
      <dd> Contact:
        <p> DASHMIT(EH) [IT] <a href="tel:+919619995655">+919619995655</a>
          <br /> ABHILASHA [IT] <a href="tel:+919821814934">+919821814934</a>
          <br /> SHUBHAM PATIL [MECH] <a href="tel:+919819868414">+919819868414</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="71">
    <dl> <dt>
          AUTOMOBILE WORKSHOP
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: ITI LAB </dd>
      <dd> 14/02/2015 11:00:00 </dd>
      <dd> 14/02/2015 14:00:00 </dd>
      <dd> Contact:
        <p> RAGHAV U. (EH) [MECH] <a href="tel:+918655423989">+918655423989</a>
          <br /> PRANIT WAVHAL [IT] <a href="tel:+918655357922">+918655357922</a>
          <br /> DHIRENDRA PANDEY [ELEC] <a href="tel:+919768303798">+919768303798</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="72">
    <dl> <dt>
          PYTHON
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CR201 </dd>
      <dd> 14/02/2015 12:00:00 </dd>
      <dd> 14/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p>  PREETAM WALVEKAR (EH) [COMPS] <a href="tel:+918097976989">+918097976989</a>
        <br /> KANIKA [EXTC] <a href="tel:+919969023581">+919969023581</a>
          <br /> KRUNAL BONDE [EXTC] <a href="tel:+917208801292">+917208801292</a>
          <br /> AKASH S [IT] <a href="tel:+919619692531">+919619692531</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="73">
    <dl> <dt>
          WEB DESIGNING
        </dt>
      <dd> Let's make a few web pages first. </dd>
      <dd> Participation Fee: Rs.110 </dd>
      <dd> Venue: INTERNET LAB </dd>
      <dd> 14/02/2015 12:00:00 </dd>
      <dd> 14/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> SHUBHAM THAKUR (EH) [IT] <a href="tel:+919920416622">+919920416622</a>
          <br /> PIYUSH JAIN [MECH] <a href="tel:+919029278741">+919029278741</a>
          <br /> AKASH KUMBHARE [COMPS] <a href="tel:+919594684194">+919594684194</a>
          <br /> SHAILESH V. [ELEC] <a href="tel:+919768184051">+919768184051</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="74">
    <dl> <dt>
          ANSYS
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CADCAM </dd>
      <dd> 14/02/2015 13:00:00 </dd>
      <dd> 14/02/2015 16:00:00 </dd>
      <dd> Contact:
        <p> YOGESH LAD (EH) [MECH] <a href="tel:+919930745024">+919930745024</a>
          <br /> PRAKSHAL [IT] <a href="tel:+917588757555">+917588757555</a>
          <br /> SHUBHAM KAMBLE [ELEC] <a href="tel:+919967046654">+919967046654</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="75">
    <dl> <dt>
          PHOTOSHOP
        </dt>
      <dd> Editing various photos and creating a single photo by merging different photos is being taught using cs 5 photoshop software. </dd>
      <dd> Participation Fee: Rs.110 </dd>
      <dd> Venue: WIRELESS LAB </dd>
      <dd> 14/02/2015 13:00:00 </dd>
      <dd> 14/02/2015 15:00:00 </dd>
      <dd> Contact:
        <p> TARUN P. (EH) [IT] <a href="tel:+919967704804">+919967704804</a>
          <br /> FELIX [ELEC] <a href="tel:+917208226672">+917208226672</a>
          <br /> SUFIYAN [EXTC] <a href="tel:+919702052467">+919702052467</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="76">
    <dl> <dt>
          .NET
        </dt>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CR103 </dd>
      <dd> 14/02/2015 15:00:00 </dd>
      <dd> 14/02/2015 17:00:00 </dd>
      <dd> Contact:
        <p> SWATHI S. (EH) [IT] <a href="tel:+919769673479">+919769673479</a>
          <br /> PRIYANKA B. [EXTC] <a href="tel:+917303012024">+917303012024</a>
          <br /> TUSHAR K. [ELEC] <a href="tel:+918286023976">+918286023976</a> </p>
      </dd>
      <dd> group-A, technical, workshop </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="77">
    <dl> <dt>
          CRACK THE ALGO
        </dt>
      <dd> The problem statement and the sample inputs will be given. The output for this sample input is to be found out and the program is to be coded. </dd>
      <dd> Participation Fee: Rs.50 </dd>
      <dd> Venue: IT LAB 3 </dd>
      <dd> 14/02/2015 10:00:00 </dd>
      <dd> 14/02/2015 12:00:00 </dd>
      <dd> Contact:
        <p> GANESH S. (EH) [COMPS] <a href="tel:+918108706700">+918108706700</a>
          <br /> PARAG W. [IT] <a href="tel:+919969440994">+919969440994</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="78">
    <dl> <dt>
          CONTRAPTIONS(FINALS)
        </dt>
      <dd> Make a marble reach its target through a track you design and keep it rolling for the longest time. The materials will be provided by the organisers. </dd>
      <dd> Participation Fee: N/A </dd>
      <dd> Venue: BACK FOYER </dd>
      <dd> 14/02/2015 13:00:00 </dd>
      <dd> 14/02/2015 16:00:00 </dd>
      <dd> Contact:
        <p> MANISH SHARMA (EH) [ELEC] <a href="tel:+919076953979">+919076953979</a>
          <br /> ASHISH SHELKE [MECH] <a href="tel:+918149131099">+918149131099</a>
          <br /> KAVIT K. [EXTC] <a href="tel:+919619583514">+919619583514</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="79">
    <dl> <dt>
          LINE FOLLOWER
        </dt>
      <dd> A bot which would follow a black path. </dd>
      <dd> Participation Fee: Rs.160(4) </dd>
      <dd> Venue: CR204 </dd>
      <dd> 14/02/2015 13:00:00 </dd>
      <dd> 14/02/2015 16:00:00 </dd>
      <dd> Contact:
        <p> UNMESH MALI (EH) [EXTC] <a href="tel:+918983412780">+918983412780</a>
          <br /> DEEPAK AILANI [MECH] <a href="tel:+919833447077">+919833447077</a>
          <br /> PRAMOD YADAV [ELEC] <a href="tel:+919702633926">+919702633926</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="80">
    <dl> <dt>
          TPP
        </dt>
      <dd> Present your paper. </dd>
      <dd> Participation Fee: Rs.160 </dd>
      <dd> Venue: CR305 </dd>
      <dd> 14/02/2015 09:30:00 </dd>
      <dd> 14/02/2015 12:30:00 </dd>
      <dd> Contact:
        <p> TEJAL J. (EH) [EXTC] <a href="tel:+917506498574">+917506498574</a>
          <br /> SANKET NALAVDE [IT] <a href="tel:+919028602182">+919028602182</a>
          <br /> TANMAY D. [ELEC] <a href="tel:+919930078276">+919930078276</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="81">
    <dl> <dt>
          JUNKYARD WARS
        </dt>
      <dd> Solve the problem </dd>
      <dd> Participation Fee: Rs.200(3) </dd>
      <dd> Venue: BACK FOYER </dd>
      <dd> 14/02/2015 09:00:00 </dd>
      <dd> 14/02/2015 12:00:00 </dd>
      <dd> Contact:
        <p> DENISE (EH) [ELEC] <a href="tel:+919769720464">+919769720464</a>
          <br /> QAIF FANGARI [MECH] <a href="tel:+919167923387">+919167923387</a>
          <br /> TANAYA T. [EXTC] <a href="tel:+918898967199">+918898967199</a> </p>
      </dd>
      <dd> group-B, technical, competition </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="82">
    <dl> <dt>
          EXTC DEPARTMENTAL SEMINAR
        </dt>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 14/02/2015 09:00:00 </dd>
      <dd> 14/02/2015 13:00:00 </dd>
      <dd> group-A, departmental seminar </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="57">
    <dl> <dt>
          MECHANICAL DEPARTMENTAL SEMINAR
        </dt>
      <dd> seminar time! </dd>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: CR310 </dd>
      <dd> 14/02/2015 10:00:00 </dd>
      <dd> 14/02/2015 1:00:00 </dd>
      <dd> group-A, departmental seminar </dd>
    </dl>
  </div>
  <div class="remodal" data-remodal-id="83">
    <dl> <dt>
          ELECTRICAL DEPARTMENTAL SEMINAR
        </dt>
      <dd> Participation Fee: FREE. </dd>
      <dd> Venue: SEMINAR HALL </dd>
      <dd> 14/02/2015 14:00:00 </dd>
      <dd> 14/02/2015 16:00:00 </dd>
      <dd> group-A, departmental seminar </dd>
    </dl>
  </div>
  
  <div class="remodal" data-remodal-id="test">
    <dl> <dt>
          EXAMPLE EVENT
        </dt>
      <dd> Participation Fee: Rs. | FREE | N/A</dd>
      <dd> Venue: </dd>
      <dd> DD/MM/YYYY HH:MM:00 [start time]</dd>
      <dd> DD/MM/YYYY HH:MM:00 [end time]</dd>
      <dd> Contact:
        <p> RAM (EH)<a href="tel:+911234567890">+911234567890</a>
        <br /> SHYAM <a href="tel:+911234567890">+911234567890</a>
        <br /> GHANSHYAM <a href="tel:+911234567890">+911234567890</a>
      </dd>
      <dd> group-[A | B | C], [technical | cultural | departmental seminar], [competition | workshop] </dd>
    </dl>
  </div>


    
   

@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery-1.11.2.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.remodal.min.js')}}
{{ HTML::script('js/jsfrontend/easyResponsiveTabs.min.js')}}
{{ HTML::script('js/jsfrontend/modernizr.custom.js')}}
   <script type="text/javascript">
    //<![CDATA[
    window.jQuery || document.write('<script src="libs/jquery/dist/jquery.min.js"><\/script>')
      //]]>
  </script>
  <script src="js/jquery.remodal.min.js" type="text/javascript"></script>
  <!--Plug-in Initialisation-->
  <script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {
      //Vertical Tab
      $('#parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
          var $tab = $(this);
          var $info = $('#nested-tabInfo2');
          var $name = $('span', $info);
          $name.text($tab.text());
          $info.show();
        }
      });
    });
    //]]>
  </script>
  <!-- Events -->
  <script type="text/javascript">
    //<![CDATA[
    $(document).on("open", ".remodal", function() {
      console.log("open");
    });
    $(document).on("opened", ".remodal", function() {
      console.log("opened");
    });
    $(document).on("close", ".remodal", function(e) {
      console.log('close' + (e.reason ? ", reason: " + e.reason : ''));
    });
    $(document).on("closed", ".remodal", function(e) {
      console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
    });
    $(document).on("confirm", ".remodal", function() {
      console.log("confirm");
    });
    $(document).on("cancel", ".remodal", function() {
      console.log("cancel");
    });
    //    You can open or close it like this:
    //    $(function () {
    //        var inst = $.remodal.lookup[$("[data-remodal-id=modal]"").data("remodal")];
    //        inst.open();
    //        inst.close();
    //    });
    //  Or init in this way:
    var inst = $("[data-remodal-id=modal2]").remodal();
    //  inst.open();
    //]]>
  </script>



@stop

