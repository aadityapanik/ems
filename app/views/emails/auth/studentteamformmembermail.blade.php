@extends('layouts.emailmaster')
@section('body')<div class="col-xs-8 col-xs-offset-2 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 jumbotron">
<h2>Hello,</h2><br>
<p>Team-Leader : {{ $tlrollno }}</p><br>
<p>Your Team ID: {{ $teamid }} for the event {{ $event_name }} on day {{ $event_day }}.</p>  <p>Following are the team-members :</p><br>
<ul>
  <li>{{$tlrollno}}</li>
  @for ($i=1;$i<=$membercount;$i++)
    <li>{{ $teammembers[$i] }}</li>                                 
  @endfor
</ul>
<br>
<p>Total amount for the team: {{$totalamount}} </p><br>
<p>Individual amount: {{$individualamount}}</p><br>
</div>
@stop