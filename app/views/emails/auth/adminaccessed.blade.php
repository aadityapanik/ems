@extends('layouts.emailmaster')
@section('body')
<h1>Hello {{$Admin_FN}}</h1>
<p>Your account was accessed at {{ Carbon::now()->format('l, jS F Y \\a\\t h:i:s A'); }}..I get a strange feeling this isn't you :/</p>
@stop