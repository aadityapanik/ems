@extends('emailmaster.blade.php')
@section('body')
<h1>ETAMAX</h1>
<br><br>
<h2>Hello {{$student->Student_FN}}</h2>

<p>Your Unique ID : <b>{{$student->Student_ID}}</b></p>
<br>
<p>This unique ID is used to confirm that this mail has been sent by the ETAMAX Team only.</p>
<br>
<p>You have been sent mail from CCAvenue regarding the payment for ETAMAX. The mail will contain your Order ID (Used for Accounts purposes by our College) and a Tracking ID (Used by Banks to monitor the transaction). The mail will confirm the payment of the registered amount. The events you have been charged for are as follows : </p>
<ol>
	@foreach($regevents as $regevent)
		<li>{{$regevent->Event_Name}} ({{$regevent->Event_ID}})
			<ul>
				<li>Event Date - {{$regevent->Event_Date}}</li>
				<li>Event Start Time - {{$regevent->Event_Start_Time}}</li>
				<li>Event End Time - {{$regevent->Event_End_Time}}</li>
			</ul>
		</li>
	@endforeach
</ol><br>
<p>Have a nice time :)</p>
@stop