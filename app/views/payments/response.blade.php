@extends('layouts.master')
@section('style')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
@stop

@section('header')
@include('header')
@stop

@section('body')
	<div class="intro">
		<div class="container" style="padding-top:60px;padding-left:100px">
				<table class="table">
					<tr><td>Order ID:</td><td>{{$order_dtls->order_no}}</td></tr>
					<tr><td>Tracking ID:</td><td>{{$order_dtls->tracking_id}}</td></tr>
					<tr><td>Bank Reference No:</td><td>{{$order_dtls->bank_ref_no}}</td></tr>
					<tr><td>Order Status:</td><td>@if($order_dtls->order_status == 1) Success @elseif ($order_dtls->order_status == 2) Aborted/Cancelled @elseif($order_dtls->order_status == 3) Failure @else Processing @endif</td></tr>
					@if($order_dtls->order_status == 3)<tr><td>Failure Message:</td><td>{{$order_dtls->failure_msg}}</td></tr>@endif
					<tr><td>Amount:</td><td>Rs. {{$order_dtls->amount}}</td></tr>
				</table>
				<a class="btn btn-primary" href="{{URL::to('student/viewregistrations')}}" role="button">Views Registered Events</a>
		</div>	
	</div>
				
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}
{{ HTML::script('js/jsfrontend/main.js')}}
@stop