@extends('layouts.master')

@section('style')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
@stop

@section('header')
@include('header')
@stop

@section('body')
<div class="intro">
	<div class="container" style="padding-top:80px;padding-left:60px">
			<table class="table">
				<tr><td>Order ID:</td><td>{{$order_id}}</td></tr>
				<tr><td>Tracking ID:</td><td>{{$tracking_id}}</td></tr>
				<tr><td>Bank Reference No:</td><td>{{$bank_ref_no}}</td></tr>
				<tr><td>Order Status:</td><td>{{$order_status}}</td></tr>
				<tr><td>Failure Message:</td><td>{{$failure_message}}</td></tr>
				<tr><td>Amount:</td><td>Rs. {{$amount}}</td></tr>
			</table>
	</div>
</div>
@stop


@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}
{{ HTML::script('js/jsfrontend/main.js')}}
@stop