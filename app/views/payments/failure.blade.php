@extends('layouts.master')
@section('style')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
@stop

@section('header')
@include('header')
@stop

@section('body')
	<div class="intro">
		<div class="intro-body" style="padding-top:100px">
	@if($order_id)<p>Transaction failed! Please contact etamax@eventsfcrit.in with the order number as {{$order_id}}</p>
	@else
	<p>Transaction failed! Please contact etamax@eventsfcrit.in</p>
	@endif
@stop
		</div>
	</div>	

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}
{{ HTML::script('js/jsfrontend/main.js')}}
@stop
