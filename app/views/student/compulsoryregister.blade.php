@extends('layouts.master')
@section('header')
@include('student.studentheadernew')
@stop
@section('body')
<!-- <div class="page-header"><h2>COMPULSORY EVENTS REGISTRATION</h2></div>
<div class="jumbotron">
  <div class="container"> -->
<header class="intro">
   <div class="container">
     <h2 >COMPULSORY REGISTRATION</h2>
     <div class="col-md-4 col-md-push-4">
       <div class="slogan">
         <div style="padding-top: 30px">

            @if($errors->test->has())
                        @foreach($errors->test->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
            @if($errors->FEOnly->has())
                        @foreach($errors->FEOnly->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->SeatCount->has())
                        @foreach($errors->SeatCount->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->OnlyGirlsEvent->has())
                        @foreach($errors->OnlyGirlsEvent->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->DeptSeminar->has())
                        @foreach($errors->DeptSeminar->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
          
           @if($errors->OnlyBoysEvent->has())
                        @foreach($errors->OnlyBoysEvent->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->OnlyMechEvent->has())
                        @foreach($errors->OnlyMechEvent->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->OnlyCompITEvent->has())
                        @foreach($errors->OnlyCompITEvent->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->OnlyElecEvent->has())
                        @foreach($errors->OnlyElecEvent->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->NoMechEvent->has())
                        @foreach($errors->NoMechEvent->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->CompulsoryPDDebateElocution->has())
                        @foreach($errors->CompulsoryPDDebateElocution->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
           @if($errors->GroupFormError->has())
                        @foreach($errors->GroupFormError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
          
           @if($errors->EventHeadError->has())
                        @foreach($errors->EventHeadError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif
            @if($errors->Others->has())
                        @foreach($errors->Others->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach
           @endif


  

           
           

{{ Form::open(array('url'=>'student/compulsoryregister','method'=>'post','class'=>'form-group')) }}
      {{ Form::label('day1','day1') }}
      {{ Form::select('day1',array('Personality Development(Day 1)' => 'Personality Development(Day 1)',
    'groupA' =>array(
    'Autonomous Robotics(Day 1)' => 'Autonomous Robotics(Day 1)',
      'Ansys(Day 1)' => 'Ansys(Day 1)',
      'Automobile Workshop(Day 1)'=>'Automobile Workshop(Day 1)',
      'Matlab(Day 1)'=>'Matlab(Day 1)',
      'Photoshop(Day 1)'=>'Photoshop(Day 1)',
      'Web Designing(Day 1)'=>'Web Designing(Day 1)',
      '.Net(Day 1)' => '.Net(Day 1)',
      'Computer Departmental Seminar'=>'Computer Departmental Seminar',
      'IT Departmental Seminar' =>'IT Departmental Seminar',
           'Humanities Departmental Seminar' =>'Humanities Departmental Seminar'),

    'groupB' =>array('Yoga And Meditation-Only Boys(Day 1)' => 'Yoga And Meditation-Only Boys(Day 1)',
      'Yoga And Meditation-Only Girls(Day 1)' => 'Yoga And Meditation-Only Girls(Day 1)',
       'Graffitti Workshop(Day 1)' => 'Graffitti Workshop(Day 1)',
       'Debate'=>'Debate',
       'Film-Making Workshop(Day 1)' => 'Film-Making Workshop(Day 1)',    
       'Cup Cake Making(Day 1)' => 'Cup Cake Making(Day 1)',
       'Magic Workshop(Day 1)' => 'Magic Workshop(Day 1)',
       'Enterpreneurship Workshop(Day 1)' => 'Enterpreneurship Workshop(Day 1)',
       'Road Rash(Elims)' => 'Road Rash(Elims)',
       'Kick To Goal(Elims)' =>'Kick To Goal(Elims)',
       'Run For The Queen(Elims)' => 'Run For The Queen(Elims)',
       'Smack Dowm(Elims)' => 'Smack Dowm(Elims)',
       'Magneto' => 'Magneto',
           'Online Tech Hunt' => 'Online Tech Hunt'),

    'groupC '=>array('War Of Bands(Audience)' => 'War Of Bands(Audience)',
           'War Of Bands(Participant)' => 'War Of Bands(Participant)'))) }}
      {{Form::label('teamid1','Enter your Team-Id for selected day1 event(if group-event):')}}
      {{Form::text('teamid1',null,array('placeholder'=>'Enter Team-ID for this event','class'=>'form-control'))}}
<br>
               {{ Form::label('day2','day2') }}
               {{ Form::select('day2',array('Personality Development(Day 2)' => 'Personality Development(Day 2)',
    'groupA' =>array('ECAD(Day 2)' => 'ECAD(Day 2)',
      'Automobile Workshop(Day 2)'=>'Automobile Workshop(Day 2)',
       'Android Development(Day 2)'=>'Android Development(Day 2)',
      'Matlab(Day 2)'=>'Matlab(Day 2)',
      'Photoshop(Day 2)'=>'Photoshop(Day 2)',
      'Web Designing(Day 2)'=>'Web Designing(Day 2)',
      'Ethical Hacking' => 'Ethical Hacking',
      'Python(Day 2)' => 'Python(Day 2)',
      'Graphical Processing(Day 2)' => 'Graphical Processing(Day 2)'
           ),

    'groupB' =>array('Yoga And Meditation-Only Boys(Day 2)' => 'Yoga And Meditation-Only Boys(Day 2)',
      'Yoga And Meditation-Only Girls(Day 2)' => 'Yoga And Meditation-Only Girls(Day 2)',
       'Graffitti Workshop(Day 2)' => 'Graffitti Workshop(Day 2)',
       'Cup Cake Making(Day 2)' => 'Cup Cake Making(Day 2)',
       'Photography' => 'Photography',
       'AD Making' => 'AD Making',
       'DJ Sound Mixing' => 'DJ Sound Mixing',
       'Reverse Engineering' => 'Reverse Engineering',
       'Truss' => 'Truss',
      'Contraptions' => 'Contraptions',
       'Unknown Coding' => 'Unknown Coding',
       ),

    'groupC '=>array('Group Dance(Audience)' => 'Group Dance(Audience)','Group Dance(Participant)' => 'Group Dance(Participant)'))) }}

      {{Form::label('teamid2','Enter your Team-Id for selected day2 event(if group-event):')}}
      {{Form::text('teamid2',null,array('placeholder'=>'Enter Team-ID for this event','class'=>'form-control'))}}
<br>
      {{ Form::label('day3','day3') }}
          {{ Form::select('day3',array('Personality Development(Day 3)' => 'Personality Development(Day 3)',
    'groupA' =>array('ECAD(Day 3)' => 'ECAD(Day 3)',
       'Graphical Processing(Day 3)' => 'Graphical Processing(Day 3)',
       'Android Development(Day 3)'=>'Android Development(Day 3)',
       'Automobile Workshop(Day 3)'=>'Automobile Workshop(Day 3)',
       'Python(Day 3)' => 'Python(Day 3)',
       'Photoshop(Day 3)'=>'Photoshop(Day 3)',
       'Web Designing(Day 3)'=>'Web Designing(Day 3)',
       'Ansys(Day 3)' => 'Ansys(Day 3)',
       '.Net(Day 3)' => '.Net(Day 3)',
       'EXTC Departmental Seminar' => 'EXTC Departmental Seminar',
           'Electrical Departmental Seminar'=>'Electrical Departmental Seminar',
      'Mechanical Departmental Seminar' => 'Mechanical Departmental Seminar'
      ),

    'groupB' =>array('Yoga And Meditation-Only Boys(Day 3)' => 'Yoga And Meditation-Only Boys(Day 3)',
      'Yoga And Meditation-Only Girls(Day 3)' => 'Yoga And Meditation-Only Girls(Day 3)',
      'Self Defence' => 'Self Defence',
      'General Awareness(Gynaecology)' => 'General Awareness(Gynaecology)',
      'Enterpreneurship Workshop(Day 3)' => 'Enterpreneurship Workshop(Day 3)',
      'Magic Workshop(Day 3)' => 'Magic Workshop(Day 3)',
      'Elocution' => 'Elocution',
      'Line Follower' => 'Line Follower',
      'TPP' => 'TPP',
      'Crack The Algo' =>'Crack The Algo',
      'Junkyard Wars' => 'Junkyard Wars'),

    'groupC '=>array('Jam Session And Solo And Duet Singing Finals' => 'Jam Session And Solo And Duet Singing Finals',
     ))) }}



      {{Form::label('teamid3','Enter your Team-Id for selected day3 event(if group-event):')}}
      {{Form::text('teamid3',null,array('placeholder'=>'Enter Team-ID for this event','class'=>'form-control'))}}
<br>

<!-- <input type="submit" name="noncomp" value="Register for Non-Compulsory Events" class="btn btn-info"> -->
<!--<input type="submit" name="payment" value="Payment"  class="btn btn-info">-->
{{ Form::submit('Payment',array('class'=>'btn btn-primary'))}} 
                
{{ Form::close() }}
                 
          </div>
        </div>
     </div>
  </div>
</header>
@stop


 


@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop
        