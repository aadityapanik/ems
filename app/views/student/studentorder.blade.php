@extends('layouts.master')
@section('style')
	{{ HTML::style('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css') }}
@stop
@section('header')
	@include('student.studentheadernew')
@stop
@section('body')
@if($regevents->count()<0)
	<h2 style="text-align:center;padding-top:60px">No Events Registered</h2>
@else
	<h2 style="text-align:center;padding-top:60px;padding-left:10px;padding-right:10px"> List of Events Registered:</h2>
    <h2 style="text-align:center;padding-top:10px;padding-left:10px;padding-right:10px"> Event Receipt:</h2>
    
	<div class="intro" style="padding-top: 30px">
	<table id="orderstable" class="table table-hover" style="text-align: center;">
		<thead>
  			<tr>
	    		<th>Order Number</th>
			    <th>Event Name</th>
			    <th>Event Date</th>
			    <th>Start Time</th>
			    <th>Event Amount</th>
			    <th>Status</th>
  			</tr>
  		</thead>
  		<tbody>
  		<?php $finalamount=0;
		$teamids=Session::get('teamids');?>

  		@foreach($regevents as $regevent)
  			<tr>
    			<td>{{$regevent->pivot->order_id}}</td>
			    <td>{{$regevent->Event_Name}}</td>
			    <td>{{$regevent->Event_Date}}</td>
			    <td>{{Carbon::parse($regevent->Event_Start_Time)->format('h:i A')}}</td>
				@if($regevent->Group_Event_Type == 'I')
    				<td>Rs. {{$regevent->Event_Amount}}</td>
   			    @endif
    			@if($regevent->Group_Event_Type == 'G')
      				@foreach($teamids as $teamid)
          				
          				@if($regevent->Event_ID == DB::table('Teamform')->where('teamid','=',$teamid)->pluck('Eventid'))
             				
              				<td>Rs. {{ DB::table('Teamform')->where('teamid','=',$teamid)->pluck('individualamount') }}</td>
          				
          				@endif
      				@endforeach
    			@endif
    
			   @if($regevent->pivot->paystatus==0)
					<td>Pending...</td>
				@elseif($regevent->pivot->paystatus==1)
					<td>Success</td>
				@elseif($regevent->pivot->paystatus==2)
					<td>Aborted</td>
                @elseif($regevent->pivot->paystatus==3)
                    <td>Failed</td>
				@endif
  			</tr>
            @if($regevent->Group_Event_Type == 'I')
  			   <?php $finalamount=$finalamount+$regevent->Event_Amount; ?>
            @elseif($regevent->Group_Event_Type == 'G') 
                @foreach($teamids as $teamid)
          				
          				@if($regevent->Event_ID == DB::table('Teamform')->where('teamid','=',$teamid)->pluck('Eventid'))
             				   <?php $finalamount=$finalamount+DB::table('Teamform')->where('teamid','=',$teamid)->pluck('individualamount'); ?>
              				
          				@endif
   				@endforeach
            
            @endif
  		@endforeach
  </tbody>
</table>
<h2>Total Amount: Rs. {{$finalamount}}</h2>
</div>
@endif
@stop
@section('footer')
{{ HTML::script('http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js') }}
{{ HTML::script('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js') }}
<script>
  $(document).ready(function() {
    $('#orderstable').DataTable();
  });
</script>
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop