@extends('layouts.master')
@section('style')
{{ HTML::style('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css') }}
@stop

@section('header')
@include('student.studentheadernew')
@stop
@section('body')
<h2 style="padding-top:120px;text-align:center"> List of selected Compulsory Events :</h2>
<div class="intro" style="padding-top: 30px">
<a class="btn btn-primary" href="{{URL::to('payment/processing/init')}}" style="margin-left:600px">Make Payment</a>
<table id="eventtable" class="table table-hover" style="text-align: center;">
  <thead>
  <tr>
    <th>Event ID</th>
    <th>Event Name</th>
    <th>Event Day</th>
    <th>Event Amount</th>
    <th>Event Date</th>
    <th>Start Time</th>
    <th>End Time</th>
    <th>Event Type</th>
    <th>Group Event Type</th>
  </tr>
  </thead>
  
  <tbody>
  <?php 
  $events=Session::get('events');
  $teamids=Session::get('teamids');?>
  @foreach($events as $event)
    <td>{{$event->Event_ID}}</td>
    <td>{{$event->Event_Name}}</td>
    <td>{{$event->Event_Day}}</td>
    @if($event->Group_Event_Type=='I')
    <td>{{$event->Event_Amount}}</td>
    @endif
    @if($event->Group_Event_Type == 'G')
      @foreach($teamids as $teamid)
          
          @if($event->Event_ID == DB::table('Teamform')->where('teamid','=',$teamid)->pluck('Eventid') )
              
              <td>{{ DB::table('Teamform')->where('teamid','=',$teamid)->pluck('individualamount') }}</td>
         
          @endif
      @endforeach
    @endif
    <td>{{Carbon::parse($event->Event_Date)->format('jS F, Y')}}</td>
    <td>{{Carbon::parse($event->Event_Start_Time)->format('h:i A')}}</td>
    <td>{{Carbon::parse($event->Event_End_Time)->format('h:i A')}}</td>
    <td>{{$event->Event_Type}}</td>
    <td>{{$event->Group_Event_Type}}</td>
  </tr>
  @endforeach
  </tbody>
</table>
<h2>Total Amount : Rs. {{Session::get('finalamount')}}</h2>
</div>

@stop

@section('footer')
{{ HTML::script('http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js') }}
{{ HTML::script('http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js') }}
<script>
  $(document).ready(function() {
    $('#eventtable').DataTable();
  });
</script>
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop