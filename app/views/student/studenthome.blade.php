@extends('layouts.master')
<!-- @section('style')
    <style>
        .dropdown-menu {
            width: 300px !important;
            padding: 10px 10px 0px 10px;
        }
    </style>
@stop -->
@section('header')
@include('student.studentheadernew')
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
{{ HTML::style('css/cssfrontend/main.css')}}

@stop
@section('body')
    <header class="intro">
      <div class="intro-body col-sm-8 col-sm-offset-2">
       
     <div style="padding-top: 30px;letter-spacing:5px">
        <h1 style="font-size:80px;text-align:center">EMS</h1>
        @if(Auth::student()->check())
            <p>Hello Student {{Auth::student()->get()->Student_FN}}</p>
        @endif
        <p>
        <ul style="text-align:left; font-size:30px;line-height:60px">
        The Online Registrations have been temporarly stopped. Please be patient.
       <!-- <li>If you have not registered even once till now, you need to register. </li>
        
         <li>If your status shows ABORTED or FAILED, You MUST Register Again.</li>
        <li>If the Order Status shown by visting the SHOW HISTORY on Student ACTIONS is as follows: </li>
        <li> If you receieved the mail as Transaction Successful, and still shows Pending..., then you are registered. Dont Worry!!</li>
      <li>If you Don't receive mail, and it shows Pending, then you are registered for that event, its payment will be SETTLED LATER. Don't try to register Again. </li>
       -->
        </ul>
        </p>
    </div>
        </div>
     
</header>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}
@stop
