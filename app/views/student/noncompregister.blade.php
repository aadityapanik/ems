@extends('layouts.master')
@section('header')
@include('student.studentheadernew')
@stop
@section('body')
  
<header class="intro">
   <div class="container">
     <h2 >NON-COMPULSORY EVENTS REGISTRATION</h2>
     <div class="col-md-4 col-md-push-4">
       <div class="slogan">
         <div style="padding-top: 30px">

             @if($errors->test->has())
                        @foreach($errors->test->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
           @endif
          
            @if($errors->IdentityError->has())
                        @foreach($errors->IdentityError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
           @endif
           @if($errors->OtherError->has())
                        @foreach($errors->OtherError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
           @endif
            @if($errors->CompulsoryFirst->has())
                        @foreach($errors->CompulsoryFirst->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
           @endif
           
           
            @if(Session::has('message'))
                <div class="alert alert-danger" role="alert">
                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                      <span class="sr-only"></span>
                      {{{ Session::pull('message') }}}
                </div>
           @endif
           
           
           
           
           
           
            {{ Form::open(array('url'=>'student/noncompregister','method'=>'post','class'=>'form-group')) }}

            {{ Form::label('event_name','Select Event:') }}
            {{ Form::select('event_name',array(
    
    'day 1' => array(
           'Dance Workshop:Salsa(Day 1)' => 'Dance Workshop:Salsa(Day 1)',
      'Singing(Elims-duet)' => 'Singing(Elims-duet)',
      'Singing(Elims-solo)' => 'Singing(Elims-solo)',
     'Autonomous Robotics(Day 1)' => 'Autonomous Robotics(Day 1)',
      'Ansys(Day 1)' => 'Ansys(Day 1)',
      'Automobile Workshop(Day 1)'=>'Automobile Workshop(Day 1)',
      'Matlab(Day 1)'=>'Matlab(Day 1)',
      'Photoshop(Day 1)'=>'Photoshop(Day 1)',
      'Web Designing(Day 1)'=>'Web Designing(Day 1)',
      '.Net(Day 1)' => '.Net(Day 1)',
      'Embedded Systems'=>'Embedded Systems',
      'Computer Departmental Seminar'=>'Computer Departmental Seminar',
      'IT Departmental Seminar' =>'IT Departmental Seminar',
       'Humanities Departmental Seminar' =>'Humanities Departmental Seminar',
    'Yoga And Meditation-Only Boys(Day 1)' => 'Yoga And Meditation-Only Boys(Day 1)',
      'Yoga And Meditation-Only Girls(Day 1)' => 'Yoga And Meditation-Only Girls(Day 1)',
       'Graffitti Workshop(Day 1)' => 'Graffitti Workshop(Day 1)',
       'Debate'=>'Debate',
       'Film-Making Workshop(Day 1)' => 'Film-Making Workshop(Day 1)',    
       'Cup Cake Making(Day 1)' => 'Cup Cake Making(Day 1)',
       'AD Making' => 'AD Making',
       'Magic Workshop(Day 1)' => 'Magic Workshop(Day 1)',
       'Enterpreneurship Workshop(Day 1)' => 'Enterpreneurship Workshop(Day 1)',
       'Road Rash(Elims)' => 'Road Rash(Elims)',
       'Kick To Goal(Elims)' =>'Kick To Goal(Elims)',
       'Run For The Queen(Elims)' => 'Run For The Queen(Elims)',
       'Smack Dowm(Elims)' => 'Smack Dowm(Elims)',
       'Magneto' => 'Magneto',
       'Online Tech Hunt' => 'Online Tech Hunt',
    'War Of Bands(Participant)' => 'War Of Bands(Participant)'
           ),
           
            'day 2' => array(
           'Dance Workshop:Rock-N-Roll(Day 2)' =>  'Dance Workshop:Rock-N-Roll(Day 2)',
           'Street Play(Day 2)' => 'Street Play(Day 2)',
           'ECAD(Day 2)' => 'ECAD(Day 2)',
      'Automobile Workshop(Day 2)'=>'Automobile Workshop(Day 2)',
       'Android Development(Day 2)'=>'Android Development(Day 2)',
      'Matlab(Day 2)'=>'Matlab(Day 2)',
      'Photoshop(Day 2)'=>'Photoshop(Day 2)',
      'Web Designing(Day 2)'=>'Web Designing(Day 2)',
      'Ethical Hacking' => 'Ethical Hacking',
      'Python(Day 2)' => 'Python(Day 2)',
      'Graphical Processing(Day 2)' => 'Graphical Processing(Day 2)',
      'Yoga And Meditation-Only Boys(Day 2)' => 'Yoga And Meditation-Only Boys(Day 2)',
      'Yoga And Meditation-Only Girls(Day 2)' => 'Yoga And Meditation-Only Girls(Day 2)',
       'Graffitti Workshop(Day 2)' => 'Graffitti Workshop(Day 2)',
       'Cup Cake Making(Day 2)' => 'Cup Cake Making(Day 2)',
       'Photography' => 'Photography',
       'DJ Sound Mixing' => 'DJ Sound Mixing',
       'Reverse Engineering' => 'Reverse Engineering',
       'Truss' => 'Truss',
       'Contraptions' => 'Contraptions',
       'Unknown Coding' => 'Unknown Coding',
       'Group Dance(Participant)' => 'Group Dance(Participant)'
                     ),
            'day 3' => array(
           'Dance Workshop:Jive(Day 3)' =>  'Dance Workshop:Jive(Day 3)',
           'ECAD(Day 3)' => 'ECAD(Day 3)',
       'Graphical Processing(Day 3)' => 'Graphical Processing(Day 3)',
       'Android Development(Day 3)'=>'Android Development(Day 3)',
       'Automobile Workshop(Day 3)'=>'Automobile Workshop(Day 3)',
       'Python(Day 3)' => 'Python(Day 3)',
       'Photoshop(Day 3)'=>'Photoshop(Day 3)',
       'Web Designing(Day 3)'=>'Web Designing(Day 3)',
       'Ansys(Day 3)' => 'Ansys(Day 3)',
       '.Net(Day 3)' => '.Net(Day 3)',
       'EXTC Departmental Seminar' => 'EXTC Departmental Seminar',
           'Electrical Departmental Seminar'=>'Electrical Departmental Seminar',
       'Mechanical Departmental Seminar' => 'Mechanical Departmental Seminar',
           'Yoga And Meditation-Only Boys(Day 3)' => 'Yoga And Meditation-Only Boys(Day 3)',
      'Yoga And Meditation-Only Girls(Day 3)' => 'Yoga And Meditation-Only Girls(Day 3)',
      'Self Defence' => 'Self Defence',
      'General Awareness(Gynaecology)' => 'General Awareness(Gynaecology)',
      'Enterpreneurship Workshop(Day 3)' => 'Enterpreneurship Workshop(Day 3)',
      'Magic Workshop(Day 3)' => 'Magic Workshop(Day 3)',
      'Elocution' => 'Elocution',
      'Line Follower' => 'Line Follower',
      'TPP' => 'TPP',
      'Crack The Algo' =>'Crack The Algo',
      'Junkyard Wars' => 'Junkyard Wars',
           
           )
           
           
           )) }}<br>
            {{Form::label('teamid','Enter your Team-Id for selected event(if group-event):')}}
            {{Form::text('teamid',null,array('placeholder'=>'Enter Team ID','class'=>'form-control'))}}
            <br>                      
            <br>
            <input type="submit" name="morenoncomp" value="Register for More Non-Compulsory Events" class="btn btn-info">
            <input type="submit" name="payment" value="Payment" class="btn btn-info">
            {{ Form::close() }}
         </div>
       </div>
     </div>
  </div>
</header>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop
        