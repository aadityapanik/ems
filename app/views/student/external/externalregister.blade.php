@extends('layouts.master')
@section('header')
@include('student.studentheader')
@stop
@section('body')

@if(Session::has('SuccessMessage'))
                <div class="alert alert-success" role="alert">
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span class="sr-only"></span>
                      {{{ Session::pull('SuccessMessage') }}}
                </div>
@endif
           
<h1>EXTERNAL STUDENTS REGISTRATION</h1>
<p> A Hearty Welcome to you </p>
<p> DISCLAIMER:If you are registering for a group-event that includes more than one student, <b>only ONE student</b> can register for the entire group. You will have to show the receipt you receive on your email-id</p> 

      {{ Form::open(array('url'=>'external/register','class'=>'form-group')) }}
                                  {{ Form::label('firstname','Enter Firstname:') }}
                                  {{ Form::text('firstname',null,array('class'=>'form-inline')) }}<br>
                                  {{ Form::label('lastname','Enter Lastname:') }}
                                  {{ Form::text('lastname') }}<br>
                                  {{ Form::label('email','Enter Email-ID:') }}
                                  {{ Form::email('email') }}<br>

                                  {{ Form::label('contact','Enter Contact:') }}
                                  {{ Form::text('contact') }}<br>

                                  {{ Form::label('event','Select Event:') }}
                                  {{ Form::select('event',array('personality_development' => 'personality_development','t1' => 't1', 'c1' =>   'c1','e1' => 'e1')) }}

                            {{ Form::submit('Submit',array('class'=>'btn btn-primary')) }}
                           
                            {{ Form::close() }}

@stop