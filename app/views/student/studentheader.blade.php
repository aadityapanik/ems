<nav class="navbar navbar-inverse" style="margin-top: 1%;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ETAMAX</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{URL::to('student/')}}">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{{URL::to('external/register')}}">Not an Agnelite ?</a></li>
                @if(!Auth::student()->check())
              <li @if($errors->signup->has()||Session::has('messagesuccess'))class="dropdown open" @else class="dropdown" @endif>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up<span class="caret"></span></a>
                        <ul id="dropdown-signup" class="dropdown-menu">
                            {{ Form::open(array('url'=>'student/signup','method'=>'post','class'=>'form-group')) }}
                          {{ Form::label('rollno','Roll No.',array('for'=>'rollno')) }}
                            {{ Form::text('rollno',null,array('class'=>'form-control','placeholder'=>'Roll No.')) }}
                          {{ Form::label('emailid','Email ID',array('for'=>'emailid')) }}
                            {{ Form::email('emailid',null,array('class'=>'form-control','placeholder'=>'Email ID')) }}<br>
                            {{ Form::submit('Sign-Up',array('class'=>'btn btn-primary')) }}
                            {{ Form::close() }}
                            @if($errors->signup->has())
                                @foreach($errors->signup->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        {{{ $error }}}
                                    </div>
                                @endforeach
                            @endif
                            @if(Session::has('messagesuccess'))
                          <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            <span class="sr-only">Info:</span>
                            {{{ Session::get('messagesuccess') }}}
                          </div>
                            @endif
                          
                          
                        </ul>
                    </li>      
              
              <li @if($errors->login->has())class="dropdown open" @else class="dropdown" @endif>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login<span class="caret"></span></a>
                        <ul id="dropdown-login" class="dropdown-menu">
                            {{ Form::open(array('url'=>'student/login','method'=>'post','class'=>'form-group')) }}
                            {{ Form::label('rollno','Roll No.',array('for'=>'rollno')) }}
                            {{ Form::text('rollno',null,array('class'=>'form-control','placeholder'=>'Roll No.')) }}
                            {{ Form::label('password','Password',array('for'=>'password')) }}
                            {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
                            {{ Form::label('rememberme', 'Remember Me') }}
                            {{ Form::checkbox('remember_me',null, array('class'=>'checkbox'))}}<br>
                            {{ Form::submit('Login',array('class'=>'btn btn-primary')) }}
                            {{ Form::close() }}
                            @if($errors->login->has())
                                @foreach($errors->login->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        {{{ $error }}}
                                    </div>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                @else
                    <li><p class="navbar-text"> Welcome, {{Auth::student()->get()->Student_FN}}</p></li>
                    <li><a href="{{ URL::to('student/logout') }}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>