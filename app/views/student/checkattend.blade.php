@extends('layouts.master')
@section('header')
@include('student.studentheadernew')
@stop
@section('body')

<header class="intro">
   <div class="container">
     <h2 >ATTENDANCE CHECK:</h2>
     <div class="col-md-4 col-md-push-4">
       <div class="slogan">
         <div style="padding-top: 30px">

      {{ Form::open(array('url'=>'student/checkattend')) }}
                                  {{ Form::label('rollno','Enter Student Roll-No:') }}
                                  {{ Form::text('rollno',null,array('placeholder'=>'Enter Student Roll No','class'=>'form-control')) }}<br>

                            {{ Form::submit('Check Attendance',array('class'=>'btn btn-primary')) }}
                           
                            {{ Form::close() }}
          </div>
       </div>
     </div>
  </div>
</header>
@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop