{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
{{ HTML::style('css/cssfrontend/main.css')}}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}

 <!-- Navigation -->
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header" >
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand page-scroll" href="{{URL::to('/')}}">
          <i class="fa fa-play-circle"></i>  <span class="light"></span> Home
        </a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
        <ul class="nav navbar-nav">
          <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
          <li class="hidden">
            <a href="#page-top"></a>
          </li>
          <li>
            <a href="{{URL::to('admin/')}}">Admin</a>
          </li>
        </ul>
        
        
        
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{{URL::to('external/register')}}">Not an Agnelite ?</a></li>
                @if(!Auth::student()->check())
              <li @if($errors->signup->has()||Session::has('messagesuccess'))class="dropdown open" @else class="dropdown" @endif>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
                        <ul id="dropdown-signup" class="dropdown-menu">
                            {{ Form::open(array('url'=>'student/signup','method'=>'post','class'=>'form-group')) }}
                          {{ Form::label('rollno','Roll No.',array('for'=>'rollno')) }}
                            {{ Form::text('rollno',null,array('class'=>'form-control','placeholder'=>'Roll No.')) }}
                          {{ Form::label('emailid','Email ID',array('for'=>'emailid')) }}
                            {{ Form::email('emailid',null,array('class'=>'form-control','placeholder'=>'Email ID')) }}<br>
                            {{ Form::submit('Sign-Up',array('class'=>'btn btn-primary')) }}
                            {{ Form::close() }}
                            @if($errors->signup->has())
                                @foreach($errors->signup->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        {{{ $error }}}
                                    </div>
                                @endforeach
                            @endif
                            @if(Session::has('messagesuccess'))
                          <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            <span class="sr-only">Info:</span>
                            {{{ Session::get('messagesuccess') }}}
                          </div>
                            @endif
                          
                          
                        </ul>
                    </li>      
              
              <li @if($errors->login->has())class="dropdown open" @else class="dropdown" @endif>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                        <ul id="dropdown-login" class="dropdown-menu">
                            {{ Form::open(array('url'=>'student/login','method'=>'post','class'=>'form-group')) }}
                            {{ Form::label('rollno','Roll No.',array('for'=>'rollno')) }}
                            {{ Form::text('rollno',null,array('class'=>'form-control','placeholder'=>'Roll No.')) }}
                            {{ Form::label('password','Password',array('for'=>'password')) }}
                            {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
                            {{ Form::label('rememberme', 'Remember Me') }}
                            {{ Form::checkbox('remember_me',null, array('class'=>'checkbox'))}}<br>
                            {{ Form::submit('Login',array('class'=>'btn btn-primary')) }}
                            {{ Form::close() }}
                            @if($errors->login->has())
                                @foreach($errors->login->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        {{{ $error }}}
                                    </div>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                @else
              
              
                  <!-- sidebar attempt -->
          <li>
            <a href="#">Actions</a>
            <ul class="dropdown-menu">
               <li><a href="{{URL::to('/student')}}">Student Dashboard</a>
              </li>
              <li><a href="{{URL::to('student/teamform')}}">Team Formation</a>
              </li>
              <li><a href=" {{URL::to('student/compulsoryregister')}}">Compulsory Registration</a>
              </li>
              <li><a href=" {{URL::to('student/noncompregister')}}">Non-Compulsory Registration</a>
              </li>
              
              <li><a href="{{URL::to('student/viewregistrations')}}">Show Transaction History</a>
              </li> 
            </ul>
         </li>
           
          <!-- /.sidebar attempt -->
             
       
              
              
              
                    <li><p class="navbar-text"> Welcome, {{Auth::student()->get()->Student_FN}}</p></li>
                    <li><a href="{{ URL::to('student/logout') }}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                @endif
            </ul>
        
         
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </nav>
  <!-- /Navigation -->