@extends('layouts.master')
@section('header')
@include('student.studentheadernew')
@stop
@section('body')

<header class="intro">
   <div class="container">
     <h2 >Team Formation</h2>
<!--      <div class="col-md-6 col-md-push-2"> -->
       <div class="slogan">
         <div style="padding-top: 30px">
           
  
          
           
           @if($errors->IdentityError->has())
                        @foreach($errors->IdentityError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
            @if($errors->AlreadyRegisteredError->has())
                        @foreach($errors->AlreadyRegisteredError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
          
            @if($errors->EventheadError->has())
                        @foreach($errors->EventheadError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
           
  
  <p>***Only Team Leaders should fill this form*** </p>
  <p>Enter Roll Numbers of team members </p>
     @if(Session::has('membercount'))
{{ Form::open(array('url'=>'student/entermember','method'=>'post','class'=>'form-group')) }}
       
           @foreach(range(1,Session::get('membercount')) as $i)
      {{ Form::label('tmrollno'.$i,'Team Members Roll No.'.$i) }}
      {{ Form::text('tmrollno'.$i,null,array('class'=>'form-control','placeholder'=>'Member Roll Number'))}}<br>          
    @endforeach
@endif
    {{ Form::submit('Submit',array('class'=>'btn btn-primary')) }}                           
    {{ Form::close() }}
  @stop
              </div>
      </div>
    </div>
<!--   </div> -->
</header> 

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
{{ HTML::script('js/browserback.js') }}

@stop
        