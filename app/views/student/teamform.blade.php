@extends('layouts.master')
@section('header')
@include('student.studentheadernew')
@stop
@section('body')
<header class="intro">
   <div class="container">
     <h2 >Team Formation</h2>
<!--      <div class="col-md-6 col-md-push-2"> -->
       <div class="slogan">
         <div style="padding-top: 30px" style="text-align:left ">
         
              @if($errors->IdentityError->has())
                        @foreach($errors->IdentityError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
           @if($errors->LimitError->has())
                        @foreach($errors->LimitError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
           @if($errors->MemberCountError->has())
                        @foreach($errors->MemberCountError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
            
            @if($errors->EventheadError->has())
                        @foreach($errors->EventheadError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
            @if($errors->DayError->has())
                        @foreach($errors->DayError->all() as $error)
                          <div class="alert alert-danger " role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                           {{{ $error }}}
                           </div>
                        @endforeach 
            @endif
            @if(Session::has('SuccessMessage'))
                <div class="alert alert-success" role="alert">
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span class="sr-only"></span>
                      {{{ Session::pull('SuccessMessage') }}}
                </div>
           @endif
           
      {{ Form::open(array('url'=>'student/teamform','method'=>'post','class'=>'form-group')) }}
            {{ Form::label('tlrollno','Team Leaders Roll No.') }}
            {{ Form::text('tlrollno',null,array('placeholder'=>'Enter Team Leader Roll No','class'=>'form-control'))}}<br>
            
            {{ Form::label('event_name','Event Name :') }}
            {{ Form::select('event_name',array(
'day 1' =>array('Graffitti Workshop(Day 1)' => 'Graffitti Workshop(Day 1)',
'Autonomous Robotics(Day 1)' => 'Autonomous Robotics(Day 1)',
      'Debate' => 'Debate',
      'Singing(Elims-duet)' => 'Singing(Elims-duet)',
      'Dance Workshop:Salsa(Day 1)' => 'Dance Workshop:Salsa(Day 1)',
      'Road Rash(Elims)' => 'Road Rash(Elims)',
      'Kick To Goal(Elims)' =>'Kick To Goal(Elims)',
      'Run For The Queen(Elims)' => 'Run For The Queen(Elims)',
      'Smack Dowm(Elims)' => 'Smack Dowm(Elims)',
      'Magneto' => 'Magneto',
           'Online Tech Hunt' => 'Online Tech Hunt',
           'War Of Bands(Participant)' => 'War Of Bands(Participant)'),
      
'day 2' =>array(
           'Graffitti Workshop(Day 2)' => 'Graffitti Workshop(Day 2)',
      'Dance Workshop:Rock-N-Roll(Day 2)' => 'Dance Workshop:Rock-N-Roll(Day 2)',
      'Street Play(Day 2)' => 'Street Play(Day 2)',
     'Reverse Engineering' => 'Reverse Engineering',
     'Contraptions' => 'Contraptions',
   'Truss' => 'Truss',
           'Group Dance(Participant)' => 'Group Dance(Participant)'),
     
'day 3' =>array('Dance Workshop:Jive(Day 3)' => 'Dance Workshop:Jive(Day 3)',
  'Line Follower' => 'Line Follower',
  'TPP' => 'TPP',
  'Junkyard Wars' => 'Junkyard Wars')
   
       )),null,array('class'=>'form-control') }}<br>
    
            {{ Form::label('event_day','Event Day :') }}
            {{ Form::select('event_day',array('1' => '1', '2' => '2', '3' => '3')) }}<br>


            {{ Form::label('membercount','Enter Number of team members(excluding team-leader) :') }}
            {{ Form::selectRange('membercount',1,20,array('class'=>'form-control') )}}<br>

            {{ Form::submit('Enter roll numbers of team members',array('class'=>'btn btn-primary')) }}
                           
                            {{ Form::close() }}
          
        </div>
      </div>
    </div>
<!--   </div> -->
</header> 
@stop


@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js') }}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js') }}
@stop
        