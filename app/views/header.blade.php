<!-- Navigation -->
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand page-scroll" href="{{URL::to('/')}}">
          <i class="fa fa-play-circle"></i>  <span class="light"></span> Home
        </a>
        <a class="navbar-brand" href="{{URL::to('http://www.fcrit.ac.in/')}}">Fr.C.R.I.T, Vashi </a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
        <ul class="nav navbar-nav">
          <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
          <li class="hidden">
            <a href="#page-top"></a>
          </li>
          <li>
            <a class="page-scroll" href="http://eventsfcrit.in/#about">About</a>
          </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Policies</a>
	            <ul class="dropdown-menu">
	              <li><a href="{{ URL::to('termsconditions') }}">T & C</a>
	              </li>
	              <li><a href="{{ URL::to('privacypolicy') }}">Privacy Policy</a>
	              </li>
	              <li><a href="{{ URL::to('refundpolicy') }}">Refund Policy</a>
	              </li>
	            </ul>
          </li>
<!--           <li>
            <a class="page-scroll" href="http://eventsfcrit.in/#sponsor">Sponsors</a>
          </li> -->
          <li>
            <a class="page-scroll" href="http://eventsfcrit.in/#contact">Contact</a>
          </li>
          
          <li>
            
            <a href="#">Events</a>
            <ul class="dropdown-menu">
              <li><a target="_blank" href="{{ URL::to('/events') }}">Events Schedule</a>
              </li>
              <li><a class="page-scroll" href="http://eventsfcrit.in/#events">Events Timeline</a>
              </li>
            </ul>
            
          </li>
          
          
          <li>
            <a href="{{URL::to('admin/')}}">Administrator</a>
          </li>
          <li>
            <a href="{{URL::to('student/')}}">Student</a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </nav>
  <!-- /Navigation -->