@extends('layouts.master')

@section('style')
{{ HTML::style('css/cssfrontend/bootstrap.min.css')}}
{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/main.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}

@stop

@section('header')
@include('header')
@stop

@section('body')
<div class="intro">
  <div class="intro-body">
    <h1 style="padding-top:100px;font-size:100px">ERROR:404 PAGE NOT FOUND</h1>
  </div>
</div>

@stop

@section('footer')
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}

{{ HTML::script('js/jsfrontend/main.js')}}

@stop
