
 @extends('layouts.master') 

  @section('header')
   @include('header')
  <!-- Bootstrap Core CSS -->

{{ HTML::style('css/cssfrontend/animate.css') }}
{{ HTML::style('css/cssfrontend/font-awesome.min.css') }}
{{ HTML::style('css/cssfrontend/jquery.smartmenus.bootstrap.css')}}
{{ HTML::style('css/cssfrontend/main.css') }}

 
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>


  <link rel="shortcut icon" href="../../images/img/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../images/img/144.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../images/img/114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../images/img/72.png">
  <link rel="apple-touch-icon-precomposed" href="../../images/img/57.png">

@stop
  
@section('body') 

  <!-- Preloader -->
  <div id="preloader" >
    <div id="load"></div>
  </div>
  
  <!-- Intro Header -->
  <header class="intro-main">
    <div class="intro-body" id="page-top">
      <div class="slogan">
        <a href="index.html">
          <div class="logo img-responsive">
            <img class="wow rotateIn img-responsive" data-wow-delay="0.9s" src="../../images/img/circle-layer.png" alt="">
          </div>
          <div class="logo img-responsive">
            <img class="wow rotateIn img-responsive" data-wow-delay="1.1s" src="../../images/img/center-layer.png" alt="">
          </div>
          <div class="logo img-responsive">
            <img class="wow flip img-responsive" data-wow-delay="1.3s" src="../../images/img/flag.png" alt="">
          </div>
        </a>
        <div>
          <div class="social-icons wow bounceInUp img-responsive" data-wow-delay="0.8s">
            <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
            <a target="_blank" href="https://www.facebook.com/fcritETAMAX"><i class="fa fa-facebook fa-2x"></i></a>
            <a target="_blank" href="https://www.youtube.com/channel/UClKxujZoma9hoW6bOI70XYg"><i class="fa fa-youtube fa-2x"></i></a>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- /.Intro Header -->


  <!-- Section: about -->
  <section id="about">
    <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 col-md-offset-2">
          <div class="section-heading">
            <div class="wow bounceInDown" data-wow-delay="0.4s">
              <h2>About Etamax</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UGnVPgy42Gs"></iframe>
</div>
        </div>
        <div class="col-md-6">
          <!--   <p><strong>something</strong></p>-->
          <p>
            Known as CRITERIA up until 2002, ETAMAX is the intercollegiate techno-cultural fest of FCRIT, Vashi. Over the past decade, it has grown to become one of the biggest and most eagerly awaited college fests in Navi Mumbai with thousands of students participating each year. It has seen celebrities like Shankar Mahadevan and Aditya Narayan in attendance. ETAMAX '15 brings to you a wide variety of events ranging from robotics to dancing. As the name suggests, ETAMAX allows the participants to explore their limits and expand their horizons in an attempt to attain Maximum Effciency!
          </p>
        </div>
      </div>
    </div>
  </section>
  <!-- /Section: about -->

  <section id="events">
    <iframe src='http://cdn.knightlab.com/libs/timeline/latest/embed/index.html?source=0AgqMhWCciGmIdERiMWVqbGVsbWNndU82cVVtaDNyaFE&font=Bevan-PotanoSans&maptype=toner&lang=en&hash_bookmark=true&height=650' width='100%' height='650' frameborder='0'></iframe>
  </section>

  <!-- Section: Contact -->
  <section id="contact">
    <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 col-md-offset-2">
          <div class="section-heading">
            <div class="wow bounceInDown" data-wow-delay="0.4s">
              <h1>Email or phone are welcome</h1>
            </div>
            <div class="wow bounceInDown" data-wow-delay="0.3s">
              <p>Introduce your self and get in touch</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="contact-section">
      <div class="ear-piece">
        <img class="img-responsive" src="../../images/img/ear-piece.png" alt="">
      </div>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-6">
            <div class="contact-text">
              <h3>Contact</h3>
              <address>
                <i class="fa fa-home"><a>
Fr. C. Rodrigues Institute of Technology,
Agnel Technical Education Complex,<br>
Sector-9A, Vashi,<br>
                  Navi Mumbai-400703.<br></a></i>

                  <i class="fa fa-pencil"><a href="mailto:etamax@eventsfcrit.in">etamax@eventsfcrit.in</a></i><br>
                  <i class="fa fa-phone"><a href="tel:02241611000"> (022)41611000 </a></i><br>
                </address>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!-- /Section: contact -->

  <!-- Map Section -->
  <div id="map"></div>
@stop
 
@section('footer')
{{ HTML::script('js/jsfrontend/jquery.easing.1.3.js')}}
{{ HTML::script('js/jsfrontend/wow.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.bootstrap.min.js')}}
{{ HTML::script('js/jsfrontend/jquery.smartmenus.min.js')}}
{{ HTML::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false')}}

{{ HTML::script('js/jsfrontend/main.js')}}
  
@stop
<!-- </html> -->