<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Carbon\Carbon;

class ESModel extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Regevent_Student';

    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//     protected $fillable = array('id','Student_RollNo','Event_ID');
  protected $fillable = array('id','student_id','regevent_id');
    
}

