<!-- HAD TO CHANGE THE NAME OF THIS CAUSE IT WAS GIVING AN ERROR IN FIRSTORNEW METHOD WITH ORIGINAL NAME -->

<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Personalitydevelopmentnew extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Personalitydevelopment';
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    protected $fillable=array('Student_RollNo','Age');
    protected $primaryKey='Student_RollNo';
    
    public function getAuthIdentifier(){
        return $this->getKey();
    }
}

  
  


