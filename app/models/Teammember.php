<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Teammember extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Teammembers';
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    protected $fillable=array('Student_RollNo');
    protected $primaryKey='id';
    
    public function getAuthIdentifier(){
        return $this->getKey();
    }
}

  
  


