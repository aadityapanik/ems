<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Admin extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Admins';

    /*protected $defaults = array(
        'Admin_ID'=>str_random(10),
    );

    public __contruct(array $attributes = array()){
        $this->setRawAttributes($this->defaults,true);
        parent::__contruct($attributes);
    }*/
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $fillable = array('Admin_ID','Admin_FN','Admin_LN','Admin_Username','Admin_Password','Admin_ContactNo','Admin_EmailID');   

    protected $hidden = array('Admin_Password', 'remember_token');
    protected $primaryKey='Admin_ID';
    
    public function getAuthIdentifier(){
        return $this->getKey();
    }

    public function getAuthPassword(){
        return $this->Admin_Password;
    }

}
