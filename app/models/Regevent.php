<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Carbon\Carbon;

class Regevent extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Regevents';

    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $fillable = array('Event_Name',
                                'Event_ID',
                                'Event_Day',
                                'Event_Seats_Max',
                                'Event_Seat_Count',
                                'Event_Description',
                                'Event_Amount',
                                'Event_Start_Time',
                                'Event_End_Time',
                                'Event_Date',
                                'Event_Type',
                                'Event_Teams_Max',
                                'Event_Teams_Count',
                               'Group_Event_Type'
                               );

    
    protected $primaryKey='Event_ID';
    
    public function getAuthIdentifier(){
        return $this->getKey();
    }

    public function getAuthPassword(){
        return $this->Admin_Password;
    }

    public function student(){
      return $this->belongsToMany('Student','Regevent_Student','regevent_id')->withPivot('paystatus','order_id');
    }
}
