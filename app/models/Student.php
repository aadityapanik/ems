<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Student extends Eloquent implements UserInterface, RemindableInterface {

//   public function __construct(array $attributes = array())
// {
//     $this->setRawAttributes($this->defaults, true);
//     parent::__construct($attributes);
// }
    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Students';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $fillable=array('Student_RollNo',
                          'Student_FN',
                          'Student_LN',
                          'Student_Gender',
                          'Student_Branch',
                          'Student_Semester',
                          'Student_ContactNo',
                          'Student_EmailID',
                          'Student_ID',
                          'Student_SignUp',
                          'Student_isEventhead',
                          'payflag',
                          'amount'
                             );
  
    protected $hidden = array('Student_Password',
                              'remember_token',
                              'Student_ID',
                              'created_at',
                              'updated_at',
                              'Student_isEventhead',
                              'payflag'
                              );
  
    
    
    protected $primaryKey='Student_RollNo';
    
    public function getAuthIdentifier(){
        return $this->getKey();
    }

    public function getAuthPassword(){
        return $this->Student_Password;
    }
  
   
    public function regevent(){
      return $this->belongsToMany('Regevent','Regevent_Student','student_id')->withPivot('paystatus','order_id');
    }
    /*public function yogameditation(){
      return $this->belongsToMany('YogaMeditation','Regevent_Student');
    }
  */
  
    public function isTimeClash($eventid){
      $eventObj=Regevent::where('Event_ID','=',$eventid)->first();
      $regevents=$this->regevent;
      if($regevents->isEmpty())
        return FALSE;
      foreach($regevents as $regevent){
        $rs=Carbon::parse($regevent->Event_Start_Time);
        $re=Carbon::parse($regevent->Event_End_Time);
        $es=Carbon::parse($eventObj->Event_Start_Time);
        if(strcmp($rs->toDateString(),$es->toDateString())!=0)
          continue;
        if($rs<=$es){
          if($re<=$es)
            continue;
          return TRUE;
        }
        else{
          if($re>=$es)
            continue;
          return TRUE;
        }
      }
      return FALSE;
    }
}


