<?php
/**************** FILTER DEFINITIONS *******************/
/*Route::filter('rushi', function(){
  if(!Auth::student->get()->Student_RollNo==101156){
    App::missing();
  }
});*/

Route::filter('admin', function(){
  if(Auth::admin()->guest()){
    return Redirect::to('admin/')->withErrors(array('password'=>'Please Login First'));
  }
});
Route::filter('student', function(){
  if(Auth::student()->guest()){
    return Redirect::to('student/')->withErrors(array('password'=>'Please Login First'), 'login');
  }
});
/*******************************************************/

/*****************EXTERNAL STUDENTS**********************/
Route::get('external/register','HomeController@showExternal');
//Route::post('external/register','ExternalController@doRegister');


/******************** HOME ROUTES **********************/

Route::get('/', 'HomeController@showHome');
Route::get('admin/', 'HomeController@showAdminHome');
Route::get('student/', 'HomeController@showStudentHome');
Route::get('termsconditions', 'HomeController@showTNC');
Route::get('privacypolicy', 'HomeController@showPrivacy');
Route::get('refundpolicy', 'HomeController@showRefund');
Route::get('events/', 'HomeController@showSchedule');

App::missing(function($exception)
{
    return Response::view('pagenotfound', array(), 404);
});
/*******************************************************/
/******************* LOGIN ROUTES **********************/

Route::group(array('before'=>'csrf'),function(){
  Route::post('admin/login','LoginController@doAdminLogin');
  Route::post('student/login','LoginController@doStudentLogin');
  Route::post('student/signup','LoginController@doSignUp');
});
//Route::get('student/login','LoginController@doStudentLogin');
/*********COMPULSORY REGISTER ROUTES****************/
Route::get('admin/logout','LoginController@doAdminLogout');
Route::get('student/logout','LoginController@doStudentLogout');

/*******************ATTENDANCE************************************/
Route::get('eventhead/login','LoginController@doEventHeadLogin');
Route::post('eventhead/student/attendance','AttendController@isEventhead');
Route::post('eventhead/student/doattendance','AttendController@doAttendance');
//Route::get('eventhead/student/checkattend','HomeController@showAttend');

/******************* ADMIN ROUTES ***********************/

Route::group(array('before'=>'admin'), function(){
  Route::get('admin/add','HomeController@showAddAdmin');
  Route::get('admin/delete','HomeController@showDeleteAdmin');
  Route::get('admin/event/add','HomeController@showAddEvent');
  Route::get('admin/event/show','HomeController@showEvents');
  Route::get('admin/event/{eventid}','HomeController@showEvent');
  Route::get('admin/event/update/{eventid}','HomeController@updateEvent');
  Route::get('admin/event/delete/{eventid}','AdminController@deleteEvent');
  Route::get('admin/mail',function(){
    return View::make('admin.adminsendmail');
  });
  Route::get('admin/excel','HomeController@showReportGeneration');
  Route::get('admin/uploadfile','HomeController@showFileUpload');
  Route::get('admin/downloadfile/PD','AdminController@getDownloadPD');
  Route::get('admin/downloadfile/Events','AdminController@getDownloadEvents');
  Route::get('admin/downloadfile/Students','AdminController@getDownloadStudents');
  Route::get('admin/students','HomeController@showStudents');
  Route::group(array('before'=>'csrf'), function(){
    Route::post('admin/add','AdminController@addAdmin');
    Route::post('admin/delete','AdminController@deleteAdmin');
    Route::post('admin/event/add','AdminController@addEvent');
    Route::post('admin/event/update/{eventid}','AdminController@updateEvent');
    Route::post('admin/mail','AdminController@sendMail');
    Route::post('admin/excel-submit','AdminController@reportGeneration');
    Route::post('admin/uploadfile-submit','AdminController@uploadFile');
    Route::post('admin/student','AdminController@showStudents');
  });
});

/*******************************************************/
/***************** STUDENT ROUTES **********************/

Route::group(array('before'=>'student'), function(){
    Route::get('student/event/show','HomeController@showStudentEvent');
  //Route::get('student/compulsoryregister', 'HomeController@showStudentCompreg');
  //Route::get('student/payment','PaymentController@doPayment');
    Route::get('student/viewregistrations','HomeController@showStudentRegistrations');   
    Route::get('student/event/delete/{eventid}','HomeController@deleteStudentEvent');  
    Route::get('student/entermember','HomeController@showEntermembers');
  Route::get('student/noncompregister', 'HomeController@showStudentNonCompreg');
  /********TEAM FORMATION**************/
    Route::get('student/teamform','HomeController@showTeamform');
    Route::group(array('before'=>'csrf'),function(){
    Route::post('student/teamform','TeamformController@doTeamForm');
    Route::post('student/entermember','TeamformController@doEnter');
  //Route::post('student/compulsoryregister','CompulsoryeventsController@doRegister');
  //Route::post('student/noncompregister', 'NonCompRegController@doMoreNonCompreg');
    //attendance
    //Route::post('student/checkattend','AttendController@checkAttend');
//    });
  });
});

/************************Payment*******************************/
Route::group(array('before'=>'student'),function(){
  Route::match(array('GET', 'POST'), '/payment/processing/init', array(
        'as'    =>  'initPayment',
        'uses'  =>  'PaymentController@doPayment'
  ));
  Route::match(array('GET', 'POST'), '/payment/processing/response', array(
        'as'    =>  'resPayment',
        'uses'  =>  'PaymentController@handleResponse'
  ));
  Route::match(array('GET','POST'), '/payment/response', array(
        'as'    =>  'paymentResponse',
        'uses'  =>  'PaymentController@paymentResponse'
  ));
});