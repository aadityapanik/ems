<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Teammembers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Teammembers',function(Blueprint $t){
			$t->increments('id');
      $t->string('teamid',10);
			$t->integer('Student_RollNo');
			$t->timestamps();
	});//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Teammembers');
    //
	}

}
