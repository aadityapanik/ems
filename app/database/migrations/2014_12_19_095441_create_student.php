<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Students',function(Blueprint $t){
			$t->string('Student_ID',10);
			$t->integer('Student_RollNo')->primary();
			$t->string('Student_FN');
			$t->string('Student_LN');
			$t->string('Student_Gender');
      		$t->string('Student_Branch');
			$t->integer('Student_Semester');
			$t->string('Student_ContactNo',10);
			$t->string('Student_EmailID');
			$t->string('Student_Password')->nullable();
			$t->integer('Student_isEventhead');
			$t->integer('Student_SignUp')->default(0);
			$t->string('remember_token', 100)->nullable();
      		$t->integer('payflag');
      		$t->double('amount');
      		$t->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Students');
	}

}
