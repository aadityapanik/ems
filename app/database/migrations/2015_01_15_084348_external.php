<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class External extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('External',function(Blueprint $t){
			$t->increments('id');
			$t->string('fname');
      $t->string('lname');
			$t->string('email');
			$t->string('contact');
      $t->string('event');
      $t->double('amount');
      $t->timestamps();
	});

    //
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('External');
    
    //
	}

}
