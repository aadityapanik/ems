<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventheads extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('EventHeads',function(Blueprint $t){
			$t->increments('id');
      $t->integer('Student_RollNo');
      $t->string('Eventhead_Password');
			$t->string('Event_ID');
      $t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('EventHeads');
	}

}
