<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Regusers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
    Schema::create('Regusers',function(Blueprint $t){
			$t->string('Student_RollNo')->primary();
      $t->string('Student_Password');
      $t->string('e1');
      $t->string('e2');
      $t->string('e3');
      $t->string('e4');
      $t->string('e5');
      $t->string('e6');
      $t->string('e7');
      $t->string('e8');
      $t->string('e9');
      $t->string('e10');
      $t->string('e11');
      $t->string('e12');
      $t->string('e13');
      $t->string('e14');
      $t->string('e15');
      $t->double('amount');
      $t->integer('active');
    });
    
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
    Schema::drop('Regusers');
	}

}
