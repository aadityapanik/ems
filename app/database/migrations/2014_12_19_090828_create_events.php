<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Regevents',function(Blueprint $t){
			$t->string('Event_Name')->unique();
			$t->string('Event_ID')->primary();
			$t->integer('Event_Day');
      $t->integer('Event_Seats_Max');
      $t->integer('Event_Teams_Max');
      $t->mediumtext('Event_Description')->nullable();
			$t->double('Event_Amount');
			$t->datetime('Event_Start_Time');
			$t->datetime('Event_End_Time');
			$t->date('Event_Date');
			$t->string('Event_Type');
      $t->string('Group_Event_Type');
      $t->integer('Event_Seat_Count');
      $t->integer('Event_Teams_Count');
      $t->timestamps();
	});
	}

	/**
	 * Reverse the migrations.
	 * 
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Regevents');
	}

}
