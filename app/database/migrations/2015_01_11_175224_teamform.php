<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Teamform extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
    Schema::create('Teamform',function(Blueprint $t){
			$t->string('teamid',10)->primary();
      $t->string('Eventname');
     $t->string('Eventid')->nullable();
       
      $t->string('TL')->nullable();
      $t->double('teamamount')->nullable();
      $t->double('individualamount')->nullable();
      
      $t->integer('active')->nullable();
      $t->integer('membercount')->nullable();
    });
    
    //
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Teamform');
	
    //
	}

}
