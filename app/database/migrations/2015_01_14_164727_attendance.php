<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attendance extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Attendance',function(Blueprint $t){
			
			$t->string('Student_RollNo')->primary();
      $t->string('Student_Barcode');
			$t->integer('day1');
      $t->string('event1');
      $t->integer('day2');
      $t->string('event2');
      $t->integer('day3');
      $t->string('event3');
      $t->timestamps();
	});

    //
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Attendance');
    //
	}

}
