<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmin extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Admins',function(Blueprint $t){
			$t->string('Admin_ID',10)->primary();
			$t->string('Admin_FN');
			$t->string('Admin_LN');
			$t->string('Admin_Username');
			$t->string('Admin_Password')->nullable();
			$t->string('Admin_ContactNo',10);
			$t->string('Admin_EmailID');
			$t->string('remember_token', 100)->nullable();
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Admins');
	}

}
