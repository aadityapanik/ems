<?php
use Carbon\Carbon;
class EventTableSeeder extends Seeder {
  public function run(){
    DB::table('Regevents');
    
    Regevent::create(array(
      'Event_Name'=>'gt1',
      'Event_ID'=>'D1T01',
      'Event_Day'=>'1',
      'Event_Seats_Max'=>'120',
      'Event_Amount'=>'450',
      'Event_Start_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-11'.' 11:15 AM')->toDateTimeString(),
      'Event_End_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-11'.' 1:15 PM')->toDateTimeString(),
      'Event_Date'=>Carbon::createFromFormat('Y-m-d', '2015-02-11')->toDateString(),
      'Event_Type'=>'T',
      'Group_Event_Type'=>'G',
      'Event_Seat_Count'=>'10'
    ));
    
    Regevent::create(array(
      'Event_Name'=>'c1',
      'Event_ID'=>'D2C01',
      'Event_Day'=>'2',
      'Event_Seats_Max'=>'120',
      'Event_Amount'=>'450',
      'Event_Start_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-12'.' 1:00 PM')->toDateTimeString(),
      'Event_End_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-12'.' 3:00 PM')->toDateTimeString(),
      'Event_Date'=>Carbon::createFromFormat('Y-m-d', '2015-02-12')->toDateString(),
      'Event_Type'=>'C',
      'Group_Event_Type'=>'I',
      'Event_Seat_Count'=>'10'
    ));
    
    Regevent::create(array(
      'Event_Name'=>'e3',
      'Event_ID'=>'D3E01',
      'Event_Day'=>'3',
      'Event_Seats_Max'=>'120',
      'Event_Amount'=>'450',
      'Event_Start_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-13'.' 5:15 PM')->toDateTimeString(),
      'Event_End_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-13'.' 8:00 PM')->toDateTimeString(),
      'Event_Date'=>Carbon::createFromFormat('Y-m-d', '2015-02-13')->toDateString(),
      'Event_Type'=>'E',
      'Group_Event_Type'=>'I',
      'Event_Seat_Count'=>'10'
    ));
    
    Regevent::create(array(
      'Event_Name'=>'Personality Development',
      'Event_ID'=>'D1T02',
      'Event_Day'=>'12',
      'Event_Seats_Max'=>'120',
      'Event_Amount'=>'450',
      'Event_Start_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-12'.' 11:15 AM')->toDateTimeString(),
      'Event_End_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-12'.' 1:15 PM')->toDateTimeString(),
      'Event_Date'=>Carbon::createFromFormat('Y-m-d', '2015-02-12')->toDateString(),
      'Event_Type'=>'T',
      'Group_Event_Type'=>'I',
      'Event_Seat_Count'=>'10'
    ));
    Regevent::create(array(
      'Event_Name'=>'t1',
      'Event_ID'=>'D1T03',
      'Event_Day'=>'1',
      'Event_Seats_Max'=>'120',
      'Event_Amount'=>'450',
      'Event_Start_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-11'.' 11:15 AM')->toDateTimeString(),
      'Event_End_Time'=>Carbon::createFromFormat('Y-m-d h:i A', '2015-02-11'.' 1:15 PM')->toDateTimeString(),
      'Event_Date'=>Carbon::createFromFormat('Y-m-d', '2015-02-11')->toDateString(),
      'Event_Type'=>'T',
      'Group_Event_Type'=>'I',
      'Event_Seat_Count'=>'10'
    ));
  
  }
}