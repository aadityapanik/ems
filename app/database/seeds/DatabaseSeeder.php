<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('StudentTableSeeder');
		//$this->call('AdminTableSeeder');
        //$this->call('EventTableSeeder');
        //$this->call('ESTableSeeder');
        //$this->call('PDSeeder');
  }
}
