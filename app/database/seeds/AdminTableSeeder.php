<?php
class AdminTableSeeder extends Seeder {
    public function run(){
        DB::table('Admins');

        Admin::create(array(
            'Admin_ID'=>str_random(10),
            'Admin_FN'=>'Aayush',
            'Admin_LN'=>'Vats',
            'Admin_Username'=>'aayush_vats',
            'Admin_Password'=>Hash::make('757Bazooka'),
            'Admin_ContactNo'=>'9969616958',
            'Admin_EmailID'=>'aayush.leomessi@gmail.com'
        ));

        Admin::create(array(
            'Admin_ID'=>str_random(10),
            'Admin_FN'=>'Rushikesh',
            'Admin_LN'=>'Shete',
            'Admin_Username'=>'rushikesh_shete',
            'Admin_Password'=>Hash::make('988thegreatescape'),
            'Admin_ContactNo'=>'7208117477',
            'Admin_EmailID'=>'sheterushikesh@yahoo.in'
        ));

        Admin::create(array(
            'Admin_ID'=>str_random(10),
            'Admin_FN'=>'Aaditya',
            'Admin_LN'=>'Panikath',
            'Admin_Username'=>'aaditya_panik',
            'Admin_Password'=>Hash::make('666freshmeat'),
            'Admin_ContactNo'=>'9920771503',
            'Admin_EmailID'=>'aadityarajeev@yahoo.com'
        ));
    }
}