<?php
use Illuminate\Support\MessageBag;
class TeamformController extends BaseController {

    public function doTeamForm() {
    $rules = array(
      
            'tlrollno' => 'required',
            'event_name' => 'required',
            'event_day' => 'required',
            'membercount' => 'required'
            
            );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
//             $error = $validator->messages()->toJSON();
//             return $error;
          $errors= new MessageBag(['errors'=> 'Enter your details first']);
          return Redirect::to('student/teamform')->withErrors($errors,'IdentityError');
        }
        else {
          $isgroupevent=$isevent_day=$maxmembers=$tlrollno=$teamid=$event_name=$membercount=$totalamount=$teamsmax=$teamsregistered=0;
          
          $tlrollno=Input::get('tlrollno');
          $event_name=Input::get('event_name');
          $event_day=Input::get('event_day');
          $membercount=Input::get('membercount');
          
          if(Auth::student()->check())
            {
            if($tlrollno != Auth::student()->get()->Student_RollNo)
              {
//               echo "Enter your own's Roll-no";
//               return View::make('student.teamform');
     $errors= new MessageBag(['errors'=> 'Enter your own Roll-no']);
    return Redirect::to('student/teamform')->withErrors($errors,'IdentityError');
            }
          }
          //check whether the selected event is available on selected day
          $days = DB::table('Regevents')
            ->where('Event_Name','=',$event_name)
            ->get();
          foreach($days as $day)
          {
            if($day->Event_Day == $event_day){
                    $isevent_day=1;
                    break;
                }
          }
          
          if($isevent_day == '1')
        {
          $teamid=str_random(10);
          $eventid = DB::table('Regevents')
            ->where('Event_Name',$event_name)
            ->where('Event_Day',$event_day)
            ->pluck('Event_ID');
            
            
            $teamsregistered=DB::table('Regevents')
              ->where('Event_ID',$eventid)
              ->pluck('Event_Teams_Count');
            $isgroupevent=DB::table('Regevents')
              ->where('Event_ID',$eventid)
              ->pluck('Group_Event_Type');
            
            
             $maxmembers=DB::table('Regevents')
              ->where('Event_ID',$eventid)
              ->pluck('Event_Seats_Max');
           
            $teamsmax=DB::table('Regevents')
              ->where('Event_ID',$eventid)
              ->pluck('Event_Teams_Max');
            
            if($teamsregistered >= $teamsmax){
//               echo "SORRY!!! The Event is Unavailable as the Upper Limit for the number of Teams has crossed! ";
//               return View::make('student/teamform');
              $errors= new MessageBag(['errors'=> 'SORRY!!! The Event is Unavailable as the Upper Limit for the number of Teams has crossed!']);
    return Redirect::to('student/teamform')->withErrors($errors,'LimitError');
            }

            if( $isgroupevent == 'G')
              {
              $teamsmax=$teamsmax/2;
              if(($membercount+1) > ($maxmembers/$teamsmax))
                {
                 $errors= new MessageBag(['errors'=> 'CAUTION: This Event cannot have these many team-members']);
                 return Redirect::to('student/teamform')->withErrors($errors,'MemberCountError');
    
                
              }
            
            }
            
          $iseventhead3=0;
   $checks=DB::table('EventHeads')->where('Event_ID','=',$eventid)->get();
   foreach($checks as $check)
      {
      if($check->Student_RollNo == $tlrollno)
      { 
        $iseventhead3=1;
        break;
      }
    }
          if($iseventhead3 == 1) {
//         echo "The event head cannot select the same event for which he/she is event head";
//         return View::make('student/teamform');
            
           $errors= new MessageBag(['errors'=> 'The event head cannot select the same event for which he/she is event head']);
          return Redirect::to('student/teamform')->withErrors($errors,'EventheadError');
      }   
      
            
          Session :: put('membercount',$membercount);
          Session :: put('teamid',$teamid);
          Session :: put('event_day',$event_day);
          
          Session::put('event_name',$event_name);
          return View::make('student.entermembers')
            -> with('membercount',$membercount)
            ->with('tlrollno',$tlrollno);
        }   
     else{
//        echo "The selected event is not there on the selected day";
//        return View::make('student.teamform');
        $errors= new MessageBag(['errors'=> 'The selected event is not there on the selected day']);
          return Redirect::to('student/teamform')->withErrors($errors,'DayError');
     }
    }
    }
  public function doEnter(){
    $isvalidmember=$selfroll=$membercount=$teamid=$event_name=$eventid=$tlrollno=$tlemail=$individualamount=$totalamount=$iseventhead1=$teamsregistered=$active=0;
    $membercount=Session::get('membercount');
    $teamid=Session::get('teamid');
    if(Auth::student()->check()){
      $tlrollno=Auth::student()->get()->Student_RollNo;
      
    }
       
    
    $event_name=Session::get('event_name');
    $event_day=Session::get('event_day');
   
    $student=Auth::student()->get();
    $tlemail= DB::table('Students')->where('Student_RollNo', $tlrollno)->pluck('Student_EmailID');
    $eventid = DB::table('Regevents')
            ->where('Event_Name',$event_name)
            ->where('Event_Day',$event_day)
            ->pluck('Event_ID');
    $teamsregistered=DB::table('Regevents')
              ->where('Event_ID',$eventid)
              ->pluck('Event_Teams_Count');
    $totalamount = DB::table('Regevents')->where('Event_ID', $eventid)->pluck('Event_Amount');
    $individualamount=$totalamount/($membercount+1);
    $rules =[];
    $teammembers =[];
     $students=Student::get();
    foreach(range(1,($membercount)) as $i){
      $rules=array_add($rules,'tmrollno'.$i,'required');
      $teammembers=array_add($teammembers,$i,Input::get('tmrollno'.$i));
     
      foreach($students as $student){
        if($student->Student_RollNo == Input::get('tmrollno'.$i)){ 
          $isvalidmember=1;     
          break;
        }
      }
      if(Auth::student()->check()){
        if(Input::get('tmrollno'.$i) == Auth::student()->get()->Student_RollNo){
          $selfroll=1;
          break;
        }
      }
    }        
    if($isvalidmember != 1){
//       echo "The Roll-No of team member is invalid";
//       return View::make('student.entermembers');
        $errors= new MessageBag(['errors'=> 'The Roll-No of team member is invalid']);
          return Redirect::to('student/entermember')->withErrors($errors,'IdentityError');
    }
    if($selfroll == 1){
//       echo "Can't enter your own's Roll-no";
//       return View::make('student.entermembers');
          $errors= new MessageBag(['errors'=> 'Cannot enter your own Roll-no']);
          return Redirect::to('student/entermember')->withErrors($errors,'IdentityError');
    }
    $validator = Validator::make(Input::all(), $rules);
    if($validator->fails()){
      $error = $validator->messages()->toJSON();
      return $error;
    }
    else{

        
      DB::table('Teamform')
              ->insert(array(
                'teamid'=>$teamid,
                'Eventname' => $event_name,
                'Eventid' => $eventid,
                'TL' => $tlrollno,
                'active' => 1,
                 'membercount' => $membercount+1
              ));
      DB::table('Teammembers')
        ->insert(array('teamid'=>$teamid,'Student_RollNo'=>$tlrollno));  
      
        
     $active = DB::table('Teamform')
      ->where('teamid',$teamid)
      ->pluck('active');
     
      if($active > 1)
        {
          $errors= new MessageBag(['errors'=> 'You have already registered under this Team.']);
          return Redirect::to('student/entermember')->withErrors($errors,'AlreadyRegisteredError');
       
    }    
    
         
      // checking whether the team members are event heads for that event
      $checks=DB::table('EventHeads')->where('Event_ID','=',$eventid)->get();
      foreach($checks as $check){
        foreach(range(1,($membercount)) as $i){
         
          if($check->Student_RollNo == $teammembers[$i]){ 
            $errors= new MessageBag(['errors'=> 'The team member cannot be the event head for the same event']);
          return Redirect::to('student/entermember')->withErrors($errors,'EventheadError');
        
          }
        }
      }
      
      
      foreach(range(1,($membercount)) as $i){
        DB::table('Teammembers')
        ->insert(array('teamid'=>$teamid,'Student_RollNo'=>$teammembers[$i]));  
      }
      DB::table('Teamform')
      ->where('teamid','=',$teamid)
      ->update(
          array('teamamount' => $totalamount,'individualamount' => $individualamount, 'active' => 2)
      );
      $student=Auth::student()->get();
      $data=array(
        'student' => $student,
        'teamid' => $teamid,
        'totalamount' => $totalamount,
        'individualamount' => $individualamount,
        'event_name' => $event_name,
        'event_day' => $event_day,
        'membercount' => $membercount,
        'tlrollno' => $student->Student_RollNo,
        'teammembers' => $teammembers
      );
      
      Mail::queue('emails.auth.studentteamformleadermail',$data, function($message) use($student){
        $message->to($student->Student_EmailID,$student->Student_FN . $student->Student_LN)->subject('Teamform Information');
      });
      
      foreach($teammembers as $teammember){
        $tmember=Student::where('Student_RollNo',$teammember)->first();
        Mail::queue('emails.auth.studentteamformmembermail',$data, function($message) use($tmember){ 
          $message->to($tmember->Student_EmailID,$tmember->Student_FN . $tmember->Student_LN)->subject('Teamform Information');
        });
      }
//       echo "The details have been mailed to you and your team!";
      DB::table('Regevents')
        ->where('Event_ID','=',$eventid)
        ->update(array('Event_Teams_Count'=>$teamsregistered+1));
        
        
       return Redirect::to('student/teamform')->with('SuccessMessage','The details have been mailed to you and your team!');
      
    
    
    
    }
  }
}