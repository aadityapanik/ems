<?php
use Maatwebsite\Excel\ExcelServiceProvider;
use Illuminate\Support\MessageBag;
class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

 public function showExternal()
	{
 		return View::make('student.external.externalregister');
	}
	
	public function showHome()
	{
 		return View::make('homepage');
	}
	public function showTNC()
	{
 		return View::make('terms');
	}
	public function showPrivacy()
	{
 		return View::make('privacypolicy');
	}
	public function showRefund()
	{
 		return View::make('refundpolicy');
	}
	public function showSchedule()
	{
 		return View::make('events');
	}
  public function showErrorPage()
	{
 		return View::make('pagenotfound');
	}
  
  //compulsory register view
  public function showTeamform()
	{
 		return View::make('student.teamform');
	}
  
  public function showEntermembers()
	{
 		return View::make('student.entermembers');
	}
  public function showStudentCompreg()
  {
  return View::make('student.compulsoryregister');		
  }
  public function showStudentNonCompreg()
  {
	return View::make('student.compulsoryregister');
  }
  
  public function showAttend()
	{
 		return View::make('student.checkattend');
	}
	
	//Shows Login Home Page for Admin

	public function showAdminHome(){		
		return View::make('admin.adminhome');
	}

	//Shows Login Home Page for Student

	public function showStudentHome(){
		return View::make('student.studenthome');
	}

	//Shows Home Page for adding a new Admin

	public function showAddAdmin(){
		return View::make('admin.adminaddadmin');
	}

	//Shows Home Page for deleting an Admin

	public function showDeleteAdmin(){
		return View::make('admin.admindeleteadmin');
	}

	public function showAddEvent(){
		return View::make('admin.adminaddevent');
	}
  
  public function showEvents(){
    $events=Regevent::all();
    return View::make('admin.adminshowevents', compact('events'));
  }
  
  public function showEvent($eventid){
    $event=Regevent::where('Event_ID','=',$eventid)->first();
    $students=Regevent::where('Event_ID',$eventid)->first()->student;
    return View::make('admin.adminshowevent', compact('event','students'));
  }
  
  public function updateEvent($eventid){
    $event=Regevent::where('Event_ID','=',$eventid)->first();
    return View::make('admin.adminupdateevent', compact('event'));
  }

	public function showStudentEvent(){
		$events=Auth::student()->get()->regevent;
		return View::make('student.studentevent', compact('events'));
	}

	public function deleteStudentEvent($eventid){
		ESModel::where('regevent_id','=',$eventid)->delete();
		$events=Auth::student()->get()->regevent;
		return View::make('student.studentevent', compact('events'));
	}
  
//   REPORT GENERATION EXPERIMENTAL 
//   STATUS :WORKING CURRENTLY 
//   SUCCESSFULLY GENERATED list of students in Student table and list of Events in Regevents Table !!!
  public function showReportGeneration(){
    return View::make('admin.admingenerateeventreport');
  }

  public function showFileUpload(){
    return View::make('admin.adminuploadfiles');
  }
  
  public function showStudents(){   
    $students=Student::all();
    return View::make('admin.adminshowstudents', compact('students'));
  }
  
  public function showStudentRegistrations(){
  	$regevents=Auth::student()->get()->regevent;
	return View::make('student.studentorder',compact('regevents'));
  }
}
  


                
