<?php
use Illuminate\Support\MessageBag;
class PaymentController extends BaseController {

	public function doPayment() {

		$thisMethod = 'PaymentController -> ' . 'doPayment -> ';

		//Check Availability
		if(!Session::has('events')){
			$errors= new MessageBag(['errors'=>'Something went wrong. Contact etamax@eventsfcrit.in']);
			return Redirect::to('student/compulsoryregister')->withErrors($errors,'SeatCount');
		}
		if(!Session::has('finalamount')){
			$errors= new MessageBag(['errors'=>'Something went wrong. Contact etamax@eventsfcrit.in']);
			return Redirect::to('student/compulsoryregister')->withErrors($errors,'SeatCount');
		}

		$cur_order = new Orders;

		$events = Session::get('events');

		foreach($events as $cur_event){
			$maxseatcount = $cur_event->Event_Seats_Max;

			$seatcount = DB::table('Regevent_Student')->where('regevent_id', $cur_event->Event_ID)->where(function($query)
			{
				$query->where('paystatus', '=', 1)
				->orWhere('paystatus', '=', 0);
			})
			->count();

			if($seatcount >= $maxseatcount){
				$errors= new MessageBag(['errors'=>'Seats of chosen event '. $cur_event->Event_Name . ' are already filled.']);
				return Redirect::to('student/compulsoryregister')->withErrors($errors,'SeatCount');
			}
		}

		$final_amt = Session::get('finalamount');
		$order_id = strval(time()) . strval(mt_rand(10000,99999));
		$amount = $final_amt;

		$std_id = Auth::student()->get()->Student_RollNo;

		DB::beginTransaction();

		try{

			//Entry in db for ES Model
			foreach($events as $cur_event){
				$reg_es = new ESModel;

				$reg_es->student_id = $std_id;
				$reg_es->regevent_id = $cur_event->Event_ID;
				$reg_es->order_id = $order_id;

				if($amount < 1.00){
					$reg_es->paystatus = 1;
				}
				else{
					$reg_es->paystatus = 0;
				}
				if(!$reg_es->save()){
					$errors= new MessageBag(['errors'=>'Something went wrong. Please contact etamax@eventsfcrit.in']);
					Log::error($thisMethod . 'Unable to save Regevent_Student. Order ID: ' . $order_id);
					return Redirect::to('student/compulsoryregister')->withErrors($errors,'SeatCount');
				}
			}

			$cur_order->order_no = $order_id;
			$cur_order->amount = $amount;
			$cur_order->std_roll_no = $std_id;

			if($amount < 1.00){
				$cur_order->order_status = 1;
			}
			else{
				$cur_order->order_status = 0;
			}

			if($amount > .99){
				$cc_data_request = [
				'merchant_id'	=>	'#####',
				'language'		=>	'en',
				'amount'		=>	number_format($amount,2, '.', ''),
				'currency'		=>	'INR',
				'redirect_url'	=>	'http://www.eventsfcrit.in/payment/processing/response',
				'cancel_url'	=>	'http://www.eventsfcrit.in/payment/processing/response',
				'order_id'		=>	$order_id,

				];

				$merchant_data = '';
				$working_key = '################################';
				$access_code = '##################';
				$redirect_link = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';

				foreach ($cc_data_request as $key => $value){
					$merchant_data .= $key.'='.$value.'&';
				}

				$encrypted_data = encrypt($merchant_data, $working_key);
			}
			if(!$cur_order->save()){
				throw new Expcetion('Unable to store Current Order!');
			}

			DB::commit();

			Session::forget('finalamount');
			Session::forget('events');

			if($amount < 1.00){
			}
			else{
				return View::make('payments.init')->with(['redirect_link'	=>	$redirect_link,
					'encrypted_data'	=>	$encrypted_data,
					'access_code'		=>	$access_code]);
			}
		}
		catch(Exception $ex){
			Log::error($thisMethod . $ex->getMessage() . ' Order ID: ' . $order_id);
			DB::rollback();
			Log::error($thisMethod . 'Rollback Successful');

			return View::make('payments.failure')->with('order_id', $order_id);
		}
		if($amount < 1.00){
			$order_dtsl = $cur_order;
			$order_dtsl->order_no = $order_id;
			return View::make('payments.response')->with('order_dtls', $order_dtsl);
		}
	}

	public function handleResponse(){

		$thisMethod = 'PaymentController -> ' . 'handleResponse -> ';

		if(Input::has('encResp')){
			$enc_resp = Input::get('encResp');

			$workingKey = '################################';
			$rcvdString = decrypt($enc_resp, $workingKey);

			$order_status = "";
			$decryptValues = explode('&', $rcvdString);
			$dataSize = sizeof($decryptValues);
			$order_id = '';
			$tracking_id = '';
			$bank_ref_no = '';
			$order_status = '';
			$failure_message = '';
			$amount = '';

			for($i = 0; $i < $dataSize; $i++)
			{
				$information = explode('=', $decryptValues[$i]);
				
				switch($information[0]){

					case 'order_id':
					$order_id = $information[1];
					break;

					case 'tracking_id':
					$tracking_id = $information[1];
					break;

					case 'bank_ref_no':
					$bank_ref_no = $information[1];
					break;

					case 'order_status':
					$order_status = $information[1];
					break;

					case 'failure_message':
					$failure_message = $information[1];
					break;

					case 'amount':
					$amount = $information[1];
					break;
				}
			}

			//Update DB
			DB::beginTransaction();
			try{
				$order_dtsl = Orders::where('order_no', 'like', $order_id)->get();

				if(count($order_dtsl) < 1){
					throw new Exception('Cannot find order id. Order ID: ' . $order_id);
				}
				$order_dtsl = $order_dtsl[0];
				$order_dtsl->tracking_id = $tracking_id;
				$order_dtsl->bank_ref_no = $bank_ref_no;
				$order_dtsl->failure_msg = $failure_message;
				$order_dtsl->resp_amount = $amount;

				if($order_status === 'Success'){
					$order_dtsl->order_status = 1;
				}
				else if($order_status === 'Aborted'){
					$order_dtsl->order_status = 2;
				}
				else if($order_status === 'Failure'){
					$order_dtsl->order_status = 3;
				}
				else{
					throw new Exception('Invalid CCAvenue Order Status. Order ID: ' . $order_id);
				}
				
				if(!$order_dtsl->save()){
					throw new Exception('Could not update orders. Order ID: ' . $order_id);
				}

				//Update Event to Student mapping with status

				$event_map = ESModel::where('order_id', 'like', $order_id)->get();

				foreach ($event_map as $cur_row){
					$cur_row->paystatus = $order_dtsl->order_status;

					if(!$cur_row->save()){
						throw new Exception('Unable to store payment status in events student map. Event-Student ID: ' . $cur_row->id . ' Order_ID: ' . $order_id);
					}
					
				
				}
				$order=Orders::where('order_no','like',$order_id)->first();
				DB::table('Students')->where('Student_RollNo',$order->std_roll_no)->update(array('amount'=>$order->amount));
				// Seat Updates
				$student=Student::where('Student_RollNo',$order->std_roll_no)->first();
				$regevents=ESModel::where('order_id','like',$order_id);
				$data=array(
					'order'=>$order,
					'student'=>$student,
					'regevents'=>$regevents
				);
				DB::commit();
				
				//if($order_status==='Success'){
				//	Mail::send('emails.auth.studentpayment',$data, function($message) use($student){
            	//		$message->to($student->Student_EmailID,$student->Student_FN . $student->Student_LN)->subject('Registration Confirmation');
          		//	});
				//}
				
				return View::make('payments.response')->with('order_dtls', $order_dtsl);
				//return Redirect::route('paymentResponse')->with('order_dtls', $order_dtsl);
			}
			catch(Exception $ex){
				Log::error($thisMethod . $ex->getMessage() . ' Order ID: ' . $order_id);
				DB::rollback();
				Log::error($thisMethod . 'Rollback Successful');

				return View::make('payments.failure')->with('order_id', $order_id);
			}
		}
		else{
			Log::error($thisMethod . ' Could not get encResponse from CC Avenue. No order no.' );
			return View::make('payments.failure');
		}

		//DB operations

	}
	
	public function paymentResponse(){
		if(!Input::has('order_dtls')){
		//Redirect to home
		}
		else{
			$order_dtls = Input::get('order_dtls');
			return View::make('payments.response')->with('order_dtls', $order_dtls);
		}
	}
}
