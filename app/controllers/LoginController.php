<?php
use Illuminate\Support\MessageBag;
use Carbon\Carbon;
class LoginController extends BaseController {

    public function doEventHeadLogin(){
      echo $eventhead_username=$_POST['eventid'];
      echo $eventhead_password=$_POST['eventpass'];
      return;
      //$eventID=Eventhead::where('Student_RollNo','=',$eventhead_username)->first();
      if($eventhead_username=="101156" && $eventhead_password=="rushi")//!strcmp($eventID->Eventhead_Password,$eventhead_password))
      {
        return "TRUE";
      }
      else{
        return "FALSE";
      }
      
    }  
    public function doAdminLogin() {
        $userdata=array(
            'Admin_Username'=>Input::get('username'),
            'password'=>Input::get('password')
        );

        $rules = array(
            'username' => 'required',
            'password' => 'required|alphaNum|min:4'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return Redirect::to('admin/')->withErrors($validator);
        }
        else {
            if(Auth::admin()->attempt($userdata)){
                return Redirect::to('admin/');
            }
            else{
                $errors=new MessageBag(['errors'=>'Username and/or Password Incorrect']);
                return Redirect::to('admin/')->withErrors($errors)->withInput(Input::except('password'));
            }
        }
    }

    public function doAdminLogout(){
        Auth::admin()->logout();
        return Redirect::to('admin/');
    }

    public function doStudentLogin() {
        $userdata=array(
            'Student_RollNo'=>Input::get('rollno'),
            'password'=>Input::get('password'),
        );

        $rules = array(
            'rollno' => 'required',
            'password' => 'required|alphaNum|min:4'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return Redirect::to('student/')->withErrors($validator,'login');
        }
        else {
            if(Auth::student()->attempt($userdata)){
                return Redirect::to('student/');
            }
            else{
                $errors=new MessageBag(['errors'=>'Username and/or Password Incorrect']);
         return Redirect::to('student/')->withErrors($errors,'login')->withInput(Input::except('password'));
            }
        }
    }

    public function doStudentLogout(){
        Auth::student()->logout();
        return Redirect::to('student/');
    }
  
    public function doSignUp(){
      $rollno=Input::get('rollno');
      $emailid=Input::get('emailid');
      $rules = array(
            'rollno' => 'required|exists:Students,Student_RollNo',
            'emailid' => 'required|email'
      );
      $v=Validator::make(Input::all(), $rules);
      if($v->fails()){
        return Redirect::to('student/')->withErrors($v,'signup');
      }
      else{
        $student=Student::where('Student_RollNo','=',$rollno)->first();
        if(strcmp($student->Student_EmailID,$emailid)==0 && $student->Student_SignUp==0){
          $randomPwd=str_random(20);
          $student->Student_Password=Hash::make($randomPwd);
          $student->Student_SignUp=1;
          $student->save();
          $data=array(
            'Student_FN'=>$student->Student_FN,
            'Student_RollNo'=>$student->Student_RollNo,
            'randomPwd'=>$randomPwd
          );
          Mail::queue('emails.auth.adminsendmail',$data, function($message) use($student){
            $message->to($student->Student_EmailID,$student->Student_FN . $student->Student_LN)->subject('Updated Password');
          });
          $messagesuccess="The Email has been sent !";
          return Redirect::to('student/')->with('messagesuccess',$messagesuccess);
        }
        elseif($student->Student_SignUp!=0){
            $errors=new MessageBag(['errors'=>'Email has already been sent to you.']);
            return Redirect::to('student/')->withErrors($errors,'signup');
        }
        else{
           $errors=new MessageBag(['errors'=>'Roll No and/or Email ID Incorrect']);
           return Redirect::to('student/')->withErrors($errors,'signup');
        }
      }
      return Redirect::to('admin/');
    }
}
