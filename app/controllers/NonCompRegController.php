<?php
use Illuminate\Support\MessageBag;

class NonCompRegController extends BaseController {

    public function doMoreNonCompreg() {
        $rules = array(
            'event_name' => 'required',
            );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
//             $error = $validator->messages()->toJSON();
//             return $error;
           $errors= new MessageBag(['errors'=> 'Enter or choose details properly.']);
           return Redirect::to('student/noncompregister')->withErrors($errors,'IdentityError');
          
        }
        else {
        
               $event_name =Input::get('event_name');
               $teamid=Input::get('teamid');
          if(Auth::student()->check())
                $username =Auth::student()->get()->Student_RollNo;
  
//checking whether already completed compulsory registration
/*********remember to remove regusers*************/

$iscompdone=0;
$paystatuses=DB::table('Regevent_Student')
                        ->where('student_id','=',$username)
                    ->get();
foreach($paystatuses as $paystatus){
  if( $paystatus->paystatus == 1){
    $iscompdone=$iscompdone+1;
  }
}
 

if($iscompdone < 3)
{
   $errors= new MessageBag(['errors'=> 'Please complete the Compulsory Registrations first ! ']);
  return Redirect::to('student/compulsoryregister')->withErrors($errors,'test');

}


$amt=$eventID=$eventtype=$grpeventtype=0;
echo $seatcount = DB::table('Regevents')->where('Event_Name', $event_name)->pluck('Event_Seat_Count');
echo $maxseatcount = DB::table('Regevents')->where('Event_Name', $event_name)->pluck('Event_Seats_Max');
echo $eventtype = DB::table('Regevents')->where('Event_Name', $event_name)->pluck('Event_Type');
echo $grpeventtype = DB::table('Regevents')->where('Event_Name', $event_name)->pluck('Group_Event_Type');
  
echo $eventID = DB::table('Regevents')->where('Event_Name', $event_name)->pluck('Event_ID');  
echo "<br>";

if($seatcount>=$maxseatcount)
{  // echo "Seats of chosen day1 event are already filled.";
//     return Redirect::to('student/compulsoryregister');
  $errors= new MessageBag(['errors'=> 'Seats of chosen day1 event are already filled.']);
  return Redirect::to('student/noncompregister')->withErrors($errors,'SeatLimit');
}
  
  if($grpeventtype == 'G' && $teamid == "")
    {
   
//     echo "Please enter the Team-ID for group events";
// return View::make('student.noncompregister');
    $errors= new MessageBag(['errors'=> 'Please enter the Team-ID for group events.']);
  return Redirect::to('student/noncompregister')->withErrors($errors,'IdentityError');
	 }
  if($grpeventtype=='I' && $teamid!="")
    {
//     echo "Please do not enter the Team-ID for individual events";
// return View::make('student.noncompregister');
//return Redirect::to('student/compulsoryregister');
    $errors= new MessageBag(['errors'=> 'Please do not enter the Team-ID for individual events']);
  return Redirect::to('student/noncompregister')->withErrors($errors,'IdentityError');
  }
  
   if(Auth::student()->get()->isTimeClash($eventID))
    {
     
       $message="time clash between this and previous events";
       return View::make('student.noncompregister')->with('message',$message);
    }
else
    {
   DB::table('Regevent_Student')
      ->insert(array('student_id' => $username, 'regevent_id' => $eventID
      ));
 
    }
 
  $iseventhead=0;
  $checks=DB::table('EventHeads')->where('Event_ID','=',$eventID)->get();
    foreach($checks as $check)
      {
      if($check->Student_RollNo == $username)
      { 
        $iseventhead=1;
        break;
      }
    }
if($iseventhead == 0)    
{  if($grpeventtype == 'G')
    {
    $ismember=$checks=0;
    $checks=DB::table('Teammembers')->where('teamid','=',$teamid)->get();
    foreach($checks as $check)
        {
      if($check->Student_RollNo == $username)
        { 
        $ismember=1;
        break;
        }
        }
      if($ismember == 0)
      {  
    //return View::make('students.compulsoryregister');
        DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID)
         ->delete();
//         echo "You are not registered for this event under this team-ID";
//         return View::make('student.noncompregister');
        $errors= new MessageBag(['errors'=> 'You are not registered for this event under this team-ID']);
        return Redirect::to('student/noncompregister')->withErrors($errors,'IdentityError');

      }
  
 $amt1=DB::table('Teamform')->where('teamid','=',$teamid)->pluck('individualamount');
    }
   else
      {
        
       $amt = DB::table('Regevents')->where('Event_Name', $event_name)
         ->pluck('Event_Amount');
   
   }}
else
  {
//   echo "SORRY !!! You need to register for another event to satisfy the compulsory criteria.You cannot select the same event in which you are event head !!";
//     //return Redirect::to('student/compulsoryregister');
// return View::make('student.noncompregister');
        $errors= new MessageBag(['errors'=> 'SORRY !!! You need to register for another event to satisfy the compulsory criteria.You cannot select the same event in which you are event head !!']);
        return Redirect::to('student/noncompregister')->withErrors($errors,'OtherError');
}
$amtcmp=Auth::student()->get()->amount;
$amtcmp=$amtcmp+$amt;  
DB::table('Students')->
    where('Student_RollNo','=',$username)
    ->update(
    array('amount' => $amtcmp)
    );

 //return Redirect::to('student/noncompregister');
  
  /* 
    DB::table('events')
    ->where('Event_Name','=', $day1)
    ->update(
    array('Event_Seat_Count' => $seatcount1+1)
    );

DB::table('events')
    ->where('Event_Name','=', $day2)
    ->update(
    array('Event_Seat_Count' => $seatcount2+1)
    );

    DB::table('events')
    ->where('Event_Name','=', $day3)
    ->update(
    array('Event_Seat_Count' => $seatcount3+1)
    );

*/
 
//$userdata= new Reguser;
//$userdata=Auth::student()->get();
if(Input::get('morenoncomp'))
  {
  return View::make('student.noncompregister');
}
 if(Input::get('payment'))
{
   return View::make('student.payment');
 }
}
//       HAVE COMMENTED THIS CAUSE THERE IS A LOGICAL MISTAKE HERE ! PLEASE CHECK THIS PART !
else      
{
//     echo "Complete the compulsory registration first";
//   return Redirect::to('student/compulsoryregister');
        $errors= new MessageBag(['errors'=> 'Complete the compulsory registration first']);
        return Redirect::to('student/noncompregister')->withErrors($errors,'CompulsoryFirst');
}
          
        }
          
        
    }}