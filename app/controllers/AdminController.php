<?php

use Illuminate\Support\MessageBag;
class AdminController extends BaseController {

//Add a new administrator

    public function addAdmin() {
        
        $rules = array(
            'username' => 'required',
            'password' => 'required|alphaNum|min:4',
            'contact'   => 'required|numeric',
            'email'     =>  'required|email'
        );

        $validator1 = Validator::make(Input::all(), $rules);

        if($validator1->fails()){
            return Redirect::to('admin/')->withErrors($validator);
        }
        else {
            $admin = array(
            'Admin_ID'=>str_random(10),   
            'Admin_Username'=>Input::get('username'),
            'Admin_Password'=>Hash::make(Input::get('password')),
            'Admin_FN'=>Input::get('firstname'),
            'Admin_LN'=>Input::get('lastname'),
            'Admin_ContactNo'=>Input::get('contact'),
            'Admin_EmailID'=>Input::get('email')
        );
            $loginrules=array(
            'Admin_Username' => 'required|unique:Admins',
            'Admin_EmailID' => 'required|unique:Admins'
            );
            $validator2 = Validator::make($admin,$loginrules);
            if($validator2->fails()){
                $errors=new MessageBag(['errors'=>'Admin already exists!']);
                return Redirect::to('admin/')->withErrors($errors);
            }
            else{
                $adminobj=Admin::firstOrNew($admin);
                $adminobj->save();
            }
            if($adminobj->exists()){
             
                return View::make('admin.Output');
            }
            else{
                $errors=new MessageBag(['errors'=>'Admin Registration failed']);
                return Redirect::to('admin/')->withErrors($errors);
            }   
        }
    }

//Delete an Admin 
  public function deleteAdmin() {
    $input=Input::get('user');
    $admindel=Admin::where('Admin_Username','=',$input);            //The working deletion logic
    $admindelcount=$admindel->count();                          //Returns the count of number of query results 
    if( $admindelcount != 1)
    {
        $errors= new MessageBag(['errors'=> 'Invalid Username']);
        return Redirect::to('admin/')->withErrors($errors);
    }
    else
    {
        Admin::where('Admin_Username', '=',$input)->delete();
        return View::make('admin.Output');
    }
  }
///// Add a new Event
  
  public function addEvent(){
    $insertevent = array(
      'Event_ID' => Input::get('eventid'),
      'Event_Name' => Input::get('eventname'),
      'Event_Day' => Input::get('eventday'),
      'Event_Seats_Max' => Input::get('maxseats'),
//       'Event_Seats_Min' => Input::get('minseats'),
      'Event_Amount' => Input::get('eventamount'),
      'Event_Start_Time' => Carbon::createFromFormat('d-m-Y h:i A',Input::get('eventdate').' '.Input::get('eventstarttime'))->toDateTimeString(),
      'Event_End_Time' => Carbon::createFromFormat('d-m-Y h:i A',Input::get('eventdate').' '.Input::get('eventendtime'))->toDateTimeString(),
      'Event_Date' => Carbon::createFromFormat('d-m-Y',Input::get('eventdate'))->toDateString(),
      'Event_Type' => Input::get('eventtype'),
      'Group_Event_Type' => Input::get('groupeventtype'),
      'Event_Seat_Count' => Input::get('currentseat')
      );

    $eventrules=array(
      'Event_ID' => 'required|unique:Events',
      'Event_Name' => 'required|unique:Events',
      'Event_Day' => 'required|numeric',
      'Event_Seats_Max' => 'required|numeric',
//       'Event_Seats_Min' => 'required|numeric',
      'Event_Amount' => 'required|numeric',
      'Event_Start_Time' => 'required',
      'Event_End_Time' => 'required',
      'Event_Date' => 'required',
      'Event_Type' => 'required|alpha',
      'Group_Event_Type' => 'required|alpha',
      'Event_Seat_Count' => 'required|numeric'
      
      
    );

    $validator3 = Validator::make($insertevent,$eventrules);
    if($validator3->fails() )
    {
        $errors= new MessageBag(['errors'=>'Invalid Input']);
        return Redirect::to('admin/')->withErrors($errors);
    }
    else
    {
        $eventobj= Regevent::firstOrNew($insertevent);                 //For creating a new event based on inputs
        $eventobj->save();
        return Redirect::to('admin/');
    }
  }
  
  ////Update an existing event
  
  public function updateEvent($eventid){
    
    $oldevent = Regevent::where('Event_ID','=',$eventid);
    
    $updateevent = array(
      'Event_ID' => Input::get('eventid'),
      'Event_Name' => Input::get('eventname'),
       'Event_Day' => Input::get('eventday'),
    
      'Event_Seats_Max' => Input::get('maxseats'),
//       'Event_Seats_Min' => Input::get('minseats'),
      'Event_Amount' => Input::get('eventamount'),
      'Event_Start_Time' => Carbon::createFromFormat('d-m-Y h:i A',Input::get('eventdate').' '.Input::get('eventstarttime'))->toDateTimeString(),
      'Event_End_Time' => Carbon::createFromFormat('d-m-Y h:i A',Input::get('eventdate').' '.Input::get('eventendtime'))->toDateTimeString(),
      'Event_Date' => Carbon::createFromFormat('d-m-Y',Input::get('eventdate'))->toDateString(),
      'Event_Type' => Input::get('eventtype'),
      'Group_Event_Type' => Input::get('groupeventtype'),
      'Event_Seat_Count' => Input::get('currentseat')
      );

    $eventrules=array(
      'Event_ID' => 'required',
      'Event_Name' => 'required',
      'Event_Seats_Max' => 'required|numeric',
//       'Event_Seats_Min' => 'required|numeric',
      'Event_Amount' => 'required|numeric',
      'Event_Start_Time' => 'required',
      'Event_End_Time' => 'required',
      'Event_Date' => 'required',
      'Event_Type' => 'required|alpha'
    );

    $validator3 = Validator::make($updateevent,$eventrules);
    if($validator3->fails() )
    {
        return Redirect::to('admin/event/show')->withErrors($validator3);
    }
    else
    {
        $oldevent->delete();
        $eventobj= Regevent::firstOrNew($updateevent);
        $eventobj->save();
        return Redirect::to('admin/event/show')->with('messageupdate','Event Updated');
    }
  }
  
//   Delete an Existing Event 
  
  
  public function deleteEvent($eventid){
    $event=Regevent::where('Event_ID','=',$eventid);
    $event->delete();
    return Redirect::to('admin/event/show')->with('messagedelete','Event Deleted');
  }
  
  
  
// Report Generation as per required criteria
//   WORK ON THIS .....THINK OF A WAY TO TAKE CARE OF ALL SORTS OF QUERIES !!!!

      public function reportGeneration(){
      $a=Input::get('criteria');
        if($a==0)  
        {
          Excel::create('Laravel Excel-01', function($excel) {

      $excel->sheet('Student List', function($sheet) {

          $sheet->setOrientation('landscape');
          $student=Student::where('Student_Branch','=','Computers')->get()->toArray();

          $sheet->fromModel($student);

      });
          })->export('xls');  
            
      }
        elseif($a==1)
          {
          $idvalue=Input::get('');
           Excel::create('Laravel Excel-02', function($excel) {

      $excel->sheet('', function($sheet) {

          $sheet->setOrientation('landscape');
          $student=ESModel::where('regevent_id','=',$idvalue)->get()->toArray();

          $sheet->fromModel($student);

            });
          })->export('xls');          
          
        }
      } 
  
//   FILE UPLOADING Under Construction !!!!!!!!!!!!!!!!!!!!!!!!!
   public function uploadFile()
     {
                
     $a= Input::get('choice');
               
     if($a==0){
         
       //File Getting uploaded properly DO NOT MESS WITH THIS 
       
       $file= Input::file('file')->getClientOriginalName();  
       Input::file('file')->move(__DIR__.'/../../app/storage/uploads/PD/',$file);
              
        $file_del=File::glob(__DIR__.'/../../app/storage/uploads/PD/'.$file);
       
      Excel::batch('app/storage/uploads/PD', function($rows,$file) {
                            
                $rows->each(function($row) {
//                   static $flag=0;
                  static $count=0; 
                  static $count_p= 0;         // Valid Rows
                  static $count_err= 0;       //Invalid Rows

                
                 $a= $row->rollno;      
 //No problem with case make sure change is made in the resp MODEL too ! 
// However, the Excel file must have lowercase column names ONLY !                  
                 $temp=array('Student_RollNo'=>$a);                          
                 
                  $rules=array(
                    'Student_RollNo'=>'required|unique:Personalitydevelopment'
                  );
                  $validator = Validator::make($temp,$rules);
    if($validator->fails() )
    {          
      $count_err=$count_err+1;
//       $flag=0;
              

//               echo " Error #".$count_err;             
                   
    }
    else{
      $count_p= $count_p+1;
//       $flag=1;
                   $pdentry= Personalitydevelopmentnew::firstOrCreate($temp);
                   $pdentry->save();
                        
    }
           

        $count=Personalitydevelopmentnew::where('Student_RollNo','!=','')->count();
//        echo $count." ";
//                   echo $count_err.":err ";
//                   echo " save:".$count_p." ";
                  
      while($count>0 && $count_err>0){
        $del=Personalitydevelopmentnew::where('Student_RollNo','!=','')->delete();
        $count=$count-1;  
        }
                         
                });
        
         });
       
       
//        File Deletion logic !!
       if($file_del!=false)
         {
              foreach($file_del as $file) {
              if(file_exists($file))
              File::delete($file);
                } 
         }
       
        $count=Personalitydevelopmentnew::where('Student_RollNo','!=','')->count();
       if($count!=0)
         {
        return Redirect::to('admin/uploadfile')->with('uploadsuccess','Uploaded Successfully !');
       }
         else
           {
            $errors= new MessageBag(['errors'=> 'Invalid Entry']);
                return Redirect::to('admin/uploadfile')->withErrors($errors,'uploadfail');
               
         }
      
     }
     
      
      elseif($a==1){
       $file= Input::file('file')->getClientOriginalName();                
       Input::file('file')->move(__DIR__.'/../../app/storage/uploads/Events',$file);
         
       $file_del=File::glob(__DIR__.'/../../app/storage/uploads/Events/'.$file);
       
       Excel::batch('app/storage/uploads/Events', function($rows,$file){
         $rows->each(function($row)
           { 
                  static $count=0; 
                  static $count_p= 0;         // Valid Rows
                  static $count_err= 0;       //Invalid Rows
             $a=$row->eventname;
             $b=$row->eventid;
             $c= $row->eventday;
             $d=$row->eventmaxseats;
             $e= $row->amount;
             $f=$row->starttime;
             $g= $row->endtime;
             $h= $row->eventdate;
             $i= $row->eventtype;
             $j= $row->groupevent;
             $k= $row->description;
             $l= $row->currentseats;
             $m= $row->teammax;
             $n= $row->teamcount;
             
             $temp=array('Event_Name'=>$a,
                        'Event_ID'=>$b,
                        'Event_Day'=>$c,
                        'Event_Seats_Max'=>$d,
                        'Event_Amount'=>$e,
                        'Event_Start_Time'=>$f,
                        'Event_End_Time'=>$g,
                        'Event_Date'=>$h,
                        'Event_Type'=>$i,
                        'Group_Event_Type'=>$j,
                        'Event_Description'=>$k,
                        'Event_Seat_Count'=>$l,
                         'Event_Teams_Max'=>$m,
                         'Event_Teams_Count'=>$n
                                       
                        );
              $rules=array(
                    'Event_Name'=>'required',
                    'Event_ID'=>'required|unique:Regevents',
                    'Event_Day'=>'required',
                    'Event_Seats_Max'=>'required|numeric',
                    'Event_Amount'=>'required|numeric',
                    'Event_Start_Time'=>'required',
                    'Event_End_Time'=>'required',
                    'Event_Date'=>'required',
                    'Event_Type'=>'required',
                    'Group_Event_Type'=>'required',
                    'Event_Description'=>'required',
                    'Event_Seat_Count'=>'required|numeric',
                    'Event_Teams_Max' => 'required|numeric',
                    'Event_Teams_Count' => 'required|numeric'
                
                  );
                  $validator = Validator::make($temp,$rules);
             if($validator->fails() )
                    {          
                      $count_err=$count_err+1;
                    }

            else{
                      $count_p= $count_p+1;      
                       $pdentry=Regevent::firstOrCreate($temp);
                       $pdentry->save();

            }
              $count=Regevent::where('Event_ID','!=','')->count();
             
          while($count>0 && $count_err>0){
            $del=Regevent::where('Event_ID','!=','')->delete();
            $count=$count-1;  
        }
                     
                });
        
         });
                  //        File Deletion logic !!
       if($file_del!=false)
         {
              foreach($file_del as $file) {
              if(file_exists($file))
              File::delete($file);
                } 
         }
       
        $count=Regevent::where('Event_ID','!=','')->count();
       if($count!=0)
         {
        return Redirect::to('admin/uploadfile')->with('uploadsuccess','Uploaded Successfully !');
       }
         else
           {
            $errors= new MessageBag(['errors'=> 'Invalid Entry']);
                return Redirect::to('admin/uploadfile')->withErrors($errors,'uploadfail');        
           }   
       
     }
     
     
     elseif($a==2){
       $file= Input::file('file')->getClientOriginalName();                
       Input::file('file')->move(__DIR__.'/../../app/storage/uploads/Students',$file);
       
//        $file_del=File::glob(__DIR__.'/storage/uploads/Students/'.$file);
        $file_del=File::glob(__DIR__.'/../../app/storage/uploads/Students/'.$file);
       
       Excel::batch('app/storage/uploads/Students', function($rows,$file){
         $rows->each(function($row)
           {  
                  static $count=0; 
                  static $count_p= 0;         // Valid Rows
                  static $count_err= 0;       //Invalid Rows
             $a=$row->rollno;
             $b=$row->firstname;
             $c= $row->lastname;
             $d=$row->branch;
             $e= $row->semester;
             $f=$row->mobileno;
             $g= $row->email;
             $h= $row->gender;
             
             $temp=array('Student_ID'=>str_random(10),
                        'Student_RollNo'=>$a,
                        'Student_FN'=>$b,
                        'Student_LN'=>$c,
                        'Student_Branch'=>$d,
                        'Student_Semester'=>$e,
                        'Student_ContactNo'=>$f,
                        'Student_EmailID'=>$g,
                        'Student_Gender'=>$h,
                        'Student_isEventhead'=>'0',
                        'payflag'=>'0',
                        'Student_SignUp'=>'0',
                        'amount'=>'0'  
                        );
              $rules=array(
                    'Student_RollNo'=>'required|unique:Students,Student_RollNo',
                    'Student_FN'=>'required',
                    'Student_LN'=>'required',
                    'Student_Gender'=>'required',
                    'Student_Branch'=>'required',
                    'Student_Semester'=>'required',
                    'Student_ContactNo'=>'required',
//                      'Student_EmailID'=>'required|email'
                  );
//              |unique:Students   |numeric
                  $validator = Validator::make($temp,$rules);
             if($validator->fails() )
                    {          
                      $count_err=$count_err+1;
                    echo 'Error: '.$count_err;
                    }

            else{
                      $count_p= $count_p+1;      
              
                       $pdentry=Student::firstOrCreate($temp);
                       $pdentry->save();
echo $count_p.' ';
            }
              $count=Student::where('Student_RollNo','!=','')->count();
             
          while($count>0 && $count_err>0){
            $del=Student::where('Student_RollNo','!=','')->delete();
            $count=$count-1;  
        }

               
         });
       });

        if($file_del!=false)
         {
              foreach($file_del as $file) {
              if(file_exists($file))
              File::delete($file);
                } 
         }
       
        $count=Student::where('Student_RollNo','!=','')->count();
       if($count!=0)
         {
        return Redirect::to('admin/uploadfile')->with('uploadsuccess','Uploaded Successfully !');
       }
         else
           {
         
//                 $errors= new MessageBag(['errors'=> 'Invalid Entry']);
//                 return Redirect::to('admin/uploadfile')->withErrors($errors,'uploadfail');
             
         }
              }
  if($a==3){
         
       //File Getting uploaded properly DO NOT MESS WITH THIS 
       
       $file= Input::file('file')->getClientOriginalName();  
//        Input::file('file')->move(__DIR__.'/storage/uploads/PD/',$file);
        Input::file('file')->move(__DIR__.'/../../app/storage/uploads/',$file);
              
        $file_del=File::glob(__DIR__.'/../../app/storage/uploads/'.$file);
       
//       Excel::batch('app/controllers/storage/uploads/PD', function($rows,$file) {
         Excel::batch('app/storage/uploads/', function($rows,$file) {
                           
                $rows->each(function($row) {
//                   static $flag=0;
                  static $count=0; 
                  static $count_p= 0;         // Valid Rows
                  static $count_err= 0;       //Invalid Rows

                
                  $a= $row->rollno;
                  $b=$row->eventid;
 //No problem with case make sure change is made in the resp MODEL too ! 
// However, the Excel file must have lowercase column names ONLY !                  
                 $temp=array('Student_RollNo'=>$a,
                            'Event_ID'=>$b 
                            ); 
                  
                  
                 
                  $rules=array(
                    'Student_RollNo'=>'required',
                    'Event_ID' => 'required'
                  );
                  $validator = Validator::make($temp,$rules);
    if($validator->fails() )
    {          
      $count_err=$count_err+1;
//       $flag=0;
              

//               echo " Error #".$count_err;             
                   
    }
    else{
      $count_p= $count_p+1;
//       $flag=1;
                   $pdentry= Eventhead::firstOrCreate($temp);
                   $pdentry->save();
                        
    }
           

        $count=Eventhead::where('Student_RollNo','!=','')->count();
//        echo $count." ";
//                   echo $count_err.":err ";
//                   echo " save:".$count_p." ";
                  
      while($count>0 && $count_err>0){
        $del=Eventhead::where('Student_RollNo','!=','')->delete();
        $count=$count-1;  
        }
                         
                });
        
         });
       
       
//        File Deletion logic !!
       if($file_del!=false)
         {
              foreach($file_del as $file) {
              if(file_exists($file))
              File::delete($file);
                } 
         }
       
        $count=Eventhead::where('Student_RollNo','!=','')->count();
       if($count!=0)
         {
        return Redirect::to('admin/uploadfile')->with('uploadsuccess','Uploaded Successfully !');
       }
         else
           {
            $errors= new MessageBag(['errors'=> 'Invalid Entry']);
                return Redirect::to('admin/uploadfile')->withErrors($errors,'uploadfail');
               
         }
      
     }
  
   }

//   Experimental Download File Function!!!!!!!!!!!!!!!!!!!!!!!!
  
  public function getDownloadPD(){ 
     $file= __DIR__."/storage/templates/PD.xls";
        $headers = array(
              'Content-Type' => 'application/xls',
            );
        return Response::download($file, 'PD_template.xls', $headers); 
}
    public function getDownloadEvents(){ 
     $file= __DIR__."/storage/templates/Events.xls";
        $headers = array(
              'Content-Type' => 'application/xls',
            );
        return Response::download($file, 'Events_template.xls', $headers); 
}
 public function getDownloadStudents(){ 
     $file= __DIR__."/storage/templates/Students.xls";
        $headers = array(
              'Content-Type' => 'application/xls',
            );
        return Response::download($file, 'Students_template.xls', $headers); 
}  
  
  
  
  

// Mail Sender !!!
  
  public function sendMail(){
        $rules = array(
            'rollno' => 'required|exists:Students,Student_RollNo',
        );

        $validator1 = Validator::make(Input::all(), $rules);

        if($validator1->fails()){
            return Redirect::to('admin/mail')->withErrors($validator1);
        }
        else {
          $randomPwd=str_random(20);
          $student=Student::find(Input::get('rollno'));
          $student->Student_Password=Hash::make($randomPwd);
          $student->save();
          $data=array(
            'Student_FN'=>$student->Student_FN,
            'Student_RollNo'=>$student->Student_RollNo,
            'randomPwd'=>$randomPwd
          );
          Mailgun::send('emails.auth.adminsendmail',$data, function($message) use($student){
            $message->to($student->Student_EmailID,$student->Student_FN . $student->Student_LN)->subject('Updated Password');
          });
          Redirect::to('admin/mail')->with('messagemailsent','The Mail has been sent to '.$student->Student_FN.' '.$student->Student_LN.', Roll No: '.$student->Student_RollNo);
  }
  }
  
//   public function showStudents(){
//     return
//   }
  
  public function showStudents(){
    $rules = array(
            'eventid' => 'required|exists:Regevent,Event_ID',
    );
    $validator=Validator::make(Input::all(), $rules);
    if($validator->fails()){
            return Redirect::to('admin/students')->withErrors($validator);
    }
    else{
      $events=Regevent::all();
      $eventid=Input::get('eventid');
      $students=Regevent::where('Event_ID',$eventid)->first()->student;
      Redirect::to('admin/students',compact('events','students'));
    }
    
    
    
    
  }
  
  
  
    public function eventreportgeneration($eventid){
      
        $event=Regevent::where('Event_ID',$eventid)->first();
          Excel::create($event->Event_Name, function($excel) {

      $excel->sheet('Student List', function($sheet) {

          $sheet->setOrientation('landscape');
          $student=$event->get()->student;
          $sheet->fromModel($student);

      });
          })->export('xls');  
            
      
  
    }
  
  
}

