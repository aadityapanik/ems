<?php
use Illuminate\Support\MessageBag;
use Carbon\Carbon;
class ExternalController extends BaseController {

    public function doRegister() {
      $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'contact' => 'required',
            'event' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            $error = $validator->messages()->toJSON();
            return $error;
        }
        else {
        $eventamount=0;
      $fname=Input::get('firstname');
        $lname=Input::get('lastname');
            $email=Input::get('email');
            $contact=Input::get('contact');
            $event=Input::get('event');
            $eventamount = DB::table('Regevents')->where('Event_Name', $event)->pluck('Event_Amount');
            DB::table('External')
              ->insert(array('fname'=>$fname, 'lname'=>$lname, 'email'=>$email, 'contact'=>$contact, 'event'=>$event, 'amount'=>$eventamount));
       return Redirect::to('student/external')->with('SuccessMessage','The details have been mailed to you!');
      
        }
    }}