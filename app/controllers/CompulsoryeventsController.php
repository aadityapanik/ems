<?php
use Illuminate\Support\MessageBag;
class CompulsoryeventsController extends BaseController 
{
  public function doRegister() 
  {
     $rules = array(
     'day1' => 'required',
     'day2' => 'required',
     'day3' => 'required',
      );

     $validator = Validator::make(Input::all(), $rules);

     if($validator->fails())
     {  
            $error = $validator->messages()->toJSON();
            return $error;
//        return Redirect::to('student/compulsoryregister')->withErrors($validator);
        }
        else 
     {
        
        $day1 =Input::get('day1');
        $day2 =Input::get('day2');
        $day3 =Input::get('day3');
       $teamid1=Input::get('teamid1');
        $teamid2=Input::get('teamid2');
        $teamid3=Input::get('teamid3');
       if(Auth::student()->check()){
        $username =Auth::student()->get()->Student_RollNo;
        $sem=Auth::student()->get()->Student_Semester;
        $gender=Auth::student()->get()->Student_Gender;
        $branch=Auth::student()->get()->Student_Branch;
          }
          
//checking whether already completed compulsory registration
/*********remember to remove regusers*************/
$iscompdone=0;
$paystatuses=DB::table('Regevent_Student')
                        ->where('student_id','=',$username)
                    ->get();
foreach($paystatuses as $paystatus){
  if($paystatus->paystatus == 0 || $paystatus->paystatus == 1){
    $iscompdone=$iscompdone+1;
  }
}
 

if($iscompdone == 3)
{
  $errors= new MessageBag(['errors'=> 'Compulsory Registration Already Completed!. Non-Compulsory Registration will be held later. ']);
  return Redirect::to('student/compulsoryregister')->withErrors($errors,'test');
}
else
{
 
  $pdroll=$debateroll=$elocutionroll=$finalamount=$t=$c=$e=$eventID1=$eventID2=$eventID3=$eventday1=$eventday2=$eventday3=$eventtype1=$eventtype2=$eventtype3=$grpeventtype1=$grpeventtype2=$grpeventtype3=$maxseatcount1=$maxseatcount2=$maxseatcount3=0;

 $maxseatcount1 = DB::table('Regevents')->where('Event_Name', $day1)->pluck('Event_Seats_Max') ;
 $eventtype1 = DB::table('Regevents')->where('Event_Name', $day1)->pluck('Event_Type');
 $eventday1 = DB::table('Regevents')->where('Event_Name', $day1)->pluck('Event_Day');
 $grpeventtype1 = DB::table('Regevents')->where('Event_Name', $day1)->pluck('Group_Event_Type');
 $eventID1 = DB::table('Regevents')->where('Event_Name', $day1)->pluck('Event_ID');  


 $seatcount1 = DB::table('Regevent_Student')
               ->where('regevent_id',$eventID1)
               ->where(function($query)
            {
                $query-> where('paystatus','=', 1)
                    ->orWhere('paystatus','=', 0);
            })
               ->count();
  
  
 $maxseatcount2 = DB::table('Regevents')->where('Event_Name', $day2)->pluck('Event_Seats_Max');
 $eventday2 = DB::table('Regevents')->where('Event_Name', $day2)->pluck('Event_Day');
 $eventtype2 = DB::table('Regevents')->where('Event_Name', $day2)->pluck('Event_Type');
 $grpeventtype2 = DB::table('Regevents')->where('Event_Name', $day2)->pluck('Group_Event_Type');
 $eventID2 = DB::table('Regevents')->where('Event_Name', $day2)->pluck('Event_ID');

 $seatcount2 = DB::table('Regevent_Student')
               ->where('regevent_id',$eventID2)
               ->where(function($query)
            {
                $query-> where('paystatus','=', 1)
                    ->orWhere('paystatus','=', 0);
            })
               ->count();
  
 $maxseatcount3 = DB::table('Regevents')->where('Event_Name', $day3)->pluck('Event_Seats_Max');
 $eventday3 = DB::table('Regevents')->where('Event_Name', $day3)->pluck('Event_Day');
 $eventtype3 = DB::table('Regevents')->where('Event_Name', $day3)->pluck('Event_Type');
 $grpeventtype3 = DB::table('Regevents')->where('Event_Name', $day3)->pluck('Group_Event_Type');
 $eventID3 = DB::table('Regevents')->where('Event_Name', $day3)->pluck('Event_ID');
 
 $seatcount3 = DB::table('Regevent_Student')
               ->where('regevent_id',$eventID3)
               ->where(function($query)
            {
            $query-> where('paystatus','=', 1)
                    ->orWhere('paystatus','=', 0);
             })
               ->count();
  if($eventtype1=='T') 
{
    $t=$t+1;
}
if($eventtype2=='T') 
{
    $t=$t+1;
}
if($eventtype3=='T') 
{
    $t=$t+1;
}

if($eventtype1=='C')
{
    $c=$c+1;
}
if($eventtype2=='C')
{
    $c=$c+1;
}
if($eventtype3=='C')
{
    $c=$c+1;
}
if($eventtype1=='E')
{
    $e=$e+1;
}
if($eventtype2=='E')
{
    $e=$e+1;
}
if($eventtype3=='E')
{
    $e=$e+1;
}

  
$pds = DB::table('Personalitydevelopment')->get();
  foreach($pds as $pd)
{   
    if($pd->student_rollno == $username)
    {       
    $pdroll=1;
    break;
    }
}
/*$debates = DB::table('Debate')->get();
foreach($debates as $debate)
{
    if($debate->Student_RollNo == $username)
        $debateroll=1;
}
  
$elocutions = DB::table('Elocution')->get();
foreach($elocutions as $elocution)
{
    if($elocution->Student_RollNo == $username)
        $elocutionroll=1;
}*/
 
if(($t==1 || $pdroll==1) && ($c==1 || $pdroll==1 ) && ($e==1 || $pdroll==1) )
{

  
//For FE students
if($sem=='2'){
  if($eventtype1 == 'T' && ($day1 != 'Photoshop(Day 1)' && $day1 != 'Web Designing(Day 1)')){
    $errors= new MessageBag(['errors'=> 'For the first year students, Photoshop or Web Designing has to be the event to be selected under Group A ']);
  return Redirect::to('student/compulsoryregister')->withErrors($errors,'FEOnly');
//     return Redirect::to('student/compulsoryregister')->with('FEOnly1','1st Error');

    
//   echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > For the first year students, Photoshop or Web Designing has to be the event to be selected under Group A  </div>';
//   return View::make('student/compulsoryregister');
         }
     
  if($eventtype2 == 'T' && ($day2 != 'Photoshop(Day 2)' && $day2 != 'Web Designing(Day 2)')){
    $errors= new MessageBag(['errors'=>'For the first year students, Photoshop or Web Designing has to be the event to be selected under Group A']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'FEOnly');
// return Redirect::to('student/compulsoryregister')->with('FEOnly1','1st Error');
    
//   echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:yellow" class="container" > For the first year students, Photoshop or Web Designing has to be the event to be selected under Group A  </div>';
//   return View::make('student/compulsoryregister');
  
  }
  
  if($eventtype3 == 'T' && ($day3 != 'Photoshop(Day 3)' && $day3 != 'Web Designing(Day 3)')){
          $errors= new MessageBag(['errors'=>'For the first year students, Photoshop or Web Designing has to be the event to be selected under Group A']);
            return Redirect::to('student/compulsoryregister')->withErrors($errors,'FEOnly');
// return Redirect::to('student/compulsoryregister')->with('FEOnly1','1st Error');
    
//     echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > For the first year students, Photoshop or Web Designing has to be the event to be selected under Group A  </div>';
//   return View::make('student/compulsoryregister');
  
  
  }
  
    }
     else{
    if($eventtype1 == 'T' && ($day1 == 'Photoshop(Day 1)' || $day1 == 'Web Designing(Day 1)')){
//             echo "Photoshop and Web Designing under Group A is only for First Year Students !!";
 $errors= new MessageBag(['errors'=>'Photoshop and Web Designing under Group A is only for First Year Students !!']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'FEOnly');
// return Redirect::to('student/compulsoryregister')->with('FEOnly2','2nd Error');

      
      //       echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > Photoshop and Web Designing under Group A is only for First Year Students !!  </div>';
//   return View::make('student/compulsoryregister');
  
    }
     
  if($eventtype2 == 'T' && ($day2 == 'Photoshop(Day 2)' || $day2 == 'Web Designing(Day 2)')){
//             echo "Photoshop and Web Designing under Group A is only for First Year Students !!";
 $errors= new MessageBag(['errors'=>'Photoshop and Web Designing under Group A is only for First Year Students !!']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'FEOnly');
//     return Redirect::to('student/compulsoryregister')->with('FEOnly2','2nd Error');
      
//     echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > Photoshop and Web Designing under Group A is only for First Year Students !!  </div>';
//   return View::make('student/compulsoryregister');
  
  
         }
  
  if($eventtype3 == 'T' && ($day3 == 'Photoshop(Day 3)' || $day3 == 'Web Designing(Day 3)')){
//             echo "Photoshop and Web Designing under Group A is only for First Year Students !!";
 $errors= new MessageBag(['errors'=>'Photoshop and Web Designing under Group A is only for First Year Students !!']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'FEOnly');
//   return Redirect::to('student/compulsoryregister')->with('FEOnly2','2nd Error');

    
    //       echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > Photoshop and Web Designing under Group A is only for First Year Students !!  </div>';
//   return View::make('student/compulsoryregister');
  
    
  }
     }     
//Checking whether seats are filled 
  if($seatcount1>=$maxseatcount1 ) 
  {   $d1=1;
//     echo "<h1 color='red'>Seats of chosen day 1 event are already filled.</h1>";
//    return Redirect::to('student/compulsoryregister');
           $errors= new MessageBag(['errors'=>'Seats of chosen day 1 event are already filled.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'SeatCount');
   
//      echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > Seats of chosen day 1 event are already filled. </div>';
//   return View::make('student/compulsoryregister');
  
 
}

       
  if($seatcount2>=$maxseatcount2)
  {   $d2=1;
//     echo "Seats of chosen day 2 event are already filled.";
//    return Redirect::to('student/compulsoryregister');

           $errors= new MessageBag(['errors'=>'Seats of chosen day 2 event are already filled.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'SeatCount');
   
   
   //    echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > Seats of chosen day 2 event are already filled. </div>';
//   return View::make('student/compulsoryregister');
}

       
  if($seatcount3>=$maxseatcount3)
  {   $d3=1;
//     echo "Seats of chosen day 3 event are already filled.";
//    return Redirect::to('student/compulsoryregister');
//  return View::make('student.compulsoryregister');

           $errors= new MessageBag(['errors'=>'Seats of chosen day 3 event are already filled.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'SeatCount');
   
   
//     echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > Seats of chosen day 3 event are already filled. </div>';
//   return View::make('student/compulsoryregister');
 }

// Events for girls only
if($eventtype3 == 'C' && ($day3 == 'Self Defence' || $day3 == 'General Awareness(Gynaecology)') && ($gender == 'M' || $gender=="MALE"))
  {
//     echo "Only girls are allowed to take part in this event";
//    return Redirect::to('student/compulsoryregister');
  
// return View::make('student.compulsoryregister');
           $errors= new MessageBag(['errors'=>'Only girls are allowed to take part in this event.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'OnlyGirlsEvent');
}
//Yoga for boys only
if(($day1 == 'Yoga And Meditation-Only Boys(Day 1)' || $day2 == 'Yoga And Meditation-Only Boys(Day 2)' || $day3 == 'Yoga And Meditation-Only Boys(Day 3)') && $gender == 'F' || $gender=="FEMALE")
{
  
//   echo "Only Boys are allowed to take part in this event. Yoga is also available for Girls(Only). Kindly check its availabilty.";
//  return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
           $errors= new MessageBag(['errors'=>'Only Boys are allowed to take part in this event. Yoga is also available for Girls(Only). Kindly check its availabilty.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'OnlyBoysEvent');

} 
 
//Yoga for Girls only
  if(($day1 == 'Yoga And Meditation-Only Girls(Day 1)' || $day2 == 'Yoga And Meditation-Only Girls(Day 2)' || $day3 == 'Yoga And Meditation-Only Girls(Day 3)') && $gender == 'M' || $gender=="MALE")
{
//   echo "Only Girls are allowed to take part in this event. Yoga is also available for Boys(Only). Kindly check its availabilty.";
//  return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
           $errors= new MessageBag(['errors'=>'Only Girls are allowed to take part in this event. Yoga is also available for Boys(Only). Kindly check its availabilty.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'OnlyGirlsEvent');
    

  }  
  
  //Handling ifDepartmental Seminar
  //Humanities
  if($sem != '2' && $day1 == 'Humanities Departmental Seminar' )
  {
    $errors= new MessageBag(['errors'=>'Only FE students are allowed to take part in this event.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'DeptSeminar');
     
  }
  
  if($sem != '2' && $branch != 'Computer' && $day1 == 'Computer Departmental Seminar'){
    $errors= new MessageBag(['errors'=>'Only Computer Department students are allowed to take part in this event.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'DeptSeminar');
    
  }
  
  if($sem != '2' && $branch != 'EXTC' && $day3 == 'EXTC Departmental Seminar'){
    $errors= new MessageBag(['errors'=>'Only EXTC Department students are allowed to take part in this event.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'DeptSeminar');
    
  }
  
  if($sem != '2' && $branch != 'Mechanical' && ($day3 == 'Mechanical Departmental Seminar')){
    $errors= new MessageBag(['errors'=>'Only Mechanical Department students are allowed to take part in this event.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'DeptSeminar');
    
  }
  
  //Mech dept seminar compulsory for sem-4,6,8
  if($pdroll != 1)
  {
  if($sem != '2' && $branch == 'Mechanical' && $day3 != 'Mechanical Departmental Seminar'){
    $errors= new MessageBag(['errors'=>'SORRY!!! Mechanical students have to compulsorily take part in Mechanical Departmental Seminar']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'DeptSeminar');
    
  }}
  else{
  if($sem != '2' && $branch == 'Mechanical' && $day3 == 'Personality Development(Day 3)'){
    
  }
  }
  
  
  if($sem != '2' && $branch != 'Electrical' && $day3 == 'Electrical Departmental Seminar'){
    $errors= new MessageBag(['errors'=>'Only Electrical Department students are allowed to take part in this event.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'DeptSeminar');
    
  }
  
  if($sem != '2' && $branch != 'IT' && $day1 == 'IT Departmental Seminar'){
    $errors= new MessageBag(['errors'=>'Only IT Department students are allowed to take part in this event.']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'DeptSeminar');
    
  }
  
  
  
  
  
//Mechanical only
if($sem!= '2' && $branch!='Mechanical' && ($day1 == 'Ansys(Day 1)' || $day1 == 'Automobile Workshop(Day 1)' || $day2 == 'Automobile Workshop(Day 2)' || $day3 =='Ansys(Day 3)' || $day3 == 'Automobile Workshop(Day 3)'))
     {
//        echo "Only Mechanical students(except FE students) are allowed to take part in these events";
//       return Redirect::to('student/compulsoryregister');
//   return View::make('student.compulsoryregister');
           $errors= new MessageBag(['errors'=>'Only Mechanical students(except FE students) are allowed to take part in these events']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'OnlyMechEvent');
    
  
   }
     
//Comps and IT only-.NET
if($sem!= '2' && $branch!='Computer' && $branch != 'IT' && ($day1 == '.Net(Day 1)' || $day1 == '.Net(Day 3)'))
     {
//        echo "Only Computer and IT students(except FE students) are allowed to take part in these events";
//       return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
    $errors= new MessageBag(['errors'=>'Only Computer and IT students(except FE students) are allowed to take part in these events']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'OnlyCompITEvent');
    
  
}
     
//Electrical only-ECAD
if($sem!= '2' && $branch!='Electrical' && ($day2 == 'ECAD(Day 2)' || $day3 == 'ECAD(Day 3)'))
     {
//        echo "Only Electrical students(except FE students) are allowed to take part in these events";
//       return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
   $errors= new MessageBag(['errors'=>'Only Electrical students(except FE students) are allowed to take part in these events']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'OnlyElecEvent');
  
}
     
//Only comps,IT,EXTC,Electrical-Python
if($sem!= '2' && $branch =='Mechanical' && ($day2 == 'Python(Day 2)' || $day3 == 'Python(Day 3)'))
     {
//        echo "Only Electrical,EXTC,IT and Computer students(except FE students) are allowed to take part in these events";
//       return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
   $errors= new MessageBag(['errors'=>'Only Electrical,EXTC,IT and Computer students(except FE students) are allowed to take part in these events']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'NoMechEvent');
  
}
     
    

//PD n debate n Elocution
$x11='Personality Development(Day 1)';
  $x12='Personality Development(Day 2)';
  $x13='Personality Development(Day 3)';

/*$x2='Debate';
$x3='Elocution';*/
if($pdroll ==1 && $branch != 'Mechanical')
{
$temp=($day1!=$x11 || $day2!=$x12 || $day3!=$x13);
if($pdroll==1 && $temp)
{
//     echo "You need to register for personality development for day1, day2 and day3";
//    return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
   $errors= new MessageBag(['errors'=>'You need to register for personality development for day1, day2 and day3']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'CompulsoryPDDebateElocution');

}}
if($sem != '2' && $pdroll ==1 && $branch == 'Mechanical')
{
$temp=($day1!=$x11 || $day2!=$x12 || $day3!= 'Mechanical Departmental Seminar');
if($pdroll==1 && $temp)
{
//     echo "You need to register for personality development for day1, day2 and Mechanical Department Seminar for day3";
//    return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
   $errors= new MessageBag(['errors'=>'You need to register for personality development for day1, day2 and Mechanical Department Seminar for day3']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'CompulsoryPDDebateElocution');

}

}
/*
if($debateroll==1 && $day1!=$x2 )
{
//     echo "You need to register for debate for day1";
//    return Redirect::to('student/compulsoryregister');
//    return;
// return View::make('student.compulsoryregister');
           $errors= new MessageBag(['errors'=>'You need to register for debate for day1']);
           return Redirect::to('student/compulsoryregister')->withErrors($errors,'CompulsoryPDDebateElocution');


}
if($elocutionroll==1 && $day3!=$x3 )
{
//     echo "You need to register for Elocution for day 3";
//    return Redirect::to('student/compulsoryregister');
//    return;
// return View::make('student.compulsoryregister');
        $errors= new MessageBag(['errors'=>'You need to register for Elocution for day 3']);
        return Redirect::to('student/compulsoryregister')->withErrors($errors,'CompulsoryPDDebateElocution');

}
*/
if($pdroll==0 && ($day1==$x11 || $day2==$x12 || $day3==$x13))
{
//     echo "You are not allowed to take for personality development";
//return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
        $errors= new MessageBag(['errors'=>'You are not allowed to take personality development']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'CompulsoryPDDebateElocution');

  //  return;
}
/*
if($debateroll==0 && $day1==$x2)
{
//     echo "You are not eligible to take for debate";
//    return Redirect::to('student/compulsoryregister');
//  return;
// return View::make('student.compulsoryregister');
  $errors= new MessageBag(['errors'=>'You are not eligible to take debate']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'CompulsoryPDDebateElocution');


}
  
 
if($elocutionroll==0 && $day3==$x3)
{
//     echo "You are not eligible to take for Elocution";
//    return Redirect::to('student/compulsoryregister');
//  return;
// return View::make('student.compulsoryregister');
  $errors= new MessageBag(['errors'=>'You are not eligible to take Elocution']);
  return Redirect::to('student/compulsoryregister')->withErrors($errors,'CompulsoryPDDebateElocution');
} 
  
  
  */
if($grpeventtype1 == 'G' && $teamid1 == "")
    {
   
//     echo "Please enter the Team-ID for group events";
 //   return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
        $errors= new MessageBag(['errors'=>'Please enter the Team-ID for group events']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');

    //return View::make('stu')
  }
if($grpeventtype1=='I' && $teamid1!="")
    {
//     echo "Please do not enter the Team-ID for individual events";
//     return View::make('student.compulsoryregister');
//return Redirect::to('student/compulsoryregister');
        $errors= new MessageBag(['errors'=>'Please do not enter the Team-ID for individual events']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');
  }
  if($grpeventtype2=='G' && $teamid2=="")
    {
//     echo "Please enter the Team-ID for group events";
//     return View::make('student.compulsoryregister');
//return Redirect::to('student/compulsoryregister');
        $errors= new MessageBag(['errors'=>'Please enter the Team-ID for group events']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');
  }
  if($grpeventtype2=='I' && $teamid2!="")
    {
//     echo "Please do not enter the Team-ID for individual events";
//     return View::make('student.compulsoryregister');
//return Redirect::to('student/compulsoryregister');
        $errors= new MessageBag(['errors'=>'Please do not enter the Team-ID for individual events']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');
  }
  if($grpeventtype3=='G' && $teamid3=="")
    {
//     echo "Please enter the Team-ID for group events";
//     return View::make('student.compulsoryregister');
//return Redirect::to('student/compulsoryregister');
        $errors= new MessageBag(['errors'=>'Please enter the Team-ID for group events']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');
  }
  if($grpeventtype3=='I' && $teamid3!="")
    {
//     echo "<h1 color='red'>Please do not enter the Team-ID for individual events</h1>";
//     return View::make('student.compulsoryregister');
//return Redirect::to('student/compulsoryregister');
        $errors= new MessageBag(['errors'=>'Please do not enter the Team-ID for individual events']);
        return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');
  }
  
  /*DB::table('Regevent_Student')
      ->insert(array('student_id' => $username, 'regevent_id' => $eventID1
      ));*/
/*  $eventIDs=array($eventID1,$eventID2,$eventID3);
   if(Auth::student()->get()->isTimeClash($eventIDs))
    {
     
    /* DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID1)
         ->delete();
      */
    /*$errors=new MessageBag(['errors' => "Time clash between Events"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'TimeClash');
     
  }*/

 /* DB::table('Regevent_Student')
      ->insert(array('student_id' => $username, 'regevent_id' => $eventID2
      ));
  
  
  
  if(Auth::student()->get()->isTimeClash($eventID3))
    {
    DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID1)
         ->delete();
      
    
    DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID2)
         ->delete();
      
//     echo "time clash between event3 and previous events";
//     return View::make('student.compulsoryregister');
    $errors=new MessageBag(['errors' => "Time clash between event3 and previous events"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'TimeClash');
  }
  
  
  DB::table('Regevent_Student')
      ->insert(array('student_id' => $username, 'regevent_id' => $eventID3
      ));
*/
  
  
  $iseventhead1=0;
  $checks=DB::table('EventHeads')->where('Event_ID','=',$eventID1)->get();
    foreach($checks as $check)
      {
      if($check->Student_RollNo == $username)
      { 
        $iseventhead1=1;
        break;
      }
    }
if($iseventhead1 == 0)    
{  if($grpeventtype1 == 'G')
    {
      $ismember1=$checks=0;
      $checks=DB::table('Teammembers')->where('teamid','=',$teamid1)->get();
      foreach($checks as $check)
      {
        if($check->Student_RollNo == $username)
        { 
          $ismember1=1;
          break;
        }
      }
      if($ismember1 == 0)
      {  
//         echo "You are not registered for this event under this team-ID";
       /* DB::table('Regevent_Student')
        ->where('student_id', '=', $username)
        ->where('regevent_id', '=', $eventID1)
         ->delete();
      
    
        DB::table('Regevent_Student')
        ->where('student_id', '=', $username)
         ->where('regevent_id', '=', $eventID2)
           ->delete();
      
        DB::table('Regevent_Student')
          ->where('student_id', '=', $username)
           ->where('regevent_id', '=', $eventID3)
             ->delete();
      
    */
   
//        return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
    $errors=new MessageBag(['errors' => "You are not registered for this event under this team-ID"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');
        
        }//end of ismember loop
  
    $amt1=DB::table('Teamform')->where('teamid','=',$teamid1)->pluck('individualamount');
   }//end of group event loop
   else
      {
        
    $amt1 = DB::table('Regevents')->where('Event_Name', $day1)
         ->pluck('Event_Amount');
   
     }
}//end of eventhead loop
else
  {
//   echo "SORRY !!! You need to register for another event to satisfy the compulsory criteria. You cannot select the same event in which you are event head !!";
//  return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
    $errors=new MessageBag(['errors' => "SORRY !!! You need to register for another event to satisfy the compulsory criteria. You cannot select the same event in which you are event head !!"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'EventHeadError');

}
    $iseventhead2=0;
  $checks=DB::table('EventHeads')->where('Event_ID','=',$eventID2)->get();
    foreach($checks as $check)
      {
      if($check->Student_RollNo == $username)
      { 
        $iseventhead2=1;
        break;
      }
    }
if($iseventhead2 == 0)    
{  if($grpeventtype2 == 'G')
    {
    $ismember1=$checks=0;
    $checks=DB::table('Teammembers')->where('teamid','=',$teamid2)->get();
    foreach($checks as $check)
        {
      if($check->Student_RollNo == $username)
        { 
        $ismember2=1;
        break;
        }
        }
      if($ismember2 == 0)
      {  
//         echo "You are not registered for this event under this team-ID";
    //return View::make('students.compulsoryregister');
       /* DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID1)
         ->delete();
      
    
      DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID2)
         ->delete();
      
      DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID3)
         ->delete();
      
    
   */
//         return View::make('student.compulsoryregister');
         $errors=new MessageBag(['errors' => "You are not registered for this event under this team-ID"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');

      }
  
    $amt2=DB::table('Teamform')->where('teamid','=',$teamid2)->pluck('individualamount');
    }
   else
      {
        
    $amt2 = DB::table('Regevents')->where('Event_Name', $day2)
         ->pluck('Event_Amount');
   
   }}
else
  {
//     echo "SORRY !!! You need to register for another event to satisfy the compulsory criteria.You cannot select the same event in which you are event head !!";
    //return Redirect::to('student/compulsoryregister');
// return View::make('student.compulsoryregister');
   $errors=new MessageBag(['errors' => "SORRY !!! You need to register for another event to satisfy the compulsory criteria. You cannot select the same event in which you are event head !!"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'EventHeadError');

  }

   $iseventhead3=0;
   $checks=DB::table('EventHeads')->where('Event_ID','=',$eventID3)->get();
   foreach($checks as $check)
      {
      if($check->Student_RollNo == $username)
      { 
        $iseventhead3=1;
        break;
      }
    }
if($iseventhead3 == 0)    
{  if($grpeventtype3 == 'G')
    {
    $ismember3=$checks=0;
    $checks=DB::table('Teammembers')->where('teamid','=',$teamid3)->get();
    foreach($checks as $check)
        {
      if($check->Student_RollNo == $username)
        { 
        $ismember3=1;
        break;
        }
        }
      if($ismember3 == 0)
      {  
//         echo "You are not registered for this event under this team-ID";
    //return View::make('students.compulsoryregister');
       /* DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID1)
         ->delete();
      
    
      DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID2)
         ->delete();
      
      DB::table('Regevent_Student')
      ->where('student_id', '=', $username)
       ->where('regevent_id', '=', $eventID3)
         ->delete();
      */
//         return View::make('student.compulsoryregister');
    
    $errors=new MessageBag(['errors' => "You are not registered for this event under this team-ID"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'GroupFormError');

      }
  
    $amt3=DB::table('Teamform')->where('teamid','=',$teamid3)->pluck('individualamount');
    }
   else
      {
        
   $amt3 = DB::table('Regevents')->where('Event_Name', $day3)
         ->pluck('Event_Amount');
   
   } }
else
  {
//   echo "SORRY !!! You need to register for another event to satisfy the compulsory criteria. You cannot select the same event in which you are event head !!";
//   return View::make('student.compulsoryregister');
 $errors=new MessageBag(['errors' => "SORRY !!! You need to register for another event to satisfy the compulsory criteria. You cannot select the same event in which you are event head !!"]);
 return  Redirect::to('student/compulsoryregister')->withErrors($errors,'EventHeadError');
}

  
$finalamount=$amt1+$amt2+$amt3;
  /*DB::table('Attendance')
    ->where('Student_RollNo','=',$username)
    ->update(
  array(
    'event1' => $day1,
    'event2' => $day2,
    'event3' => $day3
  ));*/

  
     }
else
{
//     $error=echo"improper selection of group A,B,C events";
//   echo '<div style="padding-left:500px;padding-top:150px;position:absolute;font-size:25px;color:red " class="container" > improper selection of group A,B,C events </div>';
//   return View::make('student/compulsoryregister');

   $errors=new MessageBag(['errors' => "Improper selection of group A,B,C events"]);
    return  Redirect::to('student/compulsoryregister')->withErrors($errors,'Others');
}
//  echo $t;
//   echo $c;
//   echo $e;
  Session::put('events', Regevent::whereIn('Event_ID', [$eventID1, $eventID2, $eventID3])->get());
  $teamids=array($teamid1,$teamid2,$teamid3);
  Session::put('teamids',$teamids);
  Session::put('finalamount', $finalamount);
  
   return Redirect::to('student/event/show');
}
  
// if(Input::get('noncomp'))
// {
//   return View::make('student.noncompregister');
// }
// if(Input::get('Payment'))
// {
  
// return View::make('student.payment');
// }
        
     }
     
     
     
     }
     
     
     }

